<?php

/**
 * PHP version 7.2
 * @copyright  Sven Rhinow Webentwicklung 2013-2016 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */
namespace Srhinow\BzBbkBundle\Helper;

use Contao\Database;
use Contao\Frontend;
use Contao\Input;

/**
 * Class BbkHelper
 * @package Srhinow\BzBbkBundle\Helper
 */
class BbkHelper extends Frontend
{
    /**
     * replace Details from Service-People
     * @param $strTag
     * @return mixed|null
     */
    public static function replaceCurrentEventDetails($strTag)
    {
		if(stristr($strTag,'curevent::'))
		{
		    $parts = explode('::',$strTag);
            $time = time();

		    // Get the current event
		    $objEvent = Database::getInstance()->prepare("SELECT * FROM tl_calendar_events WHERE id=? OR alias=?" . (!BE_USER_LOGGED_IN ? " AND (start='' OR start<?) AND (stop='' OR stop>?) AND published=1" : ""))
								       ->limit(1)
								       ->execute((is_numeric(Input::get('events')) ? Input::get('events') : 0), Input::get('events'), $time, $time);

		    return $objEvent->$parts[1];
		}
    }

    /**
     * @param string $type
     * @param string $strUrl
     * @param string $queryStr
     * @param string $extraQuery
     * @param bool $newget
     * @param bool $seo
     * @return string
     */
	public static function createURL($type='url',$strUrl='',$queryStr='',$extraQuery='',$newget=false,$seo=false)
	{
		if($type == 'id')
		{
            $objPage = \PageModel::findByIdOrAlias($strUrl);
			$pageArr = (null !== $objPage) ? $objPage->row() : array();
			$strUrl = self::addQueryToUrl($pageArr,$queryStr,$newget,$seo);
		}
		elseif($type == 'url')
		{
			$strUrl = self::addToUrl($queryStr);
		}

		if(!empty($extraQuery)) $strUrl .= (!strstr($strUrl,'?'))?'?'.$extraQuery:'&amp;'.$extraQuery;

		return  $strUrl;
	}

    /**
     * @param $pageArr
     * @param $strRequest
     * @param $newget
     * @param bool $seo
     * @return string
     */
	protected function addQueryToUrl($pageArr,$strRequest,$newget,$seo=false)
	{
		if($newget) $arrGet=array();
		else $arrGet = $_GET;
	
		if($strRequest != '')
		{
		    $arrFragments = preg_split('/&(amp;)?/i', $strRequest);
		    foreach ($arrFragments as $strFragment)
			{
				$arrParams = explode('=', $strFragment);
				$arrGet[$arrParams[0]] = $arrParams[1];
			}
		}
		$strParams = '';

		if(count($arrGet)>0)foreach ($arrGet as $k=>$v)
		{
			if($seo)  $strParams .= $GLOBALS['TL_CONFIG']['disableAlias'] ? '&amp;' . $k . '=' . $v  : '/' . $v;
			else  $strParams .= $GLOBALS['TL_CONFIG']['disableAlias'] ? '&amp;' . $k . '=' . $v  : '/' . $k . '/' . $v;
		}

		// Do not use aliases
		if ($GLOBALS['TL_CONFIG']['disableAlias'])
		{
			return 'index.php?' . preg_replace('/^&(amp;)?/i', '', $strParams);
		}

		$pageId = strlen($pageArr['alias']) ? $pageArr['alias'] : $pageArr['id'];

		// Get page ID from URL if not set
		if (empty($pageId))
		{
			$pageId = $this->getPageIdFromUrl();
		}

		return ($GLOBALS['TL_CONFIG']['rewriteURL'] ? '' : 'index.php/') . $pageId . $strParams . $GLOBALS['TL_CONFIG']['urlSuffix'];
	}

	/**
	 * Calculate the span between two timestamps in days
	 * @param integer
	 * @param integer
	 * @return integer
	 */
	public static function calculateSpan($intStart, $intEnd)
	{
		return self::unixToJd((int) $intEnd) - self::unixToJd((int) $intStart);
	}


	/**
	 * Convert a UNIX timestamp to a Julian day
	 * @param integer
	 * @return integer
	 */
	public static function unixToJd(int $tstamp)
	{
		list($year, $month, $day) = explode(',', date('Y,m,d', $tstamp));

		// Make year a positive number
		$year += ($year < 0 ? 4801 : 4800);

		// Adjust the start of the year
		if ($month > 2)
		{
			$month -= 3;
		}
		else
		{
			$month += 9;
			--$year;
		}

		$sdn  = floor((floor($year / 100) * 146097) / 4);
		$sdn += floor((($year % 100) * 1461) / 4);
		$sdn += floor(($month * 153 + 2) / 5);
		$sdn += $day - 32045;

		return $sdn;
	}

}
