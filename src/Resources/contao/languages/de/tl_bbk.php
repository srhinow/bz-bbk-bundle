<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2012 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_bbk']['title']       = array('Titel', 'Titel des Bilderbuches');
$GLOBALS['TL_LANG']['tl_bbk']['imageUrl']    	= array('Bilderbuch-Cover', 'eine Abbildung des Biderbuches.');
$GLOBALS['TL_LANG']['tl_bbk']['author']      = array('Verfasser', 'Hier können Sie den Verfasser / Autor des Bilderbuches angeben.');
$GLOBALS['TL_LANG']['tl_bbk']['otherPerson1']      = array('sonstige Person 1', 'Hier können Sie eine weitere beteiligte Person des Bilderbuches angeben.');
$GLOBALS['TL_LANG']['tl_bbk']['otherPerson2']      = array('sonstige Person 2', 'Hier können Sie eine weitere beteiligte Person des Bilderbuches angeben.');
$GLOBALS['TL_LANG']['tl_bbk']['publisher']      = array('Verlag', 'Hier können Sie den Verlag des Bilderbuches angeben.');
$GLOBALS['TL_LANG']['tl_bbk']['bbkNr']    	= array('BBK-Nr.', 'ähnlich einer Bestellnummer');
$GLOBALS['TL_LANG']['tl_bbk']['barcode']     = array('Barcode', 'nur für internen Gebrauch / nicht sichtbar für User');
$GLOBALS['TL_LANG']['tl_bbk']['mediatyp'] = array('Medienart', '');
$GLOBALS['TL_LANG']['tl_bbk']['location']  = array('Standort', '');
$GLOBALS['TL_LANG']['tl_bbk']['ageRecommendation']      = array('Alter (ab)', 'Angaben in Jahren');
$GLOBALS['TL_LANG']['tl_bbk']['collation']   = array('Kollationsvermerk', '');
$GLOBALS['TL_LANG']['tl_bbk']['tags']   = array('Schlagworte', 'Klicken Sie bestehende Wörter über dem Eingabefeld an um sie hinzuzufügen, schreiben sie neue Wörter ins Feld oder klicken sie Wörter in dem Textfeld an, um sie zu entfernen.');
$GLOBALS['TL_LANG']['tl_bbk']['releaseDate']       = array('Erscheinungsjahr', 'Jahreszahl immer 4-stellig');
$GLOBALS['TL_LANG']['tl_bbk']['annotation']       = array('Annotation', '');
$GLOBALS['TL_LANG']['tl_bbk']['published']   = array('BBK veröffentlichen', 'Das Bilderbuch auf der Webseite anzeigen.');
$GLOBALS['TL_LANG']['tl_bbk']['loaned']   = array('BBK ist ausgeliehen', 'Das Bilderbuch als geliehen anzeigen.');
$GLOBALS['TL_LANG']['tl_bbk']['tstamp']      = array('Änderungsdatum', 'Datum und Uhrzeit der letzten Änderung');
$GLOBALS['TL_LANG']['tl_bbk']['imageUrl']      = array('Cover', '');
$GLOBALS['TL_LANG']['tl_bbk']['memo']      = array('Memo/Notiz', 'wird als interne Notiz in der Listenansicht und im Kalender-Tooltip angezeit.');
$GLOBALS['TL_LANG']['tl_bbk']['created_tstamp']      = array('im Sortiment seit', '');
/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_bbk']['title_legend']   = 'Titel und Autor';
$GLOBALS['TL_LANG']['tl_bbk']['layout_legend']  = 'Layoutbereich und Suchbegriffe';

/**
 * Reference
 */
$GLOBALS['TL_LANG']['tl_bbk']['mediatyp_options'] = [
    'cdrom'        => 'CD-ROM',
    'dia'        => 'Diareihe',
    'produkt_themenpaket' => 'Themenpaket',
    'produkt_kamishibai' => 'Kamishibai',
    'produkt_medienkombination' => 'Medienkombination',
    'produkt_dvd' => 'DVD',
    'produkt_spiel' => 'Spiel',
    'produkt_kostuem' => 'Kostüm',
    'produkt_sonstiges' => 'Sonstiges'
];

$GLOBALS['TL_LANG']['tl_bbk']['lueneburg']      = 'Lüneburg';
$GLOBALS['TL_LANG']['tl_bbk']['aurich']         = 'Weser-Ems';
$GLOBALS['TL_LANG']['tl_bbk']['hildesheim']     = 'Südniedersachsen';

$GLOBALS['TL_LANG']['tl_bbk']['1']        = 'ein Jahr';
$GLOBALS['TL_LANG']['tl_bbk']['2']        = '2 Jahre';
$GLOBALS['TL_LANG']['tl_bbk']['3']        = '3 Jahre';
$GLOBALS['TL_LANG']['tl_bbk']['4']        = '4 Jahre';
$GLOBALS['TL_LANG']['tl_bbk']['5']        = '5 Jahre';
$GLOBALS['TL_LANG']['tl_bbk']['6']        = '6 Jahre';
$GLOBALS['TL_LANG']['tl_bbk']['7']        = '7 Jahre';
$GLOBALS['TL_LANG']['tl_bbk']['8']        = '8 Jahre';
$GLOBALS['TL_LANG']['tl_bbk']['9']        = '9 Jahre';
$GLOBALS['TL_LANG']['tl_bbk']['10']        = '10 Jahre';
$GLOBALS['TL_LANG']['tl_bbk']['11']        = '11 Jahre';
$GLOBALS['TL_LANG']['tl_bbk']['12']        = '12 Jahre';
$GLOBALS['TL_LANG']['tl_bbk']['13']        = '13 Jahre';
$GLOBALS['TL_LANG']['tl_bbk']['14']        = '14 Jahre';
$GLOBALS['TL_LANG']['tl_bbk']['15']        = '15 Jahre';
$GLOBALS['TL_LANG']['tl_bbk']['16']        = '16 Jahre';
$GLOBALS['TL_LANG']['tl_bbk']['17']        = '17 Jahre';
$GLOBALS['TL_LANG']['tl_bbk']['18']        = '18 Jahre';

/**
 * HeadButtons
 */
$GLOBALS['TL_LANG']['tl_bbk']['properties']        = array('Einstellungen', 'allgemeine Einstellungen zum Bilderbuch');

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_bbk']['new']        = array('Neues Bilderbuch', 'Ein neues Bilderbuch anlegen');
$GLOBALS['TL_LANG']['tl_bbk']['show']       = array('Bilderbuchdetails', 'Details des Bilderbuches ID %s anzeigen');
$GLOBALS['TL_LANG']['tl_bbk']['edit']       = array('Bilderbuch bearbeiten', 'Bilderbuch ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_bbk']['editheader'] = array('Bilderbuch-Einstellungen bearbeiten', 'Einstellungen des Artikels ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_bbk']['copy']       = array('Bilderbuch duplizieren', 'Bilderbuch ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_bbk']['cut']        = array('Bilderbuch verschieben', 'Bilderbuch ID %s verschieben');
$GLOBALS['TL_LANG']['tl_bbk']['delete']     = array('Bilderbuch löschen', 'Bilderbuch ID %s löschen');
$GLOBALS['TL_LANG']['tl_bbk']['toggle']     = array('Bilderbuch veröffentlichen/unveröffentlichen', 'Artikel ID %s veröffentlichen/unveröffentlichen');
$GLOBALS['TL_LANG']['tl_bbk']['pasteafter'] = array('Einfügen nach', 'Nach Bilderbuch ID %s einfügen');
$GLOBALS['TL_LANG']['tl_bbk']['pasteinto']  = array('Einfügen in', 'In Seite ID %s einfügen');
$GLOBALS['TL_LANG']['tl_bbk']['booking']  = array('Buchungen verwalten', 'Buchungen vom Bilderbuch %s verwalten');

?>
