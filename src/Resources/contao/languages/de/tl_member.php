<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_member']['library'] = array('Bibliothek', 'Wenn Sie hier eine bibliothek zuordnen wird diese mit dem Zugang verknüpft.');
$GLOBALS['TL_LANG']['tl_member']['state'] = array('Bundesland', 'Hier können Sie die Bibliothek einem Bundesland zuordnen.');
?>