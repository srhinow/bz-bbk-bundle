<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2012 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Fields
 */

$GLOBALS['TL_LANG']['tl_bbk_locations']['name']      = array('Name vom Standort', 'Geben Sie hier den Namen der Standort an die dieses Bilderbuch ausleihen möchte.');
$GLOBALS['TL_LANG']['tl_bbk_locations']['alias']      = array('Alias', 'kann leer bleiben. Es wird automatisch befüllt.');
$GLOBALS['TL_LANG']['tl_bbk_locations']['sender']   = array('E-Mail-Adresse des Versenders', 'Bitte geben Sie die E-Mail-Adresse des Versenders ein.');
$GLOBALS['TL_LANG']['tl_bbk_locations']['senderName']   = array('Name des Versenders', 'Bitte geben Sie den Namen des Versenders ein.');
$GLOBALS['TL_LANG']['tl_bbk_locations']['booking_email']   = array('Email-Empfänger für Buchungen', 'Bitte geben Sie die E-Mail-Adresse des Empfängers ein.');
$GLOBALS['TL_LANG']['tl_bbk_locations']['confirmed_subject']  = array('E-Mail-Betreff der Bestätigung', 'Hier können Sie den Betreff der E-Mail eintragen.');
$GLOBALS['TL_LANG']['tl_bbk_locations']['confirmed_html_email']         = array('HTML-Version der Bestätigungs-E-Mail', 'Platzhalter: Startdatum=##startDate##, Enddatum=##endDate##, Startzeit=##startTime##, Veranstaltungsende=##endTime##, Vorname=##firstname##, Nachname=##lastname##, Bibliothek=##library##, Straße=##street##, Postleitzahl=##postal##, Ort=##city##, Telefonnummer=##phone##, E-Mail-Adresse=##email##');
$GLOBALS['TL_LANG']['tl_bbk_locations']['confirmed_text_email']         = array('Text-Version der Bestätigungs-E-Mail',  'Platzhalter: Startdatum=##startDate##, Enddatum=##endDate##, Startzeit=##startTime##, Veranstaltungsende=##endTime##, Vorname=##firstname##, Nachname=##lastname##, Bibliothek=##library##, Straße=##street##, Postleitzahl=##postal##, Ort=##city##, Telefonnummer=##phone##, E-Mail-Adresse=##email##');

$GLOBALS['TL_LANG']['tl_bbk_locations']['rejected_subject']  = array('E-Mail-Betreff der Ablehnung', 'Hier können Sie den Betreff der E-Mail eintragen.');
$GLOBALS['TL_LANG']['tl_bbk_locations']['rejected_html_email']         = array('HTML-Version der Ablehnungs-E-Mail', 'Platzhalter: Startdatum=##startDate##, Enddatum=##endDate##, Startzeit=##startTime##, Veranstaltungsende=##endTime##, Vorname=##firstname##, Nachname=##lastname##, Bibliothek=##library##, Straße=##street##, Postleitzahl=##postal##, Ort=##city##, Telefonnummer=##phone##, E-Mail-Adresse=##email##');
$GLOBALS['TL_LANG']['tl_bbk_locations']['rejected_text_email']         = array('Text-Version der Ablehnungs-E-Mail',  'Platzhalter: Startdatum=##startDate##, Enddatum=##endDate##, Startzeit=##startTime##, Veranstaltungsende=##endTime##, Vorname=##firstname##, Nachname=##lastname##, Bibliothek=##library##, Straße=##street##, Postleitzahl=##postal##, Ort=##city##, Telefonnummer=##phone##, E-Mail-Adresse=##email##');

$GLOBALS['TL_LANG']['tl_bbk_locations']['email_legend'] = 'E-Mail-Einstellungen';
$GLOBALS['TL_LANG']['tl_bbk_locations']['confirmed_legend'] = 'Bestätigungs-Einstellungen';
$GLOBALS['TL_LANG']['tl_bbk_locations']['rejected_legend'] = 'Ablehnungs-Einstellungen';

/**
 * Reference
 */
$GLOBALS['TL_LANG']['tl_bbk_locations']['lueneburg']      = 'Lüneburg';
$GLOBALS['TL_LANG']['tl_bbk_locations']['aurich']         = 'Aurich';
$GLOBALS['TL_LANG']['tl_bbk_locations']['hildesheim']     = 'Hildesheim';
$GLOBALS['TL_LANG']['tl_bbk_locations']['cdrom']        = 'CD-ROM';
$GLOBALS['TL_LANG']['tl_bbk_locations']['dia']        = 'Diareihe';


/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_bbk_locations']['new']        = array('Neuer Standort', 'Eine neue Standort anlegen');
$GLOBALS['TL_LANG']['tl_bbk_locations']['show']       = array('Standortdetails', 'Details vom Standort ID %s anzeigen');
$GLOBALS['TL_LANG']['tl_bbk_locations']['edit']       = array('Standort bearbeiten', 'Standort ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_bbk_locations']['editheader'] = array('Standort-Einstellungen bearbeiten', 'Einstellungen vom Standort ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_bbk_locations']['copy']       = array('Standort duplizieren', 'Standort ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_bbk_locations']['cut']        = array('Standort verschieben', 'Standort ID %s verschieben');
$GLOBALS['TL_LANG']['tl_bbk_locations']['delete']     = array('Standort löschen', 'Standort ID %s löschen');
$GLOBALS['TL_LANG']['tl_bbk_locations']['toggle']     = array('Standort veröffentlichen/unveröffentlichen', 'Standort ID %s veröffentlichen/unveröffentlichen');
$GLOBALS['TL_LANG']['tl_bbk_locations']['bbk'] 	      = array('BBK-Einträge','BBK-Einträge von  ID %s verwalten');
$GLOBALS['TL_LANG']['tl_bbk_locations']['bbk_booking']     = array('BBK-Buchungen', 'BBK-Buchungen von  ID %s verwalten');
$GLOBALS['TL_LANG']['tl_bbk_locations']['reminder']     = array('Rückstände', 'Rückstände des Standortes anzeigen.');

/**
 * default-Values
 */
$GLOBALS['TL_LANG']['tl_bbk_locations']['confirmed_subject_default'] = 'Bestätigung der Bilderbuchausleihe';
$GLOBALS['TL_LANG']['tl_bbk_locations']['confirmed_html_default'] = '<p>Sehr geehrte/r Frau/Herr ##firstname## ##lastname##,</p>
<p>wir freuen uns, dass Sie unser Angebot der Bilderbuchkino-Ausleihe nutzen.<br>Hiermit bestätigen wir die Buchung des Bilderbuchkinos "<strong>##title##"</strong> vom <strong>##startDate##</strong> bis zum <strong>##endDate##</strong>.<br>Das Bilderbuchkino wird Ihnen rechtzeitig zugeschickt.</p>
<p>Mit freundlichen Grüßen</p>
<p>Beratungsstelle für Öffentliche Bibliotheken<br>Richthofenstr. 29<br>31137 Hildesheim<br>E-Mail: <a href="mailto:bst-hildesheim@bz-niedersachsen.de">bst-hildesheim@bz-niedersachsen.de<br></a>Tel: 05121-708 315<br>Fax: 05121-708 412<br>Homepage: <a href="http://www.bz-niedersachsen.de/">www.bz-niedersachsen.de<br></a></p>';
$GLOBALS['TL_LANG']['tl_bbk_locations']['confirmed_text_default'] = 'Sehr geehrte/r Frau/Herr ##firstname## ##lastname##,

wir freuen uns, dass Sie unser Angebot der Bilderbuchkino-Ausleihe nutzen.
Hiermit bestätigen wir die Buchung des Bilderbuchkinos  "##title##" vom ##startDate## bis zum ##endDate##.
Das Bilderbuchkino wird Ihnen rechtzeitig zugeschickt.

Mit freundlichen Grüßen

Beratungsstelle für Öffentliche Bibliotheken
Richthofenstr. 29
31137 Hildesheim
E-Mail: bst-hildesheim@bz-niedersachsen.de
Tel: 05121-708 315
Fax: 05121-708 412
Homepage: www.bz-niedersachsen.de';

$GLOBALS['TL_LANG']['tl_bbk_locations']['rejected_subject_default'] = 'Ablehnung der Bilderbuchausleihe';
$GLOBALS['TL_LANG']['tl_bbk_locations']['rejected_html_default'] = '<p>Sehr geehrte/r Frau/Herr ##firstname## ##lastname##,</p>
<p>wir freuen uns, dass Sie unser Angebot der Bilderbuchkino-Ausleihe nutzen möchten.<br>Leider ist das Bilderbuchkino <strong>##title##</strong> zu dem gewünschten Termin schon von einer anderen Bibliothek gebucht.<br>Gerne suchen wir mit Ihnen eine passende Alternative aus. Bitte nehmen Sie dafür mit uns Kontakt auf.<br>Vielen Dank!</p>
<p>Mit freundlichen Grüßen<br><br>Beratungsstelle für Öffentliche Bibliotheken<br>Richthofenstr. 29<br>31137 Hildesheim<br>E-Mail: <a href="mailto:bst-hildesheim@bz-niedersachsen.de">bst-hildesheim@bz-niedersachsen.de<br></a>Tel: 05121-708 315<br>Fax: 05121-708 412<br>Homepage: <a href="http://www.bz-niedersachsen.de/">www.bz-niedersachsen.de<br></a><br><br></p>';
$GLOBALS['TL_LANG']['tl_bbk_locations']['rejected_text_default'] = 'Sehr geehrte/r Frau/Herr ##firstname## ##lastname##,

wir freuen uns, dass Sie unser Angebot der Bilderbuchkino-Ausleihe nutzen möchten.
Leider ist das Bilderbuchkino ##title## zu dem gewünschten Termin schon von einer anderen Bibliothek gebucht.
Gerne suchen wir mit Ihnen eine passende Alternative aus. Bitte nehmen Sie dafür mit uns Kontakt auf.
Vielen Dank!

Mit freundlichen Grüßen

Beratungsstelle für Öffentliche Bibliotheken
Richthofenstr. 29
31137 Hildesheim
E-Mail: bst-hildesheim@bz-niedersachsen.de
Tel: 05121-708 315
Fax: 05121-708 412
Homepage: www.bz-niedersachsen.de';