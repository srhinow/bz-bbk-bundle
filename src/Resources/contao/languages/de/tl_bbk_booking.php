<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2012 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_bbk_booking']['bbk']      = array('BBK-Titel', 'Geben Sie hier das BBK an welches gebucht werden soll.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['location']      = array('Standort ändern', 'Wenn der Kunde einem anderen Standort zugewiesen werden soll, dann hier den Standort wählen.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['library']      = array('Bibliothek', 'Geben Sie hier die Bibliothek an die dieses Bilderbuch ausleihen möchte.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['firstname']       = array('Vorname', 'Vorname der Kontaktperson');
$GLOBALS['TL_LANG']['tl_bbk_booking']['lastname']       = array('Nachname', 'Nachname der Kontaktperson');
$GLOBALS['TL_LANG']['tl_bbk_booking']['library_template']    = array('Bibliothek-Vorlage', 'Füllen Sie Felder mithilfe der Bibliothektsvorlagen');
$GLOBALS['TL_LANG']['tl_bbk_booking']['library']    = array('Bibliothek', 'Bitte geben Sie den Namen der Bibliothek ein.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['street']      = array('Straße', 'Bitte geben Sie den Straßennamen und die Hausnummer ein.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['postal']      = array('Postleitzahl', 'Bitte geben Sie die Postleitzahl ein.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['city']        = array('Ort', 'Bitte geben Sie den Namen des Ortes ein.');

$GLOBALS['TL_LANG']['tl_bbk_booking']['phone']       = array('Telefonnummer', 'Bitte geben Sie die Telefonnummer ein.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['email']   = array('E-Mail-Adresse', 'Bitte geben Sie die E-Mail-Adresse des Abonnenten ein.');

$GLOBALS['TL_LANG']['tl_bbk_booking']['subject']  = array('E-Mail-Betreff', 'Hier können Sie den Betreff der E-Mail eintragen.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['html_email']         = array('HTML-Version der E-Mail', 'Platzhalter: Startdatum=##startDate##, Enddatum=##endDate##, Vorname=##firstname##, Nachname=##lastname##, Bibliothek=##library##, Straße=##street##, Postleitzahl=##postal##, Ort=##city##, Telefonnummer=##phone##, E-Mail-Adresse=##email##');
$GLOBALS['TL_LANG']['tl_bbk_booking']['text_email']         = array('Text-Version der E-Mail',  'Platzhalter: Startdatum=##startDate##, Enddatum=##endDate##, Vorname=##firstname##, Nachname=##lastname##, Bibliothek=##library##, Straße=##street##, Postleitzahl=##postal##, Ort=##city##, Telefonnummer=##phone##, E-Mail-Adresse=##email##');

$GLOBALS['TL_LANG']['tl_bbk_booking']['addedOn'] = array('Erstellungs-, Bestätigungssdatum', 'Das Datum der Erstellung-, Bestätigung.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['accepted'] = array('Buchung bestätigen / ablehnen','Teilnahme bestätigen oder ablehnen und dazu gehörige E-Mail senden');

$GLOBALS['TL_LANG']['tl_bbk_booking']['startDate']        = array('buchen ab', 'Das Bilderbuch ab diesem Tag ausleihen.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['endDate']         = array('buchen bis', 'Das Bilderbuch bis zu diesem Tag ausleihen');
$GLOBALS['TL_LANG']['tl_bbk_booking']['startShippingDate']        = array('buchen ab (inkl. Versand)', 'Das Bilderbuch ab diesem Tag als ausgeliehen markieren.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['endShippingDate']         = array('buchen bis (inkl. Versand)', 'Das Bilderbuch bis zu diesem Tag als ausgeliehen markieren.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['loan']         = array('Leihen (Buchungszähler)', 'Integerwert der Ausleihen (Buchungszeitraum / 14 Tage).');
$GLOBALS['TL_LANG']['tl_bbk_booking']['memo']      = array('Memo/Notiz', 'Wird als interne Notiz in der Listenansicht und im Kalender-Tooltip angezeit.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['inHouse']      = array('im Haus', 'Setzen Sie diese Checkbox wenn das BBK zu dieser Ausleihe wieder verfügbar ist.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['returnDate']      = array('Rückgabe-Datum', 'Setzen Sie das Datum, ab wann das BBK zu dieser Ausleihe wieder verfügbar war.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['reservation_accepted'] = 'Reservierung bestätigt';
$GLOBALS['TL_LANG']['tl_bbk_booking']['reservation_rejected'] = 'Reservierung abgelehnt';
$GLOBALS['TL_LANG']['tl_bbk_booking']['reservation_no_accepted'] = 'Reservierung nicht bestätigt';

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_bbk_booking']['title_legend']   = 'Titel';
$GLOBALS['TL_LANG']['tl_bbk_booking']['datefields_legend']   = 'Zeitspannen';
$GLOBALS['TL_LANG']['tl_bbk_booking']['layout_legend']  = 'Layoutbereich und Suchbegriffe';
$GLOBALS['TL_LANG']['tl_bbk_booking']['location_legend']  = 'Standort für diese Reservierung ändern.';
$GLOBALS['TL_LANG']['tl_bbk_booking']['address_legend']  = 'Adresse der Buchung';
$GLOBALS['TL_LANG']['tl_bbk_booking']['contact_legend']  = 'Kontakdaten mit dem Buchenden';
$GLOBALS['TL_LANG']['tl_bbk_booking']['email_legend']  = 'Email-Einstellungen';
$GLOBALS['TL_LANG']['tl_bbk_booking']['more_legend']  = 'weitere Angaben';
/**
 * Reference
 */
$GLOBALS['TL_LANG']['tl_bbk_booking']['lueneburg']      = 'Lüneburg';
$GLOBALS['TL_LANG']['tl_bbk_booking']['aurich']         = 'Aurich';
$GLOBALS['TL_LANG']['tl_bbk_booking']['hildesheim']     = 'Hildesheim';
$GLOBALS['TL_LANG']['tl_bbk_booking']['cdrom']        = 'CD-ROM';
$GLOBALS['TL_LANG']['tl_bbk_booking']['dia']        = 'Diareihe';

/**
* Export as CSV
*/
$GLOBALS['TL_LANG']['tl_bbk_booking']['csvExportBookings'] = array('CSV-Export', 'BBK-Buchungen als CSV für statistische Auswertungen exportieren.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['exportCSV'] = array('starte Export', '');
$GLOBALS['TL_LANG']['tl_bbk_booking']['exportYear'] = array('exportiere folgendes Jahr','alle Buchungen innerhalb dieses Jahres werden in eine CSV exportiert.');
$GLOBALS['TL_LANG']['tl_bbk_booking']['runList'] = array('Laufzettel', 'kommende BBK-Buchungen als druckbare Version ausgeben');
$GLOBALS['TL_LANG']['tl_bbk_booking']['runlist_generate_button'] = array('generiere Laufliste', '');
$GLOBALS['TL_LANG']['tl_bbk_booking']['runList_beginnDate'] = array('Liste ab (Datum)', 'Alle Buchungen ab diesem Datum werden aufgenommen (Standart: heute + 14 Tage)');
$GLOBALS['TL_LANG']['tl_bbk_booking']['runList_endDate'] = array('Liste bis (Datum)', 'Alle Buchungen bis diesem Datum werden aufgenommen (Standart: heute + 21 Tage)');
$GLOBALS['TL_LANG']['tl_bbk_booking']['privaddress'] = ['private Adresse'];
/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_bbk_booking']['new']        = array('Neue Buchung', 'Eine neue Buchung anlegen');
$GLOBALS['TL_LANG']['tl_bbk_booking']['show']       = array('Buchungdetails', 'Details der Buchung ID %s anzeigen');
$GLOBALS['TL_LANG']['tl_bbk_booking']['booking']       = array('Buchungskalender', 'Buchungskalender für zu buchendes BBK anzeigen');
$GLOBALS['TL_LANG']['tl_bbk_booking']['edit']       = array('Buchung bearbeiten', 'Buchung ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_bbk_booking']['editheader'] = array('Buchung-Einstellungen bearbeiten', 'Einstellungen der Buchung ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_bbk_booking']['copy']       = array('Buchung duplizieren', 'Buchung ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_bbk_booking']['cut']        = array('Buchung verschieben', 'Buchung ID %s verschieben');
$GLOBALS['TL_LANG']['tl_bbk_booking']['delete']     = array('Buchung löschen', 'Buchung ID %s löschen');
$GLOBALS['TL_LANG']['tl_bbk_booking']['toggle']     = array('Buchung veröffentlichen/unveröffentlichen', 'Buchung ID %s veröffentlichen/unveröffentlichen');

/**
* custom Options
*/
$GLOBALS['TL_LANG']['tl_bbk_booking']['acceptedOptions'] = array(''=>'nicht bearbeitet', '1'=>'bestätigt', '2'=>'abgelehnt');
?>
