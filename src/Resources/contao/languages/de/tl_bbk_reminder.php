<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2012 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_bbk_reminder']['booking']      = array('BBK-Buchung', '');

$GLOBALS['TL_LANG']['tl_bbk_reminder']['subject']  = array('E-Mail-Betreff', 'Hier können Sie den Betreff der E-Mail eintragen.');
$GLOBALS['TL_LANG']['tl_bbk_reminder']['html_email']         = array('HTML-Version der E-Mail', 'Platzhalter: Startdatum=##startDate##, Enddatum=##endDate##, Vorname=##firstname##, Nachname=##lastname##, Bibliothek=##library##, Straße=##street##, Postleitzahl=##postal##, Ort=##city##, Telefonnummer=##phone##, E-Mail-Adresse=##email##');
$GLOBALS['TL_LANG']['tl_bbk_reminder']['text_email']         = array('Text-Version der E-Mail',  'Platzhalter: Startdatum=##startDate##, Enddatum=##endDate##, Vorname=##firstname##, Nachname=##lastname##, Bibliothek=##library##, Straße=##street##, Postleitzahl=##postal##, Ort=##city##, Telefonnummer=##phone##, E-Mail-Adresse=##email##');
$GLOBALS['TL_LANG']['tl_bbk_reminder']['memo']      = array('Memo/Notiz', 'Wird als interne Notiz in der Listenansicht und im Kalender-Tooltip angezeit.');

$GLOBALS['TL_LANG']['tl_bbk_reminder']['MESSAGE']['no_id'] = 'Keine ID als Daten-Referenz angegeben.';
$GLOBALS['TL_LANG']['tl_bbk_reminder']['MESSAGE']['Reminder_is_checked'] = 'Die Rückstände wurden erfolgreich aktualisiert.';

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_bbk_reminder']['title_legend']   = 'Titel';
$GLOBALS['TL_LANG']['tl_bbk_reminder']['email_legend']  = 'Email-Einstellungen';
$GLOBALS['TL_LANG']['tl_bbk_reminder']['more_legend']  = 'weitere Angaben';

/**
 * Reference
 */
$GLOBALS['TL_LANG']['tl_bbk_reminder']['lueneburg']      = 'Lüneburg';
$GLOBALS['TL_LANG']['tl_bbk_reminder']['aurich']         = 'Aurich';
$GLOBALS['TL_LANG']['tl_bbk_reminder']['hildesheim']     = 'Hildesheim';
$GLOBALS['TL_LANG']['tl_bbk_reminder']['cdrom']        = 'CD-ROM';
$GLOBALS['TL_LANG']['tl_bbk_reminder']['dia']        = 'Diareihe';

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_bbk_reminder']['new']        = array('Neuer Rückstand', 'Einen neuen Rückstand anlegen.');
$GLOBALS['TL_LANG']['tl_bbk_reminder']['show']       = array('Rückstanddetails', 'Details des Rückstandes ID %s anzeigen.');
$GLOBALS['TL_LANG']['tl_bbk_reminder']['edit']       = array('Buchung bearbeiten', 'Buchung zu diesem Rückstand  bearbeiten');
$GLOBALS['TL_LANG']['tl_bbk_reminder']['delete']     = array('Rückstand löschen', 'Rückstand ID %s löschen');
$GLOBALS['TL_LANG']['tl_bbk_reminder']['bookingcal']     = array('Buchungskalender anzeigen', 'Buchungskalender zu dieser Rückstandsbuchung anzeigen');
$GLOBALS['TL_LANG']['tl_bbk_reminder']['checkReminder'] = array('Rückstände aktualisieren','Nach neuen Rückständen durchsuchen und anlegen.');

/**
* custom Options
*/
$GLOBALS['TL_LANG']['tl_bbk_reminder']['acceptedOptions'] = array(''=>'nicht bearbeitet', '1'=>'bestätigt', '2'=>'abgelehnt');
