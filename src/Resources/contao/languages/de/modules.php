<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2012 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['bbk'] = array('Bilderbuchkino', '');
$GLOBALS['TL_LANG']['MOD']['bbk_locations'] = array('BBK-Standorte', 'Dieses Modul erlaubt es Ihnen die Standorte mit Bilderbuchkinos zu verwalten.');
$GLOBALS['TL_LANG']['MOD']['bbk_items'] = array('BBK-Einträge', 'Dieses Modul erlaubt es Ihnen die Einträge im Bilderbuchkino zu verwalten.');
$GLOBALS['TL_LANG']['MOD']['bbk_allbookings'] = array('BBK-Buchungen', 'Dieses Modul erlaubt es Ihnen alle Buchungen im Bilderbuchkino zu verwalten.');
$GLOBALS['TL_LANG']['MOD']['bbk_properties'] = array('BBK-Einstellungen', 'Dieses Modul erlaubt es Ihnen alle Einstellungen zum Bilderbuchkino zu verwalten.');
$GLOBALS['TL_LANG']['MOD']['bbk_libraries'] = array('BBK-Bibliotheken', 'Dieses Modul erlaubt es Ihnen alle Bibliotheken zum Bilderbuchkino zu verwalten.');
/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['bbk']      = 'Bilderbuchkino';
$GLOBALS['TL_LANG']['FMD']['bbk_reserv_calendar']    = array('BBK Reservierungs-Kalender', 'Fügt der Seite einen Kalender aller Bilderbuchkino-Reservierungen hinzu.');
$GLOBALS['TL_LANG']['FMD']['bbk_reserv_form']    = array('BBK Reservierungs-Formular', 'ein Formular für die Reservierung eines BBKs');
$GLOBALS['TL_LANG']['FMD']['bbk_details']    = array('BBK Detailansicht', 'eine Detailansicht für die Reservierung eines BBKs');
$GLOBALS['TL_LANG']['FMD']['bbk_list']    = array('BBK Listansicht', 'eine Listansicht für die Reservierung eines BBKs');
$GLOBALS['TL_LANG']['FMD']['bbk_pdf_create_button']    = array('BBK PDF-Erstellen-Button', 'eine PDF von den verfügbaren BBKs erstellen.');
?>