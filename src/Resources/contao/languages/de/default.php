<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2012 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
* Bundeslaender
*/
$GLOBALS['TL_LANG']['DE_STATES']['bw'] = 'Baden-Württemberg';
$GLOBALS['TL_LANG']['DE_STATES']['bayern'] = 'Bayern';
$GLOBALS['TL_LANG']['DE_STATES']['berlin'] = 'Berlin';
$GLOBALS['TL_LANG']['DE_STATES']['brandenburg'] = 'Brandenburg';
$GLOBALS['TL_LANG']['DE_STATES']['bremen'] = 'Bremen';
$GLOBALS['TL_LANG']['DE_STATES']['hamburg'] = 'Hamburg';
$GLOBALS['TL_LANG']['DE_STATES']['hessen'] = 'Hessen';
$GLOBALS['TL_LANG']['DE_STATES']['meckpom'] = 'Mecklenburg-Vorpommern';
$GLOBALS['TL_LANG']['DE_STATES']['niedersachsen'] = 'Niedersachsen';
$GLOBALS['TL_LANG']['DE_STATES']['nw'] = 'Nordrhein-Westfalen';
$GLOBALS['TL_LANG']['DE_STATES']['rheinpfalz'] = 'Rheinland-Pfalz';
$GLOBALS['TL_LANG']['DE_STATES']['saarland'] = 'Saarland';
$GLOBALS['TL_LANG']['DE_STATES']['sachsen'] = 'Sachsen';
$GLOBALS['TL_LANG']['DE_STATES']['sachsenanhalt'] = 'Sachsen-Anhalt';
$GLOBALS['TL_LANG']['DE_STATES']['sh'] = 'Schleswig-Holstein';
$GLOBALS['TL_LANG']['DE_STATES']['tgen'] = 'Thüringen';
$GLOBALS['TL_LANG']['DE_STATES']['notgerman'] = 'nicht aus Deutschland';

$GLOBALS['TL_LANG']['MSC']['bbk_reservform_firstname'] = "Vorname";
$GLOBALS['TL_LANG']['MSC']['bbk_reservform_lastname'] = "Nachname";
$GLOBALS['TL_LANG']['MSC']['bbk_reservform_libname'] = "Bibliothek";
$GLOBALS['TL_LANG']['MSC']['bbk_reservform_email'] = "E-Mail";
$GLOBALS['TL_LANG']['MSC']['bbk_reservform_street'] = "Straße";
$GLOBALS['TL_LANG']['MSC']['bbk_reservform_postal'] = "PLZ";
$GLOBALS['TL_LANG']['MSC']['bbk_reservform_city'] = "Ort";
$GLOBALS['TL_LANG']['MSC']['bbk_reservform_start'] = "Anfang der Reservierung";
$GLOBALS['TL_LANG']['MSC']['bbk_reservform_end'] = "Ende der Reservierung";

$GLOBALS['TL_LANG']['MSC']['bbk_reservform_legend_periodoftime'] = "Zeitraum";
$GLOBALS['TL_LANG']['MSC']['bbk_reservform_legend_deleveryaddress'] = "Lieferadresse";
$GLOBALS['TL_LANG']['MSC']['bbk_reservform_legend_contactdata'] = "Kontaktdaten";
