<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2016 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_bbk_properties']['sender']   = array('E-Mail-Adresse des Versenders', 'Bitte geben Sie die E-Mail-Adresse des Versenders ein.');
$GLOBALS['TL_LANG']['tl_bbk_properties']['senderName']   = array('Name des Versenders', 'Bitte geben Sie die E-Mail-Adresse des Versenders ein.');
$GLOBALS['TL_LANG']['tl_bbk_properties']['confirmed_subject']  = array('E-Mail-Betreff der Bestätigung', 'Hier können Sie den Betreff der E-Mail eintragen.');
$GLOBALS['TL_LANG']['tl_bbk_properties']['confirmed_html_email']         = array('HTML-Version der Bestätigungs-E-Mail', 'Platzhalter: Startdatum=##startDate##, Enddatum=##endDate##, Startzeit=##startTime##, Veranstaltungsende=##endTime##, Vorname=##firstname##, Nachname=##lastname##, Bibliothek=##library##, Straße=##street##, Postleitzahl=##postal##, Ort=##city##, Telefonnummer=##phone##, E-Mail-Adresse=##email##');
$GLOBALS['TL_LANG']['tl_bbk_properties']['confirmed_text_email']         = array('Text-Version der Bestätigungs-E-Mail',  'Platzhalter: Startdatum=##startDate##, Enddatum=##endDate##, Startzeit=##startTime##, Veranstaltungsende=##endTime##, Vorname=##firstname##, Nachname=##lastname##, Bibliothek=##library##, Straße=##street##, Postleitzahl=##postal##, Ort=##city##, Telefonnummer=##phone##, E-Mail-Adresse=##email##');

$GLOBALS['TL_LANG']['tl_bbk_properties']['rejected_subject']  = array('E-Mail-Betreff der Ablehnung', 'Hier können Sie den Betreff der E-Mail eintragen.');
$GLOBALS['TL_LANG']['tl_bbk_properties']['rejected_html_email']         = array('HTML-Version der Ablehnungs-E-Mail', 'Platzhalter: Startdatum=##startDate##, Enddatum=##endDate##, Startzeit=##startTime##, Veranstaltungsende=##endTime##, Vorname=##firstname##, Nachname=##lastname##, Bibliothek=##library##, Straße=##street##, Postleitzahl=##postal##, Ort=##city##, Telefonnummer=##phone##, E-Mail-Adresse=##email##');
$GLOBALS['TL_LANG']['tl_bbk_properties']['rejected_text_email']         = array('Text-Version der Ablehnungs-E-Mail',  'Platzhalter: Startdatum=##startDate##, Enddatum=##endDate##, Startzeit=##startTime##, Veranstaltungsende=##endTime##, Vorname=##firstname##, Nachname=##lastname##, Bibliothek=##library##, Straße=##street##, Postleitzahl=##postal##, Ort=##city##, Telefonnummer=##phone##, E-Mail-Adresse=##email##');

$GLOBALS['TL_LANG']['tl_bbk_properties']['newuser_info_email']   = array('E-Mail-Adresse (Info von Neuregistrierungen)', 'An diese Adresse wird eine Email versendet wenn sich jemand neu registriert.');
$GLOBALS['TL_LANG']['tl_bbk_properties']['newuser_info_name']   = array('Name des Email-Empfängers', 'Bitte geben Sie einen Namen des Email-Empfängers ein.');
$GLOBALS['TL_LANG']['tl_bbk_properties']['newuser_info_subject']  = array('E-Mail-Betreff der Info-Mail', 'Hier können Sie den Betreff der Registrierung-Infoemail eintragen.');

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_bbk_properties']['new_member_info_legend'] = 'neu Registrierung (Einstellungen)';

/**
* DEFAULT-VALUES
*/
$GLOBALS['TL_LANG']['tl_bbk_properties']['confirmed_subject_default'] = 'Bestätigung der Bilderbuchausleihe';
$GLOBALS['TL_LANG']['tl_bbk_properties']['rejected_subject_default'] = 'Ablehnung der Bilderbuchausleihe';
$GLOBALS['TL_LANG']['tl_bbk_properties']['newmember_subject_default'] = 'neue Registrierung auf bz-niedersachsen.de';
