<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_bbk_libraries']['name']      = array('Name der Bibliothek', 'Geben Sie hier den Namen der Bibliothek an die dieses Bilderbuch ausleihen möchte.');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['member_id']      = array('Mitglied-Login zuordnen', 'Legen sie hier für eine Login-Zuordnung fest zu welchem Bibliotheks-Mitgliedzugang der aktuelle Eintrag gehört. ');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['privaddress']       = array('private Adresse', 'als private Adresse markieren');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['firstname']       = array('Vorname', 'Vorname der Kontaktperson');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['lastname']       = array('Nachname', 'Nachname der Kontaktperson');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['street']      = array('Straße', 'Bitte geben Sie den Straßennamen und die Hausnummer ein.');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['postal']      = array('Postleitzahl', 'Bitte geben Sie die Postleitzahl ein.');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['city']        = array('Ort', 'Bitte geben Sie den Namen des Ortes ein.');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['state'] = array('Bundesland', 'Hier können Sie die Bibliothek einem Bundesland zuordnen.');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['phone']       = array('Telefonnummer', 'Bitte geben Sie die Telefonnummer ein.');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['email']   = array('E-Mail-Adresse', 'Bitte geben Sie die E-Mail-Adresse des Abonnenten ein.');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['country'] = array('Land', 'Hier können Sie der Bibliothek ein Land zuordnen.');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['state'] = array('Bundesland', 'Hier können Sie die Bibliothek einem Bundesland zuordnen.');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['mobile']      = array('Handynummer', 'Bitte geben Sie die Handynummer ein.');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['fax']         = array('Faxnummer', 'Bitte geben Sie die Faxnummer ein.');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['website']     = array('Webseite', 'Hier können Sie eine Web-Adresse eingeben.');

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_bbk_libraries']['title_legend']   = 'Titel und Autor';
$GLOBALS['TL_LANG']['tl_bbk_libraries']['layout_legend']  = 'Layoutbereich und Suchbegriffe';

/**
 * Reference
 */
$GLOBALS['TL_LANG']['tl_bbk_libraries']['lueneburg']      = 'Lüneburg';
$GLOBALS['TL_LANG']['tl_bbk_libraries']['aurich']         = 'Aurich';
$GLOBALS['TL_LANG']['tl_bbk_libraries']['hildesheim']     = 'Hildesheim';
$GLOBALS['TL_LANG']['tl_bbk_libraries']['cdrom']        = 'CD-ROM';
$GLOBALS['TL_LANG']['tl_bbk_libraries']['dia']        = 'Diareihe';


/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_bbk_libraries']['new']        = array('Neue Bibliothek', 'Eine neue Bibliothek anlegen');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['show']       = array('Buchungdetails', 'Details der Bibliothek ID %s anzeigen');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['edit']       = array('Bibliothek bearbeiten', 'Bibliothek ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['editheader'] = array('Bibliothek-Einstellungen bearbeiten', 'Einstellungen der Bibliothek ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['copy']       = array('Bibliothek duplizieren', 'Bibliothek ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['cut']        = array('Bibliothek verschieben', 'Bibliothek ID %s verschieben');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['delete']     = array('Bibliothek löschen', 'Bibliothek ID %s löschen');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['toggle']     = array('Bibliothek veröffentlichen/unveröffentlichen', 'Bibliothek ID %s veröffentlichen/unveröffentlichen');

/**
 * CSV-Export
 */
$GLOBALS['TL_LANG']['tl_bbk_reminder']['MESSAGE']['no_data'] = 'Es sind keine Daten zum exportieren vorhanden.';

$GLOBALS['TL_LANG']['tl_bbk_libraries']['csvExport'] = array('CSV-Export', '');
$GLOBALS['TL_LANG']['tl_bbk_libraries']['button_exportcsv'] = 'CSV generieren';
$GLOBALS['TL_LANG']['tl_bbk_libraries']['h2_exportcsv'] = 'Bibliotheken als CSV exportieren';
