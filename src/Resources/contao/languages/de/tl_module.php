<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_module']['bbk_template'] = array('Detailseitentemplate', 'Hier können Sie das Detailseitentemplate auswählen.');
$GLOBALS['TL_LANG']['tl_module']['bbk_pdf_title'] = array('PDF-Titel','Geben Sie hier den Titel der Sammelmapper beim erstellen an.');
$GLOBALS['TL_LANG']['tl_module']['bbk_pdf_keywords'] = array('PDF-Keywords','Geben Sie hier optionale Keywords zur Sammelmappe ein.');
?>