<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    bz-bbk-bundle (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * -------------------------------------------------------------------------
 * Properties
 * -------------------------------------------------------------------------
 */

$GLOBALS['BE_BBK']['PROPERTIES']['ID'] = 1;
$GLOBALS['BE_BBK']['PROPERTIES']['PUBLICSRC'] = 'bundles/srhinowbzbbk';
$GLOBALS['BE_BBK']['CAL']['MONTHS'] = array('1'=>'Jan','2'=>'Feb','3'=>'Mär','4'=>'Apr','5'=>'Mai','6'=>'Jun','7'=>'Jul','8'=>'Aug','9'=>'Sep','10'=>'Okt','11'=>'Nov','12'=>'Dez');
$GLOBALS['BE_BBK']['CAL']['WEEKDAYS'] = array('1'=>'Mo','2'=>'Di','3'=>'Mi','4'=>'Do','5'=>'Fr','6'=>'Sa','7'=>'So');


/**
 * -------------------------------------------------------------------------
 * BACK END MODULES
 * -------------------------------------------------------------------------
 */
if ('BE' === TL_MODE) {
    $GLOBALS['TL_CSS'][] = $GLOBALS['BE_BBK']['PROPERTIES']['PUBLICSRC'].'/css/be.css|static';
}

array_insert($GLOBALS['BE_MOD'], 1, array(
    'bbk' => array
        (
        'bbk_locations' => array (
            'tables' => array('tl_bbk_locations','tl_bbk','tl_bbk_booking','tl_bbk_reminder'),
            'icon'  => $GLOBALS['BE_BBK']['PROPERTIES']['PUBLICSRC'].'/icons/favorite.png',
            'stylesheet' => $GLOBALS['BE_BBK']['PROPERTIES']['PUBLICSRC'].'/css/bbkcal.css|static',
            'showcal' => array('Srhinow\BzBbkBundle\Modules\BeBbkCal','showBeCal'),
            'csvExportBookings' => array('Srhinow\BzBbkBundle\Modules\beExportBookings', 'csvExportBookings'),
            'runList' => array('Srhinow\BzBbkBundle\Modules\beExportBookings', 'runList'),
            'checkReminder'=> array('Srhinow\BzBbkBundle\Modules\BeBbkReminder', 'checkReminder'),
            ),
        'bbk_properties' => array (
            'tables' => array('tl_bbk_properties'),
            'callback' => 'ModuleBBKProperties',
            'icon'  => $GLOBALS['BE_BBK']['PROPERTIES']['PUBLICSRC'].'/icons/process.png',
            ),
        'bbk_libraries' => array (
            'tables' => array('tl_bbk_libraries'),
            'librariesCsvExport' => array('Srhinow\BzBbkBundle\Modules\beExportLibraries', 'csvExport'),
            'icon'  => $GLOBALS['BE_BBK']['PROPERTIES']['PUBLICSRC'].'/icons/users.png',
            ),
        )
    )
);

/**
 * Front end modules
 */
array_insert($GLOBALS['FE_MOD'], 2, array
(
	'bbk' => array
	(
		'bbk_reserv_calendar'    => 'Srhinow\BzBbkBundle\Modules\ModuleBbkReservCalendar',
		'bbk_reserv_form' => 'Srhinow\BzBbkBundle\Modules\ModuleBbkReservationForm',
        'bbk_filter_form' => 'Srhinow\BzBbkBundle\Modules\ModuleBbkFilterForm',
        'bbk_list' => 'Srhinow\BzBbkBundle\Modules\ModuleBbkList',
		'bbk_details' => 'Srhinow\BzBbkBundle\Modules\ModuleBbkDetails',
		'bbk_pdf_create_button' => 'Srhinow\BzBbkBundle\Modules\ModulePdfCreateButton',
        'bbk_import_tags' => 'Srhinow\BzBbkBundle\Helper\ModuleImportTags',
	)
));

/**
 * -------------------------------------------------------------------------
 * Models
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_MODELS']['tl_bbk'] = \Srhinow\BzBbkBundle\Models\BbkModel::class;
$GLOBALS['TL_MODELS']['tl_bbk_booking'] = \Srhinow\BzBbkBundle\Models\BbkBookingModel::class;
$GLOBALS['TL_MODELS']['tl_bbk_libraries'] = \Srhinow\BzBbkBundle\Models\BbkLibrariesModel::class;
$GLOBALS['TL_MODELS']['tl_bbk_locations'] = \Srhinow\BzBbkBundle\Models\BbkLocationsModel::class;
$GLOBALS['TL_MODELS']['tl_bbk_properties'] = \Srhinow\BzBbkBundle\Models\BbkPropertiesModel::class;
$GLOBALS['TL_MODELS']['tl_bbk_reminder'] = \Srhinow\BzBbkBundle\Models\BbkReminderModel::class;

/**
 * -------------------------------------------------------------------------
 * HOOKS
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_HOOKS']['createNewUser'][] = array('srhinow.bzbbk.listener.hooks.new_user','sendNewRegisterNotification');
$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array('srhinow.bzbbk.listener.hooks.insert_tags', 'bbkReplaceInsertTags');
$GLOBALS['TL_HOOKS']['parseTemplate'][] = array('srhinow.bzbbk.listener.hooks.parse_template', 'redirectBbkDeeplink');
