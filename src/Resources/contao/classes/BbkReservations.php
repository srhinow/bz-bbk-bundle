<?php
/**
* PHP version 7.2
* @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
* @author     Sven Rhinow
* @package    bz-bbk-bundle (BilderBuchKino)
* @license    commercial
* @filesource
*/

namespace Srhinow\BzBbkBundle;

use Contao\Database;
use Contao\Module;
use Contao\StringUtil;

/**
 * Class Events
 *
 * Provide methods to get all events of a certain period from the database.
 */
abstract class BbkReservations extends Module
{

	/**
	 * Current URL
	 * @var string
	 */
	protected $strUrl;

	/**
	 * Today 00:00:00
	 * @var string
	 */
	protected $intTodayBegin;

	/**
	 * Today 23:59:59
	 * @var string
	 */
	protected $intTodayEnd;

	/**
	 * Current events
	 * @var array
	 */
	protected $arrReserv = array();


	/**
	 * Get all events of a certain period
	 * @param array
	 * @param integer
	 * @param integer
	 * @return array
	 */
	protected function getAllReserv($currBBK, $intStart, $intEnd)
	{
		$time = time();
		$this->arrReserv = array();

        if(is_object($currBBK) && $currBBK->id)
        {
		    $id = $currBBK->id;

		    // Get events of the current period
		    $objReserv = Database::getInstance()->prepare("SELECT *, (SELECT title FROM tl_bbk WHERE id=?) AS book FROM tl_bbk_booking WHERE pid=? AND ((startDate>=? AND startDate<=?) OR (endDate>=? AND endDate<=?) OR (startDate<=? AND endDate>=?)) AND `accepted`=? ORDER BY startDate")
									    ->execute($id, $id, $intStart, $intEnd, $intStart, $intEnd, $intStart, $intEnd,1);

		    #print_r($objReserv);
		    if ($objReserv->numRows > 0)
		    {

                while ($objReserv->next())
                {
                    $this->addReserv($objReserv, $objReserv->startDate, $objReserv->endDate, $intStart, $intEnd, $id);

                }

                // Sort the array
                foreach (array_keys($this->arrReserv) as $key)
                {
                    ksort($this->arrReserv[$key]);
                }

		    }
		    // HOOK: modify the result set
		    if (isset($GLOBALS['TL_HOOKS']['getAllBookReservations']) && is_array($GLOBALS['TL_HOOKS']['getAllBookReservations']))
		    {
			    foreach ($GLOBALS['TL_HOOKS']['getAllBookReservations'] as $callback)
			    {
				    $this->import($callback[0]);
				    $this->arrReserv = $this->$callback[0]->$callback[1]($this->arrReserv, $intStart, $intEnd, $this);
			    }
		    }
        }
		return $this->arrReserv;
	}


	/**
	 * Add an event to the array of active events
	 * @param Database_Result
	 * @param integer
	 * @param integer
	 * @param string
	 * @param integer
	 * @param integer
	 * @param integer
	 */
	protected function addReserv(Database_Result $objReserv, $intStart, $intEnd, $intBegin, $intLimit, $intCalendar)
	{
		global $objPage;

		$intDate = $intStart;
		$intKey = date('Ymd', $intStart);
		$span = sbcHelper::calculateSpan($intStart, $intEnd);
		$strDate = $this->parseDate($objPage->dateFormat, $intStart);
		$strDay = $GLOBALS['TL_LANG']['DAYS'][date('w', $intStart)];
		$strMonth = $GLOBALS['TL_LANG']['MONTHS'][(date('n', $intStart)-1)];

		if ($span > 0)
		{
			$strDate = $this->parseDate($objPage->dateFormat, $intStart) . ' - ' . $this->parseDate($objPage->dateFormat, $intEnd);
			$strDay = '';
		}

		$strTime = '';

		if ($objReserv->addTime)
		{
			if ($span > 0)
			{
				$strDate = $this->parseDate($objPage->datimFormat, $intStart) . ' - ' . $this->parseDate($objPage->datimFormat, $intEnd);
			}
			elseif ($intStart == $intEnd)
			{
				$strTime = $this->parseDate($objPage->timeFormat, $intStart);
			}
			else
			{
				$strTime = $this->parseDate($objPage->timeFormat, $intStart) . ' - ' . $this->parseDate($objPage->timeFormat, $intEnd);
			}
		}

		// Store raw data
		$arrReserv = $objReserv->row();

		// Overwrite some settings
		$arrReserv['time'] = $strTime;
		$arrReserv['date'] = $strDate;
		$arrReserv['day'] = $strDay;
		$arrReserv['month'] = $strMonth;
		$arrReserv['parent'] = $intCalendar;
		$arrReserv['link'] = $objReserv->title;
		$arrReserv['title'] = specialchars($objReserv->title, true);
		$arrReserv['class'] = ($objReserv->cssClass != '') ? ' ' . $objReserv->cssClass : '';
		$arrReserv['details'] = StringUtil::encodeEmail($objReserv->details);
		$arrReserv['start'] = $intStart;
		$arrReserv['end'] = $intEnd;

		// Get todays start and end timestamp
		if ($this->intTodayBegin === null)
		{
			$this->intTodayBegin = strtotime('00:00:00');
		}
		if ($this->intTodayEnd === null)
		{
			$this->intTodayEnd = strtotime('23:59:59');
		}

		// Mark past and upcoming events (see #3692)
		if ($intEnd < $this->intTodayBegin)
		{
			$arrReserv['class'] .= ' bygone';
		}
		elseif ($intStart > $this->intTodayEnd)
		{
			$arrReserv['class'] .= ' upcoming';
		}
		else
		{
			$arrReserv['class'] .= ' current';
		}

		$this->arrReserv[$intKey][$intStart][] = $arrReserv;

		// Multi-day event
		for ($i=1; $i<=$span && $intDate<=$intLimit; $i++)
		{
			// Only show first occurrence
			if ($this->cal_noSpan && $intDate >= $intBegin)
			{
				break;
			}

			$intDate = strtotime('+ 1 day', $intDate);
			$intNextKey = date('Ymd', $intDate);

			$this->arrReserv[$intNextKey][$intDate][] = $arrReserv;
		}
	}


	/**
	 * Return the begin and end timestamp and an error message as array
	 * @param Date
	 * @param string
	 * @return array
	 */
	protected function getDatesFromFormat(Date $objDate, $strFormat)
	{
		switch ($strFormat)
		{
			case 'cal_day':
				return array($objDate->dayBegin, $objDate->dayEnd, $GLOBALS['TL_LANG']['MSC']['cal_emptyDay']);
				break;

			default:
			case 'cal_month':
				return array($objDate->monthBegin, $objDate->monthEnd, $GLOBALS['TL_LANG']['MSC']['cal_emptyMonth']);
				break;

			case 'cal_year':
				return array($objDate->yearBegin, $objDate->yearEnd, $GLOBALS['TL_LANG']['MSC']['cal_emptyYear']);
				break;

			case 'cal_all': // 1970-01-01 00:00:00 - 2038-01-01 00:00:00
				return array(0, 2145913200, $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_7':
				$objToday = new Date();
				return array($objToday->dayBegin, (strtotime('+7 days', $objToday->dayBegin) - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_14':
				$objToday = new Date();
				return array($objToday->dayBegin, (strtotime('+14 days', $objToday->dayBegin) - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_30':
				$objToday = new Date();
				return array($objToday->dayBegin, (strtotime('+1 month', $objToday->dayBegin) - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_90':
				$objToday = new Date();
				return array($objToday->dayBegin, (strtotime('+3 months', $objToday->dayBegin) - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_180':
				$objToday = new Date();
				return array($objToday->dayBegin, (strtotime('+6 months', $objToday->dayBegin) - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_365':
				$objToday = new Date();
				return array($objToday->dayBegin, (strtotime('+1 year', $objToday->dayBegin) - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_two':
				$objToday = new Date();
				return array($objToday->dayBegin, (strtotime('+2 years', $objToday->dayBegin) - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_cur_month':
				$objToday = new Date();
				return array($objToday->dayBegin, $objToday->monthEnd, $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_cur_year':
				$objToday = new Date();
				return array($objToday->dayBegin, $objToday->yearEnd, $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_all': // 2038-01-01 00:00:00
				$objToday = new Date();
				return array($objToday->dayBegin, 2145913200, $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_7':
				$objToday = new Date();
				return array((strtotime('-7 days', $objToday->dayBegin) - 1), ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_14':
				$objToday = new Date();
				return array((strtotime('-14 days', $objToday->dayBegin) - 1), ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_30':
				$objToday = new Date();
				return array((strtotime('-1 month', $objToday->dayBegin) - 1), ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_90':
				$objToday = new Date();
				return array((strtotime('-3 months', $objToday->dayBegin) - 1), ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_180':
				$objToday = new Date();
				return array((strtotime('-6 months', $objToday->dayBegin) - 1), ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_365':
				$objToday = new Date();
				return array((strtotime('-1 year', $objToday->dayBegin) - 1), ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_two':
				$objToday = new Date();
				return array((strtotime('-2 years', $objToday->dayBegin) - 1), ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_cur_month':
				$objToday = new Date();
				return array($objToday->monthBegin, ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_cur_year':
				$objToday = new Date();
				return array($objToday->yearBegin, ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_all': // 1970-01-01 00:00:00
				$objToday = new Date();
				return array(0, ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;
		}
	}
}
