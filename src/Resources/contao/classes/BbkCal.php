<?php

/**
 * PHP version 7
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    bz-bbk-bundle (BilderBuchKino)
 * @license    commercial
 * @filesource
 */
namespace Srhinow\BzBbkBundle;


use Contao\Config;
use Contao\Controller;
use Contao\Database;
use Contao\Date;
use Contao\StringUtil;
use Database\Result;
use Srhinow\BzBbkBundle\Helper\BbkHelper;

class BbkCal extends Controller
{

	protected $firstMon = '';

    protected $lastMon = '';

    protected $bookingDays = array();

	/**
	* create Calendar in HTML like GuestCal
	* @var string
	* @var string
	* @return string
	*/
	public function createCal()
	{
        if(!$this->getFirstMon()) $this->setFirstMon(date('m.Y'));
        $dateFormat = $this->Config->get('dateFormat');
		$timeStart = strtotime('01.'.$this->getFirstMon());
		$dateStart = date('Ymd',$timeStart);
		$dateStartMonth = date('m',$timeStart);
	    $dateStartDay = date('d',$timeStart);
	    $dateStartYear = $curyear = date('Y',$timeStart);

		$timeCur = $timeStart;
		$curmon = $checkmon = '';
		$table = $thead = $tbody = '';
		$tooltipTitel = $tooltipText = '';
		$dayArr = array();

		//monate ergänzen bis auf den 12 Monat des angefangenen Jahres
		if($this->getLastMon() === '')
		{
			$timeEnd = strtotime('+12 months',$timeStart);
			$endmonth = date('n',$timeEnd);
			$rest = 12-$endmonth;
			$formonth = 12 + $rest + 1;
		}
		else
		{
		   $formonth = (int) $this->getLastMon();
		}

		for($d=1; $d<=31; $d++) $dayArr[] = str_pad($d, 2, '0', STR_PAD_LEFT);

		reset($dayArr);

		//Header
		foreach($dayArr as $day) $thead .= '<th class="day'.$day.'">'.$day.'</th>';
		$thead = '<thead><tr><th>&nbsp;</td>'.$thead.'</tr>';

		//Body with Month and events
		for($m=1; $m <= $formonth; $m++) {
            $curmon = (!$curmon) ? $dateStartMonth : $curmon + 1;
            $checkmon = (!$checkmon) ? $curmon : date('m', $timeCur);

            // hole alle Buchungen für den aktuellen Monat
            $this->Date = new Date($timeCur);
            $arrAllReserv = $this->getAllReserv($this->Date->monthBegin, $this->Date->monthEnd);

            $row = '<td class="">' . $GLOBALS['BE_BBK']['CAL']['MONTHS'][date('n', $timeCur)] . '</td>';

            for ($d = 1; $d <= 31; $d++) {
                if (date('Y', $timeCur) != $curyear) $curyear++;

                $timeCur = mktime(0, 0, 0, $checkmon, $d, $curyear);
                $dateCur = date('Ymd', $timeCur);


                $weclass = in_array(date('N', $timeCur), array(6, 7)) ? 'we' : 'wd'; //we = weekend , wd = weekday

                if (checkdate($checkmon, $d, date('Y', $timeCur))) {

                    if (isset($arrAllReserv[$dateCur])) {
                        if (TL_MODE == 'BE') {
                            $tooltipTitel = date($dateFormat, (int) $arrAllReserv[$dateCur][0]['startShippingDate']) . ' - ' . date($dateFormat, (int) $arrAllReserv[$dateCur][0]['endShippingDate']);
                            $html = '<p>';
                            if ($arrAllReserv[$dateCur][0]['accepted']) {
                                if ((int)$arrAllReserv[$dateCur][0]['returnDate'] > 0) {
                                    $html .= 'im Haus: ' . (($arrAllReserv[$dateCur][0]['inHouse'] == 1) ? 'ja, zurück am '
                                            . date($dateFormat, (int) $arrAllReserv[$dateCur][0]['returnDate']) : 'nein') . '<br>';
                                } else {
                                    $html .= 'im Haus: ' . (($arrAllReserv[$dateCur][0]['inHouse'] == 1) ? 'ja ' : 'nein') . '<br>';

                                }
                            }
                            $html .= $arrAllReserv[$dateCur][0]['library'] . '<br>';
                            $html .= $arrAllReserv[$dateCur][0]['firstname'] . ' ' . $arrAllReserv[$dateCur][0]['lastname'] . '<br>';
                            $html .= $arrAllReserv[$dateCur][0]['street'] . '<br>';
                            $html .= $arrAllReserv[$dateCur][0]['postal'] . ' ' . $arrAllReserv[$dateCur][0]['city'] . '<br>';
                            $html .= 'Tel.: ' . $arrAllReserv[$dateCur][0]['phone'] . '<br>';
                            $html .= 'E-Mail: ' . $arrAllReserv[$dateCur][0]['email'] . '<br>';
                            $html .= 'Memo: ' . $arrAllReserv[$dateCur][0]['memo'] . '<br>';
                            $html .= '</p>';
                            $tooltipText = htmlspecialchars($html);

                            $acceptedClass = ($arrAllReserv[$dateCur][0]['accepted']) ? ' accepted' : ' unfinished';
                            $script = ' onclick="window.location = \'contao/main.php?do=bbk_locations&table=tl_bbk_booking&act=edit&id=' . $arrAllReserv[$dateCur][0]['id'] . '&rt=' . $_GET['rt'] . '&' . $_GET['ref'] . '\'"';

                            $row .= '<td class="tooltip booked ' . $weclass . $acceptedClass . '" title="' . $tooltipTitel . '" rel="' . $tooltipText . '" ' . $script . '>' . $GLOBALS['BE_BBK']['CAL']['WEEKDAYS'][date('N', (int) $timeCur)] . '</td>';

                        } else {
                            $row .= '<td class="booked ' . $weclass . '">' . $GLOBALS['BE_BBK']['CAL']['WEEKDAYS'][date('N', (int) $timeCur)] . '</td>';
                        }

                    } else {
                        $row .= '<td class="free ' . $weclass . '">' . $GLOBALS['BE_BBK']['CAL']['WEEKDAYS'][date('N', (int) $timeCur)] . '</td>';
                    }

                } else {
                    $row .= '<td class="empty ' . $weclass . '">&nbsp;</td>';
                }

            }
            $tbody .= '<tr>' . $row . '</tr>';
            $timeCur = mktime(0, 0, 0, $checkmon, $d, $curyear);

        }

		$tbody = '<tbody>'.$tbody.'</tbody>';

        $table = '<table id="bbkreservcal">'.$thead.$tbody.'</table>';

	    return $table;
	}

	/**
	 * Get all events of a certain period
	 * @param array
	 * @param integer
	 * @param integer
	 * @return array
	 */
	protected function getAllReserv($intStart, $intEnd)
	{
	    $time = time();
	    $this->arrReserv = array();

	    if($this->Input->get('bbk'))
	    {
            $bbk = $this->Input->get('bbk');

            // Get events of the current period
            $objReserv = Database::getInstance()->prepare("SELECT `tl_bbk_booking`.* FROM `tl_bbk_booking` LEFT JOIN `tl_bbk` ON `tl_bbk`.`id` = `tl_bbk_booking`.`bbk` WHERE `tl_bbk_booking`.`bbk`=? AND `accepted` != ? AND ((startShippingDate>=? AND startShippingDate<=?) OR (endShippingDate>=? AND endShippingDate<=?) OR (startShippingDate<=? AND endShippingDate>=?)) ORDER BY startShippingDate")
                                        ->execute($bbk, 2, $intStart, $intEnd, $intStart, $intEnd, $intStart, $intEnd);

            if ($objReserv->numRows > 0)
            {
                while ($objReserv->next())
                {

                // wenn die Reservierung Monatsuebergreifend ist, den Monatsbeginn nehmen
                $intStart =  $objReserv->startShippingDate;

                // wenn die Reservierung Monatsuebergreifend ist, das Monatsende nehmen
                $intEnd   = $objReserv->endShippingDate;

                $this->addReserv($objReserv, $objReserv->startShippingDate, $objReserv->endShippingDate, $intStart, $intEnd);
                }

                // Sort the array
                foreach (array_keys($this->arrReserv) as $key)
                {
                    ksort($this->arrReserv[$key]);
                }

            }

            // HOOK: modify the result set
            if (isset($GLOBALS['TL_HOOKS']['getAllBookReservations']) && is_array($GLOBALS['TL_HOOKS']['getAllBookReservations']))
            {
                foreach ($GLOBALS['TL_HOOKS']['getAllBookReservations'] as $callback)
                {
                    $this->import($callback[0]);
                    $this->arrReserv = $this->$callback[0]->$callback[1]($this->arrReserv, $intStart, $intEnd, $this);
                }
            }
	    }
	    return $this->arrReserv;
	}

	/**
	 * Add an event to the array of active events
	 * @param Database_Result
	 * @param integer
	 * @param integer
	 * @param string
	 * @param integer
	 * @param integer
	 * @param integer
	 */
	protected function addReserv(Result $objReserv, $intStart, $intEnd, $intBegin, $intLimit)
	{
//        $config = $this->framework->getAdapter(Config::class);

        $dateFormat = $this->Config->get('dateFormat');
		$intDate = $intBegin;
		$intKey = date('Ymd', $intStart);
		$span = BbkHelper::calculateSpan($intBegin, $intLimit);
		$strDate = $this->parseDate($dateFormat, $intStart);
		$strDay = $GLOBALS['TL_LANG']['DAYS'][date('w', $intStart)];
		$strMonth = $GLOBALS['TL_LANG']['MONTHS'][(date('n', $intStart)-1)];

		if ($span > 0)
		{
			$strDate = $this->parseDate($dateFormat, $intStart) . ' - ' . $this->parseDate($dateFormat, $intEnd);
			$strDay = '';
		}

		// Store raw data
		$arrReserv = $objReserv->row();

		// Overwrite some settings
		$arrReserv['date'] = $strDate;
		$arrReserv['link'] = $objReserv->title;
		$arrReserv['title'] = specialchars($objReserv->title, true);
		$arrReserv['class'] = ($objReserv->cssClass != '') ? ' ' . $objReserv->cssClass : '';
		$arrReserv['details'] = StringUtil::encodeEmail($objReserv->details);
		$arrReserv['start'] = $intStart;
		$arrReserv['end'] = $intEnd;

		// Get todays start and end timestamp
		if ($this->intTodayBegin === null)
		{
			$this->intTodayBegin = strtotime('00:00:00');
		}
		if ($this->intTodayEnd === null)
		{
			$this->intTodayEnd = strtotime('23:59:59');
		}


		$intNextKey = date('Ymd', $intDate);
                $this->arrReserv[$intNextKey][] = $arrReserv;

		// Multi-day event
		for ($i=1; $i <= $span && $intDate <= $intLimit; $i++)
		{
			$intDate = strtotime('+ 1 day', $intDate);
			$intNextKey = date('Ymd', $intDate);

			$this->arrReserv[$intNextKey][] = $arrReserv;
		}
	}

    /**
     * @return string
     */
    public function getFirstMon()
    {
        return $this->firstMon;
    }

    /**
     * @param string $firstMon
     */
    public function setFirstMon($firstMon)
    {
        $this->firstMon = $firstMon;
    }

    /**
     * @return string
     */
    public function getLastMon()
    {
        return $this->lastMon;
    }

    /**
     * @param string $lastMon
     */
    public function setLastMon($lastMon)
    {
        $this->lastMon = $lastMon;
    }

    /**
     * @return array
     */
    public function getBookingDays()
    {
        return $this->bookingDays;
    }

    /**
     * @param array $bookingDays
     */
    public function setBookingDays($bookingDays)
    {
        $this->bookingDays = $bookingDays;
    }
}
