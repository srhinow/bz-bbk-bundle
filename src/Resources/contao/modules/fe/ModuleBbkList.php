<?php

/**
 * PHP version 7.2
 * @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

namespace Srhinow\BzBbkBundle\Modules;

use Contao\BackendTemplate;
use Contao\FilesModel;
use Contao\Module;
use Contao\PageModel;
use Contao\Pagination;
use Srhinow\BzBbkBundle\Models\BbkLocationsModel;
use Srhinow\BzBbkBundle\Models\BbkModel;

/**
 * Class ModuleBbkList
 *
 * Front end module "bz-bbk-bundle"
 */
class ModuleBbkList extends Module
{
	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'bbk_list';

	/**
	 * Template
	 * @var string
	 */
	protected $strFormId = 'BBK_SEARCH_FORM';

	/**
	 * Target pages
	 * @var array
	 */
	protected $arrTargets = array();

        /**
        * Table with BilderBuchKino entries
        * @var string
        */
        protected $curr_table = 'tl_bbk';

	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### BBK-LISTE ###';

			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'typolight/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}

        // Fallback template
		if (strlen($this->bbk_template)) $this->strTemplate = $this->bbk_template;

		return parent::generate();
	}


	/**
	 * Generate module
	 */
	protected function compile()
	{
		$this->loadLanguageFile($this->curr_table);
		$this->loadDataContainer($this->curr_table);

		$searchWhereArr = array();
		$searchWhereStr  = '';
		$offset = intval($this->skipFirst);
		$limit = null;
		$this->Template->libraries = array();

		// Maximum number of items
		if ($this->numberOfItems > 0)
		{
			$limit = $this->numberOfItems;
		}

		//aktuelle Listen-Url in Cookie speichern
		if(!stristr($this->Environment->requestUri, 'pdf=')) {
            $cookieLifeTime = time()+3600*24*7;
            $this->setCookie(
                'aktlisturl',
                $this->Environment->url.$this->Environment->requestUri,
                $cookieLifeTime
            );
        }

		//wenn ein Cookie existiert und kein querystring übergeben wurde dann zur letzten Suche weiterleiten
		if(
		    isset($_COOKIE['aktlisturl'])
            && $this->Environment->queryString == ''
            && !stristr($_COOKIE['aktlisturl'], 'pdf=')
            && $_COOKIE['aktlisturl'] != $this->Environment->url.$this->Environment->requestUri
        )
		{
			$this->redirect($_COOKIE['aktlisturl']);
		}

		$this->Template->formSend = (\Input::get('FORM_SUBMIT') == $this->strFormId || \Input::get('tag')) ? true : false;

		if((\Input::get('FORM_SUBMIT') == $this->strFormId) || \Input::get('tag'))
		{
            $tagsFound = '';

		    //only entries with this tag
		    if(\Input::get('tag') || \Input::get('keyword'))
		    {
			  $tag = (\Input::get('tag')) ? \Input::get('tag') : \Input::get('keyword');
			  $this->Template->keyword = $tag;

			  //get entries with current tag
			  $tagObj = $this->Database->prepare('SELECT `tid` FROM `tl_tag` WHERE `from_table`= ? AND `tag` LIKE ?')
						  ->execute($this->curr_table,'%%'.$tag.'%%');
			  if($tagObj->numRows > 0)
			  {
			      $bbkIdArr = array();
			      while($tagObj->next()) $bbkIdArr[] = $tagObj->tid;

			      $tagsFound = " OR `id` IN (".implode(',',$bbkIdArr).")";
			  }

		    }

			//only ads with posted mediatyp
			if(\Input::get('mediatyp'))
			{
				$searchWhereArr[] = "`mediatyp` = '".strip_tags(stripslashes(\Input::get('mediatyp')) )."'";
			}
			$this->Template->mediatyp_label = '';

			//only ads with posted agereco
			if(\Input::get('agereco'))
			{
				$searchWhereArr[] = "`ageRecommendation` = '".strip_tags(stripslashes(\Input::get('agereco')) )."'";
			}
			$this->Template->agereco_label = '';

			//only ads with posted location
			if(\Input::get('location'))
			{
				$searchWhereArr[] = "`location` = '".strip_tags(stripslashes(\Input::get('location')) )."'";
			}

			//only ads with posted mediatyp
			if(\Input::get('zeitspanne'))
			{
				$zstime = strtotime(stripslashes(\Input::get('zeitspanne')));
				$searchWhereArr[] = "`created_tstamp` > '".$zstime."'";
			}

		    //only ads with posted keyword
		    if(isset($tag))
		    {
				$searchInArr = array('bbkNr','barcode','title','author','otherPerson1','otherPerson2','publisher','annotation','releaseDate','ageRecommendation');
				$searchWhereArr[] =  "(CONCAT(".implode(',',$searchInArr).") LIKE '%%".strtolower($tag)."%%' OR `collation` LIKE '%%".$tag."%%'$tagsFound)";
		    }

		    $searchWhereArr[] = "`published` = 1";
			// Get the total number of items
			$intTotal = BbkModel::countBbkEntries($searchWhereArr);

			// Filter anwenden um die Gesamtanzahl zuermitteln
			if((int) $intTotal > 0)
			{

				$total = $intTotal - $offset;

				// Split the results
				if ($this->perPage > 0 && (!isset($limit) || $this->numberOfItems > $this->perPage))
				{

					// Adjust the overall limit
					if (isset($limit))
					{
						$total = min($limit, $total);
					}

					// Get the current page
					$id = 'page_n' . $this->id;
					$page = \Input::get($id) ?: 1;

					// Do not index or cache the page if the page number is outside the range
					if ($page < 1 || $page > max(ceil($total/$this->perPage), 1))
					{
						global $objPage;

						$objPage->noSearch = 1;
						$objPage->cache = 0;

						$objTarget = PageModel::findByPk($objPage->id);
						if ($objTarget !== null)
						{
							$reloadUrl = ampersand($this->generateFrontendUrl( $objTarget->row() ) );
						}

						$this->redirect($reloadUrl);
					}

					// Set limit and offset
					$limit = $this->perPage;
					$offset += (max($page, 1) - 1) * $this->perPage;
					$skip = intval($this->skipFirst);

					// Overall limit
					if ($offset + $limit > $total + $skip)
					{
						$limit = $total + $skip - $offset;
					}

					// Add the pagination menu
					$objPagination = new Pagination($total, $this->perPage, $GLOBALS['TL_CONFIG']['maxPaginationLinks'], $id);
					$this->Template->pagination = $objPagination->generate("\n  ");
				}


				// Get the items
				if (isset($limit))
				{
					$resultObj = BbkModel::findBbks($limit, $offset, $searchWhereArr);

				}
				else
				{
					$resultObj = BbkModel::findBbks(0, $offset, $searchWhereArr);

				}

				//create empty item-Array
                $itemArr = array();
                $arrFields = array();
			    $headFields = array('imageUrl','bbkNr','ageRecommendation','title','location','mediatyp');
			    $limit = BbkModel::countBbkEntries($searchWhereArr);
			    $count = -1;
			    $ageRecos = array();

				while($resultObj->next())
				{
					//row - Class
					$class = 'row_' . ++$count . (($count == 0) ? ' row_first' : '') . (($count >= ($limit - 1)) ? ' row_last' : '') . ((($count % 2) == 0) ? ' even' : ' odd');

					//get tags
					$tags = array();
					$tagObj = $this->Database->prepare('SELECT `tag` FROM `tl_tag` WHERE `from_table`=? AND `tid`=?')
								 ->execute($this->curr_table, $resultObj->id);
					if($tagObj->numRows > 0)
					{
					    while($tagObj->next())
					    {
							$tags[] = $tagObj->tag;
					    }
					}
					$this->Template->tags = $tags;

					//get row-Data
					reset($headFields);
					foreach ($headFields as $k)
					{
						$v = $resultObj->$k;
                        $fieldDca = $GLOBALS['TL_DCA'][$this->curr_table]['fields'][$k];
                        if(!isset($fieldDca)) {
                            continue;
                        }

						// Never show passwords
						if (isset($fieldDca['inputType']) && $fieldDca['inputType'] == 'password')
						{
							--$limit;
							continue;
						}


						if($k == 'imageUrl')
						{
                            $objFile = FilesModel::findByUuid($v);

						    if ($objFile !== null)
                            {
                                $v = $objFile->path;
                            } else {
                                $v = $GLOBALS['BE_BBK']['PROPERTIES']['PUBLICSRC'] . '/icons/'. (($resultObj->mediatyp == 'cdrom') ? 'cdrom.png': 'dia.png');
                            }
						}

                        if ($k == 'location'){
                            $objLocation = BbkLocationsModel::findByIdOrAlias($v);
                            if (null !== $objLocation) {
                                $v = $objLocation->name;
                            }
                        }

                        $objDetailPage = \PageModel::findByPk($this->jumpTo);
                        $detailUrl = ampersand( $this->generateFrontendUrl($objDetailPage->row(),'/bbk/'.$resultObj->id) );

						$arrFields[$k] = array
						(
							'raw' => $v,
							'label' => (strlen($label = $GLOBALS['TL_DCA'][$this->curr_table]['fields'][$k]['label'][0]) ? $label : $k),
							'content' => $this->formatValue($k, $v, true),
							'class' => $class,
							'url' => $detailUrl,
						);

					}

					$itemArr[$class] = $arrFields;
			    }

                $this->Template->headFields = $arrFields;
                $this->Template->record = $itemArr;
		    	$this->Template->message = 'Eintr&auml;ge: '.$total;
			}
			else
			{
				$this->Template->message = "Leider liegen für diese Suchanfrage keine Ergebnisse bereit. Erweitern Sie evtl ihre Suchkriterien.";

			}

		    $this->Template->totalItems = $intTotal;


		}
		else
		{
		    $this->Template->message = "Durch eine entsprechende Vorauswahl der einzelnen Suchkriterien haben Sie hier die Möglichkeit Ihre Recherche im Katalog näher einzugrenzen.";
		}

	}
	/**
	 * Format a value
	 * @param string
	 * @param mixed
	 * @param boolean
	 * @return mixed
	 */
	protected function formatValue($k, $value, $blnListSingle=false)
	{
		$value = deserialize($value);

		// Return if empty
		if (empty($value))
		{
			return '';
		}

		global $objPage;

        $fieldDca = $GLOBALS['TL_DCA'][$this->curr_table]['fields'][$k];

		// Array
		if (is_array($value))
		{
			$value = implode(', ', $value);
		}
		// Date
		elseif (isset($fieldDca['eval']['rgxp']) && $fieldDca['eval']['rgxp'] == 'date')
		{
			$value = \Date::parse($objPage->dateFormat, $value);
		}

		// Time
		elseif (isset($fieldDca['eval']['rgxp']) && $fieldDca['eval']['rgxp'] == 'time')
		{
			$value = \Date::parse($objPage->timeFormat, $value);
		}

		// Date and time
		elseif (isset($fieldDca['eval']['rgxp']) && $fieldDca['eval']['rgxp'] == 'datim')
		{
			$value = \Date::parse($objPage->datimFormat, $value);
		}

		// URLs
		elseif (isset($fieldDca['eval']['rgxp']) && $fieldDca['eval']['rgxp'] == 'url' && preg_match('@^(https?://|ftp://)@i', $value))
		{
			global $objPage;
			$value = '<a href="' . $value . '"' . (($objPage->outputFormat == 'xhtml') ? ' onclick="return !window.open(this.href)"' : ' target="_blank"') . '>' . $value . '</a>';
		}

		// E-mail addresses
		elseif (isset($fieldDca['eval']['rgxp']) && $fieldDca['eval']['rgxp'] == 'email')
		{
			$value = \StringUtil::encodeEmail($value);
			$value = '<a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;' . $value . '">' . $value . '</a>';
		}

		// Reference
		elseif (isset($fieldDca['reference']) && is_array($fieldDca['reference']))
		{
			$value = $fieldDca['reference'][$value];
		}

		// Associative array
		elseif (isset($fieldDca['eval']['isAssociative']) || (isset($fieldDca['options']) && array_is_assoc($fieldDca['options'])))
		{
			if ($blnListSingle)
			{
				$value = $fieldDca['options'][$value];
			}
			else
			{
				$value = '<span class="value">[' . $value . ']</span> ' . $fieldDca['options'][$value];
			}
		}

		return $value;
	}

}
