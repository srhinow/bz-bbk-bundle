<?php

/**
 * PHP version 7.2
 * @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

namespace Srhinow\BzBbkBundle\Modules;

use Contao\BackendTemplate;
use Contao\Input;
use Contao\Module;
use Contao\StringUtil;
use Contao\TagModel;
use Dompdf\Dompdf;
use Srhinow\BzBbkBundle\Models\BbkModel;

/**
 * Class Module PDF-Button
 *
 * Front end module "BBK"
 */
class ModulePdfCreateButton extends Module
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'bbk_pdf_create_button';

    /**
     * Table with BilderBuchKino entries
     * @var string
     */
    protected $curr_table = 'tl_bbk';

    /**
     * importand Fieldnames
     * @var array
     */
    protected $headFields = array('bbkNr','ageRecommendation','title','location','mediatyp');

    /**
     * max entry fpr pdf-file
     * @var int
     */
    protected $maxForPdf = 500;

    protected $tdWidthArr = array('50px', '90px','180px','70px','70px');

    /**
     * Check whether the article is published
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### PDF-Button (PDF erstellen) ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'typolight/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }
        // Fallback template
        if (strlen($this->pdf_template)) $this->strTemplate = $this->pdf_template;

        return parent::generate();
    }


    /**
     * Generate the module
     */
    protected function compile()
    {
        $this->loadLanguageFile($this->curr_table);
        $this->loadDataContainer($this->curr_table);

        $strContent = $tagsFound = '';
        $itemArr = array();
        $arrFields = array();
        $headtr = $bodytr = '';

        $showPdfLink = (Input::get('mediatyp') || Input::get('agereco') || Input::get('location') || Input::get('zeitspanne') || Input::get('keyword') || Input::get('tag')) ? true : false;
        $c =-1;

        if(Input::get('pdf') == 'create')
        {

            //only entries with this tag
            if(Input::get('tag') || Input::get('keyword'))
            {

                $tag = (Input::get('tag')) ? Input::get('tag') : Input::get('keyword');
                $this->Template->keyword = $tag;

                //get entries with current tag
                $tagObj = TagModel::findByIdAndTable($tag, $this->curr_table);
//                $tagObj = $this->Database->prepare('SELECT `tid` FROM `tl_tag` WHERE `from_table`= ? AND `tag` LIKE ?')
//						      ->execute($this->curr_table,'%%'.$tag.'%%');

                if(null !== $tagObj)
                {
                    $bbkIdArr = array();
                    while($tagObj->next()) $bbkIdArr[] = $tagObj->tid;

                    $tagsFound = " OR `id` IN (".implode(',',$bbkIdArr).")";
                }


            }

            //only ads with posted mediatyp
            if(Input::get('mediatyp'))
            {
                $searchWhereArr[] = "`mediatyp` = '".stripslashes(Input::get('mediatyp'))."'";
            }
            $this->Template->mediatyp_label = '';

            //only ads with posted agereco
            if(Input::get('agereco'))
            {
                $searchWhereArr[] = "`ageRecommendation` = '".stripslashes(Input::get('agereco'))."'";
            }
            $this->Template->agereco_label = '';

            //only ads with posted mediatyp
            if(Input::get('location'))
            {
                $searchWhereArr[] = "`location` = '".stripslashes(Input::get('location'))."'";
            }
            //only ads with posted zeitspanne
            if(Input::get('zeitspanne'))
            {
                $zstime = strtotime(stripslashes(Input::get('zeitspanne')));
                $searchWhereArr[] = "`created_tstamp` > '".$zstime."'";
            }
            //only ads with posted keyword
            if(isset($tag))
            {
                $searchInArr = array('bbkNr','barcode','title','author','otherPerson1','otherPerson2','publisher','annotation','releaseDate','ageRecommendation');
                $searchWhereArr[] =  "(CONCAT(".implode(',',$searchInArr).") LIKE '%%".strtolower($tag)."%%' OR `collation` LIKE '%%".$tag."%%'$tagsFound)";
            }

            $searchWhereArr[] = "`published` = 1";

            // Get the total number of items
            $intTotal = BbkModel::countBbkEntries($searchWhereArr);

            // Filter anwenden um die Gesamtanzahl zuermitteln
            if((int) $intTotal > 0)
            {
                //create table-head
                $headtd = '';
                $c = -1;
                foreach($this->headFields as $field)
                {
                    $c++;
                    $headtd .= '<th style="width:'.$this->tdWidthArr[$c].'; font-weight: bold;">'.$GLOBALS['TL_DCA'][$this->curr_table]['fields'][$field]['label'][0].'</th>';
                }

                if(strlen($headtd)>0)
                {
                    $headtd .= '<th style="width:'.$this->tdWidthArr[$c].'; font-weight: bold;">Schlagworte</th>';

                    $headtr = '<tr>'.$headtd.'</tr>';
                    reset($this->headFields);
                    $headtd = '';
                }

                // Get the total number of items
                $resultObj = BbkModel::findBbks(0,0,$searchWhereArr);

                //create-table-body
                $bodytr = '';
                $rc = 0;
                while($resultObj->next())
                {
                    $rowtd = '';
                    $c = -1;
                    foreach($this->headFields as $field)
                    {
                        $value = $this->formatValue($field,$resultObj->$field,true);
                        $cleanfield = $this->cleanTextForPdf($value);
                        $c++;
                        $rowtd .= '<td style="width:'.$this->tdWidthArr[$c].';"><span style="padding:10px 0px 10px 0;">'.$cleanfield.'</span></td>';

                    }
                    $rc++;

                    //tags zu diesem Eintrag holen
                    $tagsArr = array();

                    $tagsObj = $this->Database->prepare('SELECT * FROM `tl_tag` WHERE `from_table`=? AND `tid`=?')
                        ->execute('tl_bbk',$resultObj->id);

                    if($tagsObj->numRows)
                    {
                        while($tagsObj->next()) $tagsArr[] = $tagsObj->tag;
                        $rowtd .= '<td style="width: 90px;">'.implode(', ',$tagsArr).'</td>';
                    }
                    else
                    {
                        $rowtd .= '<td style="width: 90px;"> - </td>';
                    }

                    $bodytr .= '<tr style="background-color:'.(($rc%2 == 0)? "#EDEDED":"#ffffff").';">'.$rowtd.'</tr>';

                }
            }
            else
            {
                $showPdfLink = false;
            }

            $dlLinkUrl = $this->addToUrl('view=download&pdf=create');

            //if entries to big then HTML-output
            if($intTotal > $this->maxForPdf)
            {

                if(Input::get('view') == 'download')
                {
                    $strTable = '<html lang="de"><head><title>'.$this->bbk_pdf_title.'</title><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
					<link rel="stylesheet" href="'.$this->Environment->base.'tl_files/bz-niedersachsen/css/bzn_print.css" type="text/css" media="all"><style>td{ padding:10px;}</style></head><body>
					<table><thead style="padding-bottom:10mm;">'.$headtr.'</thead><tbody>'.$bodytr.'</tbody></table></body></html>';

                    header('Content-type: application/html');
                    header('Content-Disposition: attachment; filename="'.standardize(ampersand($this->bbk_pdf_title, false)).'.html"');
                }
                else
                {
                    $strTable = '<html lang="de"><head><title>'.$this->bbk_pdf_title.'</title><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
					<link rel="stylesheet" href="'.$this->Environment->base.'tl_files/bz-niedersachsen/css/bzn_print.css" type="text/css" media="all"><style>td{ padding:10px;}</style></head><body>
					<div class="print_actions"><a href="javascript:print();" class="print">Liste drucken</a>&nbsp;|&nbsp;<a href="'.$dlLinkUrl.'" class="save">Liste speichern</a></div>
					<table><thead style="padding-bottom:10mm;">'.$headtr.'</thead><tbody>'.$bodytr.'</tbody></table></body></html>';
                }

                print $strTable;
                exit();
            }
            else
            {
                $strTable = '<html lang="de"><head><title>'.$this->bbk_pdf_title.'</title><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			    <link rel="stylesheet" href="'.$this->Environment->base.'tl_files/bz-niedersachsen/css/bzn_print.css" type="text/css" media="all"><style>td{ padding:10px;}</style></head><body>
			    <table><thead style="padding-bottom:10mm;">'.$headtr.'</thead><tbody>'.$bodytr.'</tbody></table></body></html>';

                $this->createDOMPdf($strTable);
                exit();
            }
        }

        $this->Template->buttontext = 'BBK-Liste (PDF)';
        $this->Template->linkUrl = $this->addToUrl('pdf=create');
        $this->Template->showPdfLink = $showPdfLink;

    }
    /**
     * Format a value
     * @param string
     * @param mixed
     * @param boolean
     * @return mixed
     */
    protected function formatValue($k, $value, $blnListSingle=false)
    {
        $value = deserialize($value);
        global $objPage;

        // Return if empty
        if (empty($value))
        {
            return '';
        }

        $dcaField = $GLOBALS['TL_DCA'][$this->curr_table]['fields'][$k];

        // Array
        if (is_array($value))
        {
            $value = implode(', ', $value);
        }

        // Date
        elseif (isset($dcaField['eval']['rgxp']) && $dcaField['eval']['rgxp'] == 'date')
        {
            $value = $this->parseDate($objPage->dateFormat, $value);
        }

        // Time
        elseif (isset($dcaField['eval']['rgxp']) && $dcaField['eval']['rgxp'] == 'time')
        {
            $value = $this->parseDate($objPage->timeFormat, $value);
        }

        // Date and time
        elseif (isset($dcaField['eval']['rgxp']) && $dcaField['eval']['rgxp'] == 'datim')
        {
            $value = $this->parseDate($objPage->datimFormat, $value);
        }

        // URLs
        elseif (isset($dcaField['eval']['rgxp']) && $dcaField['eval']['rgxp'] == 'url' && preg_match('@^(https?://|ftp://)@i', $value))
        {
            global $objPage;
            $target = ($objPage->outputFormat == 'xhtml') ? 'onclick="return !window.open(this.href)"' : ' target="_blank"';

            $str = "<a href='%s' %s >%s</a>";
//			$value = '<a href="' . $value . '"' . (($objPage->outputFormat == 'xhtml') ? ' onclick="return !window.open(this.href)"' : ' target="_blank"') . '>' . $value . '</a>';

            $value = sprintf($str,$value,$target,$value);
        }

        // E-mail addresses
        elseif (isset($dcaField['eval']['rgxp']) && $dcaField['eval']['rgxp'] == 'email')
        {
            $value = StringUtil::encodeEmail($value);
            $value = '<a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;' . $value . '">' . $value . '</a>';
        }

        // Reference
        elseif (isset($dcaField['reference']) && is_array($dcaField['reference']))
        {
            $value = $dcaField['reference'][$value];
        }

        // Associative array
        elseif ((isset($dcaField['eval']['isAssociative']) && array_key_exists('options', $dcaField)) && ($dcaField['eval']['isAssociative'] || array_is_assoc($dcaField['options'])))
        {
            if ($blnListSingle)
            {
                $value = $dcaField['options'][$value];
            }
            else
            {
                $value = '<span class="value">[' . $value . ']</span> ' . $dcaField['options'][$value];
            }
        }

        return $value;
    }

    /**
     * genrate an article for PDF and stream it to the browser
     * @param Database_Result
     */
    protected function cleanTextForPdf($string)
    {
        $string = $this->replaceInsertTags($string);
        $string = html_entity_decode($string, ENT_QUOTES, $GLOBALS['TL_CONFIG']['characterSet']);
        $string = $this->convertRelativeUrls($string, '', true);

        // Remove form elements and JavaScript links
        $arrSearch = array
        (
            '@<form.*</form>@Us',
            '@<a [^>]*href="[^"]*javascript:[^>]+>.*</a>@Us',
        );

        $string = preg_replace($arrSearch, '', $string);
        $string = strip_tags($string);

        // Handle line breaks in preformatted text
        $string = preg_replace_callback('@(<pre.*</pre>)@Us', 'nl2br_callback', $string);

        // Default PDF export using TCPDF
        $arrSearch = array
        (
            '@<span style="text-decoration: ?underline;?">(.*)</span>@Us',
            '@(<img[^>]+>)@',
            '@(<div[^>]+block[^>]+>)@',
            '@[\n\r\t]+@',
            '@<br( /)?><div class="mod_article@',
            '@href="([^"]+)(pdf=[0-9]*(&|&amp;)?)([^"]*)"@'
        );

        $arrReplace = array
        (
            '<u>$1</u>',
            '$1',
            '$1',
            ' ',
            '<div class="mod_article',
            'href="$1$4"'
        );

        $string = preg_replace($arrSearch, $arrReplace, $string);

        return $string;
    }

    /**
     * Print an article as PDF and stream it to the browser
     * @param Database_Result
     */
    protected function createTCPdf($strContent = '')
    {

        // TCPDF configuration
        $l['a_meta_dir'] = 'ltr';
        $l['a_meta_charset'] = $GLOBALS['TL_CONFIG']['characterSet'];
        $l['a_meta_language'] = $GLOBALS['TL_LANGUAGE'];
        $l['w_page'] = 'page';

        // Include library
        require_once(TL_ROOT . '/system/config/tcpdf.php');
        require_once(TL_ROOT . '/plugins/tcpdf/tcpdf.php');

        // Create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true);

        // Set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor(PDF_AUTHOR);
        $pdf->SetTitle($this->bbk_pdf_title);
        $pdf->SetSubject($this->bbk_pdf_title);
        $pdf->SetKeywords($this->bbk_pdf_keywords);

        // Prevent font subsetting (huge speed improvement)
        $pdf->setFontSubsetting(false);

        // Remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // Set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // Set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // Set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // Set some language-dependent strings
        $pdf->setLanguageArray($l);

        // Initialize document and add a page
        $pdf->AddPage();

        // Set font
        $pdf->SetFont(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN);

        // Write the HTML content
        $pdf->writeHTML($strContent, true, 0, true, 0);

        // Close and output PDF document
        $pdf->lastPage();
        $pdf->Output(standardize(ampersand($this->bbk_pdf_title, false)) . '.pdf', 'D');

        // Stop script execution
        exit();
    }

    /**
     * Print an article as PDF and stream it to the browser
     * @param Database_Result
     */
    protected function createDOMPdf($strContent = '')
    {
        $dompdf = new Dompdf();
        $dompdf->load_html($strContent);
        $dompdf->set_paper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream(standardize(ampersand($this->bbk_pdf_title, false)) . '.pdf', array("Attachment" => false));

        // Stop script execution
        exit();
    }
}
