<?php

/**
 * PHP version 7.2
 * @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    bz-bbk-bundle (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

namespace Srhinow\BzBbkBundle\Modules;

use Contao\BackendTemplate;
use Srhinow\BzBbkBundle\BbkCal;
use Srhinow\BzBbkBundle\BbkReservations;

/**
 * Class ModuleBbkReservCalendar
 *
 * Front end module "bz-bbk-bundle"
 */
class ModuleBbkReservCalendar extends BbkReservations
{

	/**
	 * Current date object
	 * @var integer
	 */
	protected $Date;

	/**
	 * Redirect URL
	 * @var string
	 */
	protected $strLink;

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_reserv_calendar';

	/**
	 * current BBK
	 * @var string
	 */
	protected $currBBK;

	/**
	* show month calendars
	*  @var integer
	*/
	protected $mCount = 12;

	/**
	 * Do not show the module if no calendar has been selected
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### BBK RESERVATIONS CALENDAR ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}

		return parent::generate();
	}


	/**
	 * Generate the module
	 */
	protected function compile()
	{
	   $GLOBALS['TL_CSS'][] = $GLOBALS['BE_BBK']['PROPERTIES']['PUBLICSRC'].'/css/bbkcal.css';

	   $BbkCal = new BbkCal();
	   $BbkCal->setFirstMon(date('m.Y'));
	   $this->Template->reserv_calendar = $BbkCal->createCal();
	}



}
