<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Class Module PDF-Button
 *
 * Front end module "BBK"
 */
class ModuleImportTags extends Module
{

        /**
        * Table with story book Cinema entries
        * @var string
        */
        protected $bbk_table = 'tl_bbk';

        /**
        * Table with story book Cinema entries
        * @var string
        */
        protected $tag_table = 'tl_tag';


	/**
	 * Check whether the article is published
	 * @param boolean
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### TAGS aus CSV importieren ###';

			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'typolight/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}

		return parent::generate();
	}


	/**
	 * Generate the module
	 */
	protected function compile()
	{
		$csv = null;
		$this->import('Files');
		$this->import('Database');
		$this->import('String');

		$seperators = array('comma'=>',','semicolon'=>';','tabulator'=>'\t','linebreak'=>'\n');
		$locations = array('Aurich'=>'aurich','Lüneburg'=>'lueneburg','Hildesheim'=>'hildesheim');

		// Lock the tables
// 		$arrLocks = array($this->tag_table => 'WRITE');
// 		$this->Database->lockTables($arrLocks);

		//get DB-Fields as arrays
		$fields = $this->Database->listFields($this->bbk_table);

		/**
		*import BBK-Tags
		*/
		$CSVFile = '/tl_files/bz-niedersachsen/bbk_mit_tags.csv';
// 		print $CSVFile;
		$handle = $this->Files->fopen($CSVFile,'r');
		$counter = 0;
		$csvhead = array();
		$Set = '';
		$drop_first_row = 1;

		while (($data = fgetcsv ($handle, 1000, $seperators['comma'])) !== FALSE )
		{
		      $counter ++;
		      if($counter == 1 && $drop_first_row == 1)
		      {
			  $csvhead = $data;
			  continue;
		      }
		      foreach($csvhead AS $headk => $headv) $headfields[$headv]=$headk;

		      $lineA  = array();
		      foreach($fields as  $field)
		      {
			  //exclude index Fields
			  if($field['type']=='index') continue;
			  $actkey = $headfields[$field['name']];

			  $lineA[$field['name']] =  $data[$actkey];

		      }
		      $lineA['tags'] = $data[12];
		      $set = $lineA;

		    //Hole passenden BBK-Datensatz
		    $bbkItemObj = $this->Database->prepare('SELECT * FROM '.$this->bbk_table.' WHERE `bbkNr`=? AND `barcode`=? AND `location`=?')
						  ->limit(1)
						  ->execute($set['bbkNr'],$set['barcode'],$locations[$set['location']]);
		    if($bbkItemObj->numRows > 0)
		    {
			//alle bestehenden Tags dieses Datensatzes loeschen
			$this->Database->prepare('DELETE FROM `tl_tag` WHERE `id`=? AND `from_table`=?')->execute($bbkItemObj->id, $this->bbk_table);

			//tags vorbereiten
			$tags = explode(';',$set['tags']);

			//tags zum Datensatz speichern
			foreach($tags as $tag)
			{

			   $tagSet = array(
			      'id' => $bbkItemObj->id,
			      'tag' => trim($tag),
			      'from_table' => $this->bbk_table,
			   );
			   $this->Database->prepare("INSERT INTO `tl_tag` %s")->set($tagSet)->execute();
			}

		    }
		}
		print "fertig";

        }
}