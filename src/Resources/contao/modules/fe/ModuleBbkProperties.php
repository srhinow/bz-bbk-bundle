<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Class ModuleTeacherMember
 *
 * Front end module "BBK"
 */
class ModuleBBKProperties extends BackendModule
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_properties';


	/**
	 * Change the palette of the current table and switch to edit mode
	 */
	public function generate()
	{
	    return $this->objDc->edit($GLOBALS['BE_BBK']['PROPERTIES']['ID']);
	}

	/**
	 * Generate module
	 */
	protected function compile()
	{
		return '';
	}
}

?>