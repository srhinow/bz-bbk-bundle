<?php

/**
 * PHP version 7
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    bz-bbk-bundle (BilderBuchKino)
 * @license    commercial
 * @filesource
 */
namespace Srhinow\BzBbkBundle\Modules;

use Contao\BackendTemplate;
use Contao\Controller;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\Database;
use Contao\Email;
use Contao\FrontendUser;
use Contao\Input;
use Contao\Module;
use Contao\PageModel;
use Contao\Widget;
use Psr\Log\LogLevel;
use Srhinow\BzBbkBundle\BbkReservations;
use Srhinow\BzBbkBundle\Helper\BbkHelper;
use Srhinow\BzBbkBundle\Models\BbkLibrariesModel;

/**
 * Class ModuleBbkReservationForm
 *
 * Front end module "bz-bbk-bundle"
 */
class ModuleBbkReservationForm extends BbkReservations
{
	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'bbk_reservation_form';


	/**
	 * Target pages
	 * @var array
	 */
	protected $arrTargets = array();


	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### BBK Reservation Formular ###';

			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao?do=modules&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}

		// Fallback template
		if (strlen($this->bbk_template)) $this->strTemplate = $this->bbk_template;

		// Set the item from the auto_item parameter
		if ($GLOBALS['TL_CONFIG']['useAutoItem'] && isset($_GET['auto_item']))
		{
			Input::setGet('bbk', Input::get('auto_item'));
		}

		// Do not index or cache the page if no event has been specified
		if (!Input::get('bbk'))
		{
			global $objPage;
			$objPage->noSearch = 1;
			$objPage->cache = 0;
			return '';
		}
		return parent::generate();
	}


	/**
	 * Generate module
	 */
	protected function compile()
	{
		global $objPage;
	    // Get the front end user object
		$User = FrontendUser::getInstance();
		$bbkHelper = new BbkHelper();
		$userId = ($User->id) ? $User->id : '';
        $this->Template->showForm = true;
		// get library date
		if((int)$userId > 0)
		{
            $libObj = BbkLibrariesModel::findOneBy('member_id',$userId);
		}

		if(null === $libObj) {
            $this->Template->errorMsg = 'Dieser Login ist keiner BBK-Bibliothek zugeordnet. Bitte wenden sie sich an Ihre zuständige Büchereizentrale.';
            $this->Template->showForm = false;
		    return true;
        }

		// Form fields
		$arrFields = array
		(
			'firstname' => array
			(
				'name' => 'firstname',
				'label' => 'Vorname',
				'value' => trim($libObj->firstname),
				'inputType' => 'text',
				'eval' => array('mandatory'=>true, 'maxlength'=>64)
			),
			'lastname' => array
			(
				'name' => 'lastname',
				'label' => 'Nachname',
				'value' => trim($libObj->lastname),
				'inputType' => 'text',
				'eval' => array('mandatory'=>true, 'maxlength'=>64)
			),
			'library' => array
			(
				'name' => 'library',
				'label' => 'Bibliothek',
				'value' => $libObj->name,
				'inputType' => 'hidden',
				'eval' => array('mandatory'=>false, 'maxlength'=>155)
			),
            'library_id' => array
            (
                'name' => 'library_id',
                'label' => 'Bibliothek',
                'value' => $libObj->id,
                'inputType' => 'hidden',
                'eval' => array('mandatory'=>false, 'maxlength'=>10)
            ),
            'privaddress' => array
            (
                'name'                    => 'privaddress',
                'value'                   => $libObj->privaddress,
                'inputType'               => 'hidden',
                'eval'                    => array('readonly'=>true),
            ),
			'email' => array
			(
				'name' => 'email',
				'label' => 'E-Mail',
				'value' => trim($libObj->email),
				'inputType' => 'text',
				'eval' => array('rgxp'=>'email', 'mandatory'=>true, 'maxlength'=>128, 'decodeEntities'=>true)
			),
			// 'phone' => array
			// (
			// 	'name' => 'phone',
			// 	'label' => 'Rufnummer',
			// 	'value' => trim($User->phone),
			// 	'inputType' => 'text',
			// 	'eval' => array('rgxp'=>'phone', 'mandatory'=>true, 'maxlength'=>155)
			// ),
			'street' => array
			(
				'name' => 'street',
				'label' => 'Stra&szlig;e',
				'value' => trim($libObj->street),
				'inputType' => 'text',
				'eval' => array('mandatory'=>true, 'rgxp'=>'extnd', 'maxlength'=>128, 'decodeEntities'=>true)
			),
			'postal' => array
			(
				'name' => 'postal',
				'label' => 'PLZ',
				'value' => trim($libObj->postal),
				'inputType' => 'text',
				'eval' => array('mandatory'=>true, 'rgxp'=>'digit', 'maxlength'=>5, 'decodeEntities'=>true)
			),
			'city' => array
			(
				'name' => 'city',
				'label' => 'Stadt',
				'value' => trim($libObj->city),
				'inputType' => 'text',
				'eval' => array('mandatory'=>true, 'rgxp'=>'alpha', 'maxlength'=>128, 'decodeEntities'=>true)
			),
			'startDate' => array
			(
				'name' => 'startDate',
				'label' => 'Anfang der Reservierung',
				'inputType' => 'text',
//				'xdepend' => 'endDate',
				'eval' => array('mandatory'=>true, 'maxlength'=>10,'readonly'=>true)
			),
			'endDate' => array
			(
				'name' => 'endDate',
				'label' => 'Ende der Reservierung',
				'inputType' => 'text',
				'eval' => array('mandatory'=>true, 'maxlength'=>10,'readonly'=>true)
			),
		);

		if(!FE_USER_LOGGED_IN)
		{
		    $arrFields['captcha'] = array
		    (
			'name' => 'captcha',
			'inputType' => 'captcha',
			'eval' => array('mandatory'=>true)
		    );
		}

		$doNotSubmit = false;
		$arrWidgets = array();
		$strFormId = 'bbk_reservation_form_'. $objPage->id;

		// Initialize widgets
		foreach ($arrFields as $arrField)
		{
			$strClass = $GLOBALS['TL_FFL'][$arrField['inputType']];

			// Continue if the class is not defined
			if (!class_exists($strClass))
			{
				continue;
			}

            if(isset($arrField['eval']['mandatory'])) {
                $arrField['eval']['required'] = $arrField['eval']['mandatory'];
            }

            $varValue = isset($arrField['value']) ? $arrField['value'] : null;
			$objWidget = new $strClass(Widget::getAttributesFromDca($arrField, $arrField['name'], $varValue));

			if($arrField['inputType']=='xdependentcalendarfields')
			{
			    $objWidget->xdepend = $arrField['xdepend'];
			}

			// Validate the widget
			if (Input::post('FORM_SUBMIT') == $strFormId)
			{
				$objWidget->validate();

				if ($objWidget->hasErrors())
				{
					$doNotSubmit = true;
				}
			}

			$arrWidgets[$arrField['name']] = $objWidget;
		}

		// Get the current bbk
		$objBBK = $this->Database->prepare("SELECT `tl_bbk`.*, `tl_bbk_locations`.`booking_email` FROM `tl_bbk` LEFT JOIN `tl_bbk_locations` ON `tl_bbk`.`pid` = `tl_bbk_locations`.`id`  WHERE `tl_bbk`.`id`=?  AND published=1")
								->limit(1)
								->execute((is_numeric(Input::get('bbk')) ? Input::get('bbk') : 0));

		$this->Template->headline = $this->headline;
		$this->Template->fields = $arrWidgets;
		$this->Template->submit = 'Reservierungsanfrage senden';
		$this->Template->action = ampersand($this->Environment->request);
		$this->Template->messages = ''; // Backwards compatibility
		$this->Template->formId = $strFormId;
		$this->Template->hasError = $doNotSubmit;

		//wenn Formular abgesendet wurde
		if($objBBK->numRows > 0 && Input::post('FORM_SUBMIT') == $strFormId && !$doNotSubmit)
		{
			$time = time();
            $startDate = strtotime($arrWidgets['startDate']->value);
            $endDate = strtotime($arrWidgets['endDate']->value);
            $endShippingDate = $this->fillShippingEnd();

			// Prepare the record
			$arrSet = array
			(
				'pid' => $objBBK->pid,
				'bbk' => $objBBK->id,
				'member_id' => $userId,
				'tstamp' => $time,
				'firstname' => $arrWidgets['firstname']->value,
				'lastname' => $arrWidgets['lastname']->value,
				'library' => $arrWidgets['library']->value,
				'library_id' => $arrWidgets['library_id']->value,
				'privaddress' => $arrWidgets['privaddress']->value,
				'email' => $arrWidgets['email']->value,
				// 'phone' => $arrWidgets['phone']->value,
				'street' => $arrWidgets['street']->value,
				'postal' => $arrWidgets['postal']->value,
				'city' => $arrWidgets['city']->value,
				'startDate' => $startDate,
				'endDate' => strtotime($arrWidgets['endDate']->value),
				'startShippingDate' => $startDate,
				'endShippingDate' => $endShippingDate,
				'loan' => $this->fillLoan($startDate,$endDate),
				'addedOn' => $time
			);

			$insertId = Database::getInstance()->prepare("INSERT INTO `tl_bbk_booking` %s")->set($arrSet)->execute()->insertId;

            try {
			$objEmail = new Email();

			$objEmail->from = $arrWidgets['email']->value;
			$objEmail->fromName = $arrWidgets['firstname']->value.' '.$arrWidgets['lastname']->value;
			$objEmail->subject = $this->ereserv_subject;
			$objEmail->text .= "\n----------------------------------\n";
			$objEmail->text = 'Datensatz-ID: '.$insertId."\n";
			$objEmail->text .= 'Bilderbuchkino: '.$objBBK->title.' (BBK-Nr: '.$objBBK->bbkNr.")\n";
			$objEmail->text .= 'Buchungszeitraum: '.$arrWidgets['startDate']->value.' - '.$arrWidgets['endDate']->value."\n";
			$objEmail->text .= 'Host: '.$this->Environment->host."\n";
			$objEmail->text .= 'Backend-Link: '.$this->Environment->url.'/contao?do=bbk_locations&table=tl_bbk_booking&act=edit&id='.$insertId."\n";
			$objEmail->text .= "\n----------------------------------\n";

			$empfangEmail = ($objBBK->booking_email) ? $objBBK->booking_email : $this->ereserv_email;
			$objEmail->sendTo($empfangEmail);

            }catch(\Exception $e) {
                $logger = static::getContainer()->get('monolog.logger.contao');
                $logger->log(LogLevel::ERROR, $e->getMessage(), array('contao' => new ContaoContext(__METHOD__, TL_ERROR)));
            }

			if($this->jumpTo)
			{
                $objReservationPage = PageModel::findByPk($this->jumpTo);
                $ReservationUrl = ampersand( $this->generateFrontendUrl($objReservationPage->row()) );
			    $this->redirect($ReservationUrl);
			}
			else $this->reload();
		}

	}


	/**
	 * get ShippngEnd value
	 * @return time-string
	 */
	public function fillShippingEnd()
	{
	    if(Input::post('endDate'))
	    {
			$endShippingDate = strtotime('+7 days', strtotime(Input::post('endDate')) );

			switch(date('N',$endShippingDate)) // Wochenend-Tag überspringen
			{
				case 6: // Sonnabend +2 = Montag
					 $endShippingDate = strtotime('+2 days', $endShippingDate);
				break;
				case 7: // Sonntag +1 = Montag
				     $endShippingDate = strtotime('+1 days', $endShippingDate);
				break;
			}

			return $endShippingDate;
	    }
	}

	/**
	 * get Loan value
	 * @return int
	 */
	public function fillLoan($start = 0, $end = 0)
	{
	    $loan = 0;

	    if((int)$start > 0 && (int)$end > 0)
	    {
				$loan = 0;

				if($end == $start)  // wenn selber Tag
				{
				   $loan = 1;
				}
				elseif($end > $start) // wenn über mehrere Tage
				{
					$diff = ($end - $start) / 86400;// Differenz in Tagen
					$loan = ceil($diff /14);
				}
	    }

	    return $loan;
	}

}
