<?php

/**
 * PHP version 7.2
 * @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    bz-bbk-bundle (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

namespace Srhinow\BzBbkBundle\Modules;

use Contao\BackendTemplate;
use Contao\Database;
use Contao\FilesModel;
use Contao\Input;
use Contao\Module;
use Contao\PageModel;
use Srhinow\BzBbkBundle\Models\BbkLocationsModel;
use Srhinow\BzBbkBundle\Models\BbkModel;

/**
 * Class ModuleBbkDetails
 *
 * Front end module "bz-bbk-bundle"
 */
class ModuleBbkDetails extends Module
{
	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'bbk_details';


	/**
	 * Target pages
	 * @var array
	 */
	protected $arrTargets = array();

        /**
        * Table with BilderBuchKino entries
        * @var string
        */
        protected $curr_table = 'tl_bbk';

	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### BBK-Detailansicht ###';

			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'typolight/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}

        // Fallback template
		if (strlen($this->mcsp_template)) $this->strTemplate = $this->mcsp_template;

		return parent::generate();
	}


	/**
	 * Generate module
	 */
	protected function compile()
	{

		$this->loadLanguageFile($this->curr_table);
		$this->loadDataContainer($this->curr_table);

        $searchWhereStr  = '';
		$searchWhereArr = array();
// 		$showFields = array('bbkNr','title','mediatyp','location','ageRecommendation');

		//Item per Get
		if(!Input::get('bbk'))
		{
			$this->Template->items = array();
            return;
		}

        //Daten holen
		$resultObj = BbkModel::findByIdOrAlias(Input::get('bbk'));

		if (null === $resultObj)
		{
			return;
		}

		$arrFields = array();
		$arrRow = $resultObj->row();
		$limit = count($arrRow);
		$count = -1;

		foreach ($arrRow as $k => $v)
		{
// 			if(!in_array($k,$showFields)) continue;

			// Never show passwords
            $fieldDca = $GLOBALS['TL_DCA'][$this->curr_table]['fields'][$k];

			if (isset($fieldDca['inputType']) && $fieldDca['inputType'] == 'password')
			{
				--$limit;
				continue;
			}

            if($k == 'imageUrl')
            {
                $objFile = FilesModel::findByUuid($v);

                if ($objFile !== null)
                {
                    $v = $objFile->path;
                } else {
                    $v = $GLOBALS['BE_BBK']['PROPERTIES']['PUBLICSRC'] .'/icons/'. (($resultObj->mediatyp == 'cdrom') ? 'cdrom.png': 'dia.png');
                }
            }

            if ($k == 'location'){
                $objLocation = BbkLocationsModel::findByIdOrAlias($v);
                if (null !== $objLocation) {
                    $v = $objLocation->name;
                }
            }

            $class = 'row_' . ++$count . (($count == 0) ? ' row_first' : '') . (($count >= ($limit - 1)) ? ' row_last' : '') . ((($count % 2) == 0) ? ' even' : ' odd');


			$arrFields[$k] = array
			(
				'raw' => $v,
				'label' => (isset($fieldDca['label'][0])?$fieldDca['label'][0]:$k),
				'content' => $this->formatValue($k, $v, true),
				'class' => $class
			);
		}

		$this->Template->record = $arrFields;

		$tags = array();
		$tagObj = $this->Database->prepare('SELECT `tag` FROM `tl_tag` WHERE `from_table`=? AND `tid`=?')
					 ->execute($this->curr_table,$resultObj->id);
		if($tagObj->numRows > 0)
		{
		    while($tagObj->next())
		    {
			$tags[] = '<a href="bilderbuchkino-katalog.html?FORM_SUBMIT=BBK_SEARCH_FORM&amp;tag='.$tagObj->tag.'" title="weitere BBKs mit dem Begriff '.$tagObj->tag.' finden">'.$tagObj->tag.'</a>';
		    }
		}

		$objPage = PageModel::findById($this->jumpTo);
//		var_dump($objPage->getFrontendUrl()); die();
		$this->Template->backlink = is_object($objPage)?$objPage->getFrontendUrl():'{{env::referer}}';
		$this->Template->tags = implode(', ',$tags);
		$this->Template->isBE = (BE_USER_LOGGED_IN) ? true : false ;

      }



	/**
	 * Format a value
	 * @param string
	 * @param mixed
	 * @param boolean
	 * @return mixed
	 */
	protected function formatValue($k, $value, $blnListSingle=false)
	{
		$value = deserialize($value);
        $fieldDca = $GLOBALS['TL_DCA'][$this->curr_table]['fields'][$k];

		// Return if empty
		if (empty($value))
		{
			return '';
		}

		global $objPage;

		// Array
		if (is_array($value))
		{
			$value = implode(', ', $value);
		}

		// Date
		elseif (isset($fieldDca['eval']['rgxp']) && $fieldDca['eval']['rgxp'] == 'date')
		{
			$value = $this->parseDate($objPage->dateFormat, $value);
		}

		// Time
		elseif (isset($fieldDca['eval']['rgxp']) && $fieldDca['eval']['rgxp'] == 'time')
		{
			$value = $this->parseDate($objPage->timeFormat, $value);
		}

		// Date and time
		elseif (isset($fieldDca['eval']['rgxp']) && $fieldDca['eval']['rgxp'] == 'datim')
		{
			$value = $this->parseDate($objPage->datimFormat, $value);
		}

		// URLs
		elseif (isset($fieldDca['eval']['rgxp']) && $fieldDca['eval']['rgxp'] == 'url' && preg_match('@^(https?://|ftp://)@i', $value))
		{
			global $objPage;
			$value = '<a href="' . $value . '"' . (($objPage->outputFormat == 'xhtml') ? ' onclick="return !window.open(this.href)"' : ' target="_blank"') . '>' . $value . '</a>';
		}

		// E-mail addresses
		elseif (isset($fieldDca['eval']['rgxp']) && $fieldDca['eval']['rgxp'] == 'email')
		{
			$value = \StringUtil::encodeEmail($value);
			$value = '<a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;' . $value . '">' . $value . '</a>';
		}

		// Reference
		elseif (isset($fieldDca['reference']) && is_array($fieldDca['reference']))
		{
			$value = $fieldDca['reference'][$value];
		}

		// Associative array
        elseif (isset($fieldDca['eval']['isAssociative']) || (isset($fieldDca['options']) && array_is_assoc($fieldDca['options'])))
        {
			if ($blnListSingle)
			{
				$value = $fieldDca['options'][$value];
			}
			else
			{
				$value = '<span class="value">[' . $value . ']</span> ' . $fieldDca['options'][$value];
			}
		}

		return $value;
	}
}
