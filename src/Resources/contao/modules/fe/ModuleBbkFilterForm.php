<?php

/**
 * PHP version 7.2
 * @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

namespace Srhinow\BzBbkBundle\Modules;

use Contao\BackendTemplate;
use Contao\Input;
use Contao\Module;
use Srhinow\BzBbkBundle\Models\BbkLocationsModel;

/**
 * Class ModuleBbkFilterForm
 *
 * Front end module "bz-bbk-bundle"
 */
class ModuleBbkFilterForm extends Module
{
	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'bbk_list';

	/**
	 * Template
	 * @var string
	 */
	protected $strFormId = 'BBK_SEARCH_FORM';

	/**
	 * Target pages
	 * @var array
	 */
	protected $arrTargets = array();

        /**
        * Table with BilderBuchKino entries
        * @var string
        */
        protected $curr_table = 'tl_bbk';

	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### BBK-FILTER-FORM ###';

			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'typolight/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}
                // Fallback template
		if (strlen($this->bbk_template)) $this->strTemplate = $this->bbk_template;

		return parent::generate();
	}


	/**
	 * Generate module
	 */
	protected function compile()
	{
		$this->loadLanguageFile($this->curr_table);
		$this->loadDataContainer($this->curr_table);

        $this->Template->record = array();
        $this->Template->headFields = array();

		$tag = (Input::get('tag')) ? Input::get('tag') : Input::get('keyword');
		$this->Template->keyword = $tag;

		// MediaTypes for select-Field
		$mediatyp_options = array();
		foreach($GLOBALS['TL_LANG'][$this->curr_table]['mediatyp_options'] as $mtyp => $value)
		{
		    $mediatyp_options[$mtyp] = array(
		    'label' => $value,
		    'value' => $mtyp,
		    'active' => (Input::get('mediatyp') == $mtyp) ? 'selected="selected"' : ''
		    );
		}
        $this->Template->mediatyp_options = $mediatyp_options;

        // locations for select-Field
		$location_options = array();
		$objLocation = BbkLocationsModel::findAll(['order'=>'name']);
		if(null === $objLocation) return $objLocation;

		while($objLocation->next())
		{
		    $location_options[$objLocation->alias] = array(
		    'label' => $objLocation->name,
		    'value' => $objLocation->alias,
		    'active' => (
		        Input::get('location') == $objLocation->alias
                || Input::get('location') == $objLocation->alias
            ) ? 'selected="selected"' : ''
		    );
		}
        $this->Template->location_options = $location_options;

		// ageRecommendations for select-Field
		$ageRecos = array();
		$agerecoObj = $this->Database->execute('SELECT `ageRecommendation` FROM `'.$this->curr_table.'` WHERE `published`=1  AND `ageRecommendation`!=0 GROUP BY `ageRecommendation` ORDER BY `ageRecommendation` DESC');

		if($agerecoObj->numRows > 0)
		{
		    while($agerecoObj->next())
		    {
                $ageRecos[$agerecoObj->ageRecommendation] = array(
                    'value' => $agerecoObj->ageRecommendation,
                    'active'=> (Input::get('agereco') == $agerecoObj->ageRecommendation) ? 'selected="selected"' : '',
                    'label' => $GLOBALS['TL_LANG'][$this->curr_table][$agerecoObj->ageRecommendation],
                );
		    }
		    ksort($ageRecos);
		    $this->Template->ageRecos = $ageRecos;
                }

                //neueste BBKs
                $zeitspannen = array(
                    'Neue Medien des letzten Monats'=>'-1 months',
                    'Neue Medien der letzten 3 Monate'=>'-3 months',
                    'Neue Medien der letzten 6 Monate'=>'-6 months',
                    'Neue Medien des letzten Jahres'=>'-1 year'
                );
		$zeitspannen_options = array();
		foreach($zeitspannen as $zsk => $zsv)
		{
		    $zeitspannen_options[$zsv] = array(
		    'label' => $zsk,
		    'value' => $zsv,
		    'active' => (Input::get('zeitspanne') == $zsv || Input::get('zeitspanne') == $zsv) ? 'selected="selected"' : ''
		    );
		}

        $this->Template->zeitspannen_options = $zeitspannen_options;
		$this->Template->formId = $this->strFormId;
		$this->Template->formSend = (Input::get('FORM_SUBMIT') || Input::get('tag')) ? true : false;
	}

}
