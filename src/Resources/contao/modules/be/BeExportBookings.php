<?php

/**
 * PHP version 7
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    bz-bbk-bundle (BilderBuchKino)
 * @license    commercial
 * @filesource
 */
namespace Srhinow\BzBbkBundle\Modules;

use Contao\Backend;
use Contao\File;
use Contao\System;

/**
 * Class beExportBookings
 */
class beExportBookings extends Backend
{
	/**
	 * Export bookings for statistics
	 */
	public function csvExportBookings()
	{

		if ($this->Input->post('FORM_SUBMIT') == 'export_bookings')
		{
		    $this->import('Database');

                    $pid =  $this->Input->post('pid');

                    //Check the reference-id
		    if($pid < 1)
		    {
                $_SESSION['TL_ERROR'][] = 'keine ID als Daten-Referenz angegeben';
                $this->reload();
		    }

		    //set handle from file
		    $seperators = array('comma'=>',','semicolon'=>';','tabulator'=>"\t",'linebreak'=>"\n");
		    $fieldnames = array('title'=>'BBK-Titel','bbkNr'=>'BBK-Nummer','mediatyp'=>'BBK-Typ','standort'=>'Standort','startDate'=>'geliehen von','endDate'=>'geliehen bis','loan'=>'Laien','created'=>'im Sortiment');

		    $eventObj = $this->Database->prepare('SELECT `tl_bbk`.*,
		    `tl_bbk_locations`.`name` as `standort`,
		    `tl_bbk_booking`.`startDate`,
		    `tl_bbk_booking`.`endDate`,
		    `tl_bbk_booking`.`loan`,
		    FROM_UNIXTIME(`tl_bbk`.`created_tstamp`,"%Y") as `created`
		    FROM `tl_bbk_booking`
		    LEFT JOIN `tl_bbk` ON `tl_bbk_booking`.`bbk` = `tl_bbk`.`id`
		    LEFT JOIN `tl_bbk_locations` ON `tl_bbk_booking`.`pid` = `tl_bbk_locations`.`id`
		    WHERE `tl_bbk_locations`.`id` = ? AND `accepted` = ? AND (FROM_UNIXTIME(`startDate`,"%Y") = '.$this->Input->post('exportYear').' || FROM_UNIXTIME(`endDate`,"%Y") = '.$this->Input->post('exportYear').')
		    ORDER BY `startDate` ASC')
				     ->execute($pid, 1);

		    System::loadLanguageFile('tl_bbk');

            //Check the reference-id
		    if($eventObj->numRows < 1)
		    {
                $_SESSION['TL_ERROR'][] = 'keine Daten zum exportieren vorhanden';
                $this->reload();
		    }
		    $arrExport = $eventObj->fetchAllAssoc();

		    // start output
		    $exportFile =  'export_'.$eventObj->alias.'_' . date("Ymd-Hi");
		    $output = '';
		    $output = '"' . implode('"'.$seperators[$this->Input->post('separator')].'"', array_values($fieldnames)).'"' . "\n";

		    foreach ($arrExport as $export)
		    {
                $row = array(
                   'title' => $export['title'],
                   'bbkNr' => $export['bbkNr'],
                   'mediatyp' => $GLOBALS['TL_LANG']['tl_bbk']['mediatyp_options'][$export['mediatyp']],
                   'standort' => $export['standort'],
                   'startDate' => date($GLOBALS['TL_CONFIG']['dateFormat'],$export['startDate']),
                   'endDate' => date($GLOBALS['TL_CONFIG']['dateFormat'],$export['endDate']),
                   'loan' => $export['loan'],
                   'created' => $export['created']
                );

			    $output .= '"' . implode('"'.$seperators[$this->Input->post('separator')].'"', str_replace("\"", "\"\"", $row)).'"' . "\n";
		    }

		    ob_end_clean();
		    header('Content-Type: application/csv');
		    header('Content-Transfer-Encoding: binary');
		    header('Content-Disposition: attachment; filename="' . $exportFile .'.csv"');
		    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		    header('Pragma: public');
		    header('Expires: 0');
		    echo $output;
		    exit();
		}

    		// Return the form
		return '<div class="tl_content">
                    <div id="tl_buttons">
                        <a href="'.ampersand(str_replace('&key=csvExportBookings', '', $this->Environment->request)).'" class="header_back" title="'.specialchars($GLOBALS['TL_LANG']['MSC']['backBT']).'" accesskey="b">'.$GLOBALS['TL_LANG']['MSC']['backBT'].'</a>
                    </div>
                    
                    <form action="'.ampersand($this->Environment->request, true).'" id="tl_bbk_csvexport" class="tl_form" method="post">
                    <div class="tl_formbody_edit">
                        <input type="hidden" name="FORM_SUBMIT" value="export_bookings" />
                        <input type="hidden" name="REQUEST_TOKEN" value="'.REQUEST_TOKEN.'" />
                        <input type="hidden" name="pid" value="'.$this->Input->get('id').'" />
                        <fieldset class="tl_box">
                            <div class="w50 widget">
                            <h3><label for="ctrl_bbk">'.$GLOBALS['TL_LANG']['tl_bbk_booking']['exportYear'][0].'</label></h3>
                            <select name="exportYear" id="exportYear" class="tl_select" onfocus="Backend.getScrollOffset();">
                            <option value="'.date('Y',strtotime('+2 year')).'">'.date('Y',strtotime('+2 year')).'</option>
                            <option value="'.date('Y',strtotime('+1 year')).'">'.date('Y',strtotime('+1 year')).'</option>
                            <option value="'.date('Y').'">'.date('Y').'</option>
                            <option value="'.date('Y',strtotime('-1 year')).'">'.date('Y',strtotime('-1 year')).'</option>
                            <option value="'.date('Y',strtotime('-2 years')).'">'.date('Y',strtotime('-2 years')).'</option>
                            </select>'.(($GLOBALS['TL_LANG']['MSC']['separator'][1] != '') ? '<p class="tl_help tl_tip">'.$GLOBALS['TL_LANG']['tl_bbk_booking']['exportYear'][1].'</p>' : '').'
                            </div>
                            <div class="w50 widget">
                            <h3><label for="ctrl_bbk">CSV-Trenner</label></h3>
                            <select name="separator" id="separator" class="tl_select" onfocus="Backend.getScrollOffset();">
                            <option value="semicolon">'.$GLOBALS['TL_LANG']['MSC']['semicolon'].' (;)</option>
                            <option value="comma">'.$GLOBALS['TL_LANG']['MSC']['comma'].' (,)</option>
                            <option value="tabulator">'.$GLOBALS['TL_LANG']['MSC']['tabulator'].'</option>
                            <option value="linebreak">'.$GLOBALS['TL_LANG']['MSC']['linebreak'].'</option>
                            </select>'.(($GLOBALS['TL_LANG']['MSC']['separator'][1] != '') ? '<p class="tl_help tl_tip">'.$GLOBALS['TL_LANG']['MSC']['separator'][1].'</p>' : '').'
                            </div>
                        </fieldset>
                    </div>
        
                    <div class="tl_formbody_submit">    
                        <div class="tl_submit_container">
                          <input type="submit" name="save" id="save" class="tl_submit" accesskey="s" value="'.specialchars($GLOBALS['TL_LANG']['tl_bbk_booking']['exportCSV'][0]).'" />
                        </div>    
                    </div>
                </form>
		    </div>';
	}

	/**
	 * generate runlist
	 */
	public function runList()
	{
		if ($this->Input->post('FORM_SUBMIT') == 'generate_runlist')
		{
		    $this->import('Database');
            $pid =  $this->Input->post('pid');

                    //Check the reference-id
		    if($pid < 1)
		    {
			$_SESSION['TL_ERROR'][] = 'keine ID als Daten-Referenz angegeben';
			$this->reload();
		    }

		    $resultObj = $this->Database->prepare('SELECT `tl_bbk`.*,
		    `tl_bbk_locations`.`name` as `standort`,
		    `tl_bbk_booking`.`startDate`,
		    `tl_bbk_booking`.`endDate`,
		    `tl_bbk_booking`.`firstname`,
		    `tl_bbk_booking`.`lastname`,
		    `tl_bbk_booking`.`library`,
		    `tl_bbk_booking`.`street`,
		    `tl_bbk_booking`.`postal`,
		    `tl_bbk_booking`.`city`,
		    `tl_bbk_booking`.`phone`,
		    `tl_bbk_booking`.`email`,
		    `tl_bbk_booking`.`memo`
		    FROM `tl_bbk_booking`
		    LEFT JOIN `tl_bbk` ON `tl_bbk_booking`.`bbk` = `tl_bbk`.`id`
		    LEFT JOIN `tl_bbk_locations` ON `tl_bbk_booking`.`pid` = `tl_bbk_locations`.`id`
		    WHERE `tl_bbk_locations`.`id` = ?
		    AND (`startDate` >= '.strtotime($this->Input->post('beginnDate')).' && `startDate` <= '.strtotime($this->Input->post('endDate')).')
		    AND `accepted` != 2
		    ORDER BY `startDate` ASC')
				     ->execute($pid);

            //Check the reference-id
		    if($resultObj->numRows < 1)
		    {
                $_SESSION['TL_ERROR'][] = 'keine Daten zum exportieren vorhanden';
                $this->reload();
		    }

		    $arrExport = $resultObj->fetchAllAssoc();
    	    $fieldnames = array('bookingtime'=>'Zeitraum','memo'=>'Memo','bbk'=>'BBK','adresse'=>'Adresse');

		    // start output
		    $exportFile =  'export_'.$resultObj->alias.'_' . date("Ymd-Hi");
		    $output = $head = '';
		    foreach(array_values($fieldnames) as $field) $head .= '<th>'.$field.'</th>';
		    $output = '<thead><tr>' . $head . "</tr></thead>";

		    foreach ($arrExport as $export)
		    {
                $row = '';
                System::loadLanguageFile('tl_bbk');
                $mediatyp = $GLOBALS['TL_LANG']['tl_bbk']['mediatyp_options'][$export['mediatyp']];
                $rowArr = array(
                   'bookingtime' =>  'Beginn:'.date($GLOBALS['TL_CONFIG']['dateFormat'],$export['startDate']).'<br>Ende: '.date($GLOBALS['TL_CONFIG']['dateFormat'],$export['endDate']),
                   'memo' => $export['memo'],
                   'bbk' => '<b>BBK-Nummer:</b> '.$export['bbkNr'].'<br><b>BBK-Titel:</b> '.$export['title'].'<br><b>BBK-Typ:</b> '.$mediatyp.'<br>',
                   'adresse' => '<b>Adresse:</b> '.$export['library'].'<br>'.$export['firstname'].' '.$export['lastname'].'<br>'.$export['street'].'<br>'.$export['postal'].' '.$export['city']
                                .'<br><b>Kontakt:</b> <br>Telefon:'.$export['phone'].'<br>Email: '.$export['email'].'<br>'
                );

                foreach($rowArr as $k => $cell) $row .= '<td>'.$cell.'</td>';
			    $output .= '<tr>'.$row.'</tr>';
		    }
		    $output = '<table>'.$output.'</table>';

  		    $pdfFile = new File('files/bzn-c3/layout/css/pdf.css');
		    $cssText = $pdfFile->getContent();
		    $cssText = '<style>'.$cssText.'</style>';

		    ob_end_clean();
		    $this->createPdf($cssText.$output);


		}

		$beginnDate = ($this->Input->post('beginnDate')) ? $this->Input->post('beginnDate') : date($GLOBALS['TL_CONFIG']['dateFormat'],strtotime('+14 days'));
    	$endDate = ($this->Input->post('endDate')) ? $this->Input->post('endDate') :date($GLOBALS['TL_CONFIG']['dateFormat'],strtotime('+21 days'));
    		// Return the form
		return '<div class="tl_content">
		    <div id="tl_buttons">
		    <a href="'.ampersand(str_replace('&key=runList', '', $this->Environment->request)).'" class="header_back" title="'.specialchars($GLOBALS['TL_LANG']['MSC']['backBT']).'" accesskey="b">'.$GLOBALS['TL_LANG']['MSC']['backBT'].'</a>
		    </div>
		    
		    <form action="'.ampersand($this->Environment->request, true).'" id="tl_bbk_csvexport" class="tl_form" method="post">
		    <div class="tl_formbody_edit">
                <input type="hidden" name="FORM_SUBMIT" value="generate_runlist" />
                <input type="hidden" name="REQUEST_TOKEN" value="'.REQUEST_TOKEN.'" />
                <input type="hidden" name="pid" value="'.$this->Input->get('id').'" />
                <fieldset class="tl_box">
                    <div class="w50 widget">
                        <h3><label for="ctrl_bbk">'.$GLOBALS['TL_LANG']['tl_bbk_booking']['runList_beginnDate'][0].'</label></h3>
                        <input type="text" name="beginnDate" id="ctrl_beginnDate" class="tl_text" value='.$beginnDate.'>
                    '.(($GLOBALS['TL_LANG']['MSC']['separator'][1] != '') ? '<p class="tl_help tl_tip">'.$GLOBALS['TL_LANG']['tl_bbk_booking']['runList_beginnDate'][1].'</p>' : '').'
                    </div>
                    <div class="w50 widget">
                        <h3><label for="ctrl_endDate">'.$GLOBALS['TL_LANG']['tl_bbk_booking']['runList_endDate'][0].'</label></h3>
                        <input type="text" name="endDate" id="ctrl_endDate" class="tl_text" value='.$endDate.'>
                    '.(($GLOBALS['TL_LANG']['MSC']['separator'][1] != '') ? '<p class="tl_help tl_tip">'.$GLOBALS['TL_LANG']['tl_bbk_booking']['runList_endDate'][1].'</p>' : '').'
                    </div>
                </fieldset>
		    </div>

		    <div class="tl_formbody_submit">
                <div class="tl_submit_container">
                  <input type="submit" name="save" id="save" class="tl_submit" accesskey="s" value="'.specialchars($GLOBALS['TL_LANG']['tl_bbk_booking']['runlist_generate_button'][0]).'" />
                </div>
		    </div>
		    </form>
		    </div>';
	}

	/**
	 * Print an article as PDF and stream it to the browser
	 * @param Database_Result
	 */
	protected function createPdf($strContent = '')
        {
	    // TCPDF configuration
	    $l['a_meta_dir'] = 'ltr';
	    $l['a_meta_charset'] = $GLOBALS['TL_CONFIG']['characterSet'];
	    $l['a_meta_language'] = $GLOBALS['TL_LANGUAGE'];
	    $l['w_page'] = 'page';

		// Include library
		require_once(TL_ROOT . '/system/config/tcpdf.php');
		
	    // Create new PDF document
	    $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true);

	    $title = date('Ymdhis').'-runlist';

	    // Set document information
	    $pdf->SetCreator(PDF_CREATOR);
	    $pdf->SetAuthor(PDF_AUTHOR);
	    $pdf->SetTitle($title);
	    $pdf->SetSubject($title);
	    $pdf->SetKeywords($title);

	    // Prevent font subsetting (huge speed improvement)
	    $pdf->setFontSubsetting(false);

	    // Remove default header/footer
	    $pdf->setPrintHeader(false);
	    $pdf->setPrintFooter(false);

	    // Set margins
	    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

	    // Set auto page breaks
	    $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

	    // Set image scale factor
	    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	    // Set some language-dependent strings
	    $pdf->setLanguageArray($l);

	    // Initialize document and add a page
	    $pdf->AddPage();

	    // Set font
	    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN);

	    // Write the HTML content
	    $pdf->writeHTML($strContent, true, 0, true, 0);

	    // Close and output PDF document
	    $pdf->lastPage();
	    $pdf->Output(standardize(ampersand($title, false)) . '.pdf', 'D');

	    // Stop script execution
	    exit;
        }
}
