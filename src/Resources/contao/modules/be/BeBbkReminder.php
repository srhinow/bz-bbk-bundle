<?php

/**
 * PHP version 7
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    bz-bbk-bundle (BilderBuchKino)
 * @license    commercial
 * @filesource
 */
namespace Srhinow\BzBbkBundle\Modules;


use Contao\Backend;
use Contao\Database;
use Contao\Input;
use Contao\Message;
use Contao\System;
use Psr\Log\LogLevel;

/**
 * Class BeBbkReminder
 */
class BeBbkReminder extends Backend
{
	/**
	 * check all Invoices of reminder
	 */
	public function checkReminder()
	{
		// test location-ID
		if(!Input::get('id'))
		{
            Message::addError($GLOBALS['TL_LANG']['tl_bbk_reminder']['MESSAGE']['no_id']);

            $logger = System::getContainer()->get('monolog.logger.contao');
            $logger->log(LogLevel::ERROR, $GLOBALS['TL_LANG']['tl_bbk_reminder']['MESSAGE']['no_id']);

			$this->redirect(str_replace('&key=checkReminder', '', $this->Environment->request));
		}

		//get all bookings where is accepted, not in house 
		$bookingObj = Database::getInstance()
            ->prepare('SELECT * FROM `tl_bbk_booking` WHERE `accepted`=? AND `inHouse`!=? AND `endShippingDate`<? AND `pid`=?')
            ->execute(1, 1, time(), Input::get('id'));

		if($bookingObj->numRows > 0)
		{
			while($bookingObj->next())
			{
                $reminderObj = Database::getInstance()
                    ->prepare('SELECT * FROM `tl_bbk_reminder` WHERE `pid`=? AND `booking_id`=?')
					->limit(1)
                	->execute(Input::get('id'), $bookingObj->id);

				//nur neuen Rueckstand anlegen wenn es diesen noch nicht gibt
				if($reminderObj->numRows == 0)
				{
					$set = array
					(
                        'pid' => Input::get('id'),
						'booking_id' => $bookingObj->id,
						'member_id' => $bookingObj->member_id,
						'bbk_id' =>  $bookingObj->bbk,
						'endDate' => $bookingObj->endShippingDate,
						'tstamp' => time()
					);
					$reminderID = $this->Database->prepare("INSERT INTO `tl_bbk_reminder` %s")->set($set)->execute()->insertId;
				}

			}
		}

		Message::addConfirmation($GLOBALS['TL_LANG']['tl_bbk_reminder']['MESSAGE']['Reminder_is_checked']);

        $logger = System::getContainer()->get('monolog.logger.contao');
        $logger->log(LogLevel::NOTICE, $GLOBALS['TL_LANG']['tl_bbk_reminder']['Reminder_is_checked']);

		$this->redirect(str_replace('&key=checkReminder', '', $this->Environment->request));
	}
}
