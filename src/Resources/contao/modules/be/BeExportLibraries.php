<?php

/**
 * PHP version 7
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    bz-bbk-bundle (BilderBuchKino)
 * @license    commercial
 * @filesource
 */
namespace Srhinow\BzBbkBundle\Modules;

use Contao\Backend;
use Contao\Database;
use Contao\Input;
use Contao\Session;
use Srhinow\BzBbkBundle\Models\BbkLibrariesModel;

/**
 * Class beExportLibraries
 */
class beExportLibraries extends Backend
{
	/**
	 * Export bookings for statistics
	 */
	public function csvExport()
	{

        $sessionFilter = Session::getInstance()->get('filter');

		if (Input::post('FORM_SUBMIT') == 'export_libraries')
		{
		    //set handle from file
		    $seperators = array('comma'=>',','semicolon'=>';','tabulator'=>"\t",'linebreak'=>"\n");
		    $listFields = Database::getInstance()->listFields('tl_bbk_libraries');
		    $fieldnames = array();
		    $ignorefields = array('tstamp','modify');
;
		    foreach($listFields as $k => $field)
		    {
                echo $field['name'].' ';
		    	if(in_array($field['name'],$ignorefields) || $field['type'] == 'index') continue;

		    	$fieldnames[] = $field['name'];	
		    }

		    // get records
		    $arrExport = array();
		    
		    $selectfields = implode(', ',$fieldnames);
            $libObj = Database::getInstance()->prepare('SELECT '.$selectfields.' FROM tl_bbk_libraries ORDER BY `name` ASC')->execute();

		    if($libObj->numRows < 1)
		    {
                Message::addError($GLOBALS['TL_LANG']['tl_bbk_reminder']['MESSAGE']['no_data']);

                $logger = System::getContainer()->get('monolog.logger.contao');
                $logger->log(LogLevel::ERROR, $GLOBALS['TL_LANG']['tl_bbk_reminder']['MESSAGE']['no_data']);

                $this->redirect(str_replace('&key=librariesCsvExport', '', $this->Environment->request));
		    }

		    while($libObj->next())
		    {
		    	$arrExport[] = $libObj->row();
		    }

		    // start output
		    $exportFile =  'contao_libraries_' . date("Ymd-Hi");
		    
		    $output = '"' . implode('"'.$seperators[$this->Input->post('separator')].'"', array_values($fieldnames)).'"' . "\n";

		    foreach ($arrExport as $export)
		    {
			    $output .= '"' . implode('"'.$seperators[$this->Input->post('separator')].'"', str_replace("\"", "\"\"", $export)).'"' . "\n";
		    }

		    ob_end_clean();
		    header('Content-Type: application/csv');
		    header('Content-Transfer-Encoding: binary');
		    header('Content-Disposition: attachment; filename="' . $exportFile .'.csv"');
		    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		    header('Pragma: public');
		    header('Expires: 0');
		    echo $output;
		    exit();

		}

    	// Return the form
		return '<div class="tl_content">
		    <div id="tl_buttons">
		    <a href="'.ampersand(str_replace('&key=librariesCsvExport', '', $this->Environment->request)).'" class="header_back" title="'.specialchars($GLOBALS['TL_LANG']['MSC']['backBT']).'" accesskey="b">'.$GLOBALS['TL_LANG']['MSC']['backBT'].'</a>
		    </div>
		    
		    <form action="'.ampersand($this->Environment->request, true).'" id="tl_bbk_csvexport" class="tl_form" method="post">
		    <div class="tl_formbody_edit">
			<input type="hidden" name="FORM_SUBMIT" value="export_libraries" />
			<input type="hidden" name="REQUEST_TOKEN" value="'.REQUEST_TOKEN.'" />
			<input type="hidden" name="pid" value="'.$this->Input->get('id').'" />
			<fieldset class="tl_box">
			    <div class="w50 widget">
			    <h3><label for="ctrl_bbk">CSV-Trenner</label></h3>
			    <select name="separator" id="separator" class="tl_select" onfocus="Backend.getScrollOffset();">
				<option value="semicolon">'.$GLOBALS['TL_LANG']['MSC']['semicolon'].' (;)</option>
				<option value="comma">'.$GLOBALS['TL_LANG']['MSC']['comma'].' (,)</option>
				<option value="tabulator">'.$GLOBALS['TL_LANG']['MSC']['tabulator'].'</option>
				<option value="linebreak">'.$GLOBALS['TL_LANG']['MSC']['linebreak'].'</option>
			    </select>'.(($GLOBALS['TL_LANG']['MSC']['separator'][1] != '') ? '<p class="tl_help tl_tip">'.$GLOBALS['TL_LANG']['MSC']['separator'][1].'</p>' : '').'
			    </div>
			</fieldset>
		    </div>

		    <div class="tl_formbody_submit">

		    <div class="tl_submit_container">
		      <input type="submit" name="save" id="save" class="tl_submit" accesskey="s" value="'.specialchars($GLOBALS['TL_LANG']['tl_bbk_libraries']['button_exportcsv']).'" />
		    </div>

		    </div>
		    </form></div>';
	}
}
