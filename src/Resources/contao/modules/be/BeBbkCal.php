<?php

/**
 * PHP version 7
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    bz-bbk-bundle (BilderBuchKino)
 * @license    commercial
 * @filesource
 */
namespace Srhinow\BzBbkBundle\Modules;


use Contao\Backend;
use Contao\Database;
use Contao\DataContainer;
use Contao\Input;
use Srhinow\BzBbkBundle\BbkCal;
use Srhinow\BzBbkBundle\Models\BbkLocationsModel;

class BeBbkCal extends Backend
{

    /**
	* show the Calendar-View in Backend
	* @var object
	*/
	public function showBeCal(DataContainer $dc)
	{

           $this->loadLanguageFile('tl_bbk');
           $GLOBALS['TL_CSS'][] = 'system/modules/BBK/html/bbkcal.css';

           if(Input::get('bbk'))
           {

	       $bbkObj = Database::getInstance()->prepare('SELECT `tl_bbk`.* FROM `tl_bbk` LEFT JOIN `tl_bbk_booking` ON `tl_bbk`.`id`=`tl_bbk_booking`.`bbk` WHERE `tl_bbk`.`id`=?')
					->limit(1)
					->execute(Input::get('bbk'));
           $bbkLocationName = '';
           if(null !== ($objBbkLocation = BbkLocationsModel::findByPk($bbkObj->pid))) {
               $bbkLocationName = $objBbkLocation->name;
           }

	       $firstMon = date('m.Y',strtotime('-2 month'));
	       $lastMon = '';
	       if (Input::post('FORM_SUBMIT') == 'show_calperiod')
	       {
		    if(Input::post('exportYear') != '')
		    {
		        $firstMon =  '01.'.Input::post('exportYear');
		        $lastMon  =  '12';
		    }
	       }

	       $returnHTML = '<div class="tl_content">
<div id="tl_buttons">
		    <a class="header_back" onclick="Backend.getScrollOffset()" accesskey="b" title="Zurück" href="'.$this->getReferer(true).'">Zurück</a>&nbsp;
		    <a onclick="Backend.getScrollOffset()" accesskey="n" title="Eine neue Buchung für dieses Bilderbuch anlegen" class="header_new" href="contao/main.php?do=bbk_locations&amp;table=tl_bbk_booking&amp;id='.Input::get('id').'&amp;act=create&amp;mode=2&amp;pid='.\Input::get('id').'&amp;bbk='.\Input::get('bbk').'&amp;rt='.\Input::get('rt').'">Neue Buchung</a>
		    </div>
		    <h2 class="sub_headline">Kalendaransicht von "'.$bbkObj->title.'"
		    <br>BBK-Nr.: '.$bbkObj->bbkNr.'
		    <br>Standort: '.$bbkLocationName.'
		    </h2>';
		$returnHTML .=     '
		<form action="'.ampersand($this->Environment->request, true).'" id="tl_bbk_csvexport" class="tl_form" method="post">
		    <div class="tl_formbody_edit">
			<input type="hidden" name="FORM_SUBMIT" value="show_calperiod" />
			<input type="hidden" name="REQUEST_TOKEN" value="'.REQUEST_TOKEN.'" />
			<fieldset class="tl_tbox block">
			    <div class="w50">
			    <h3><label for="ctrl_bbk">Buchungs-Kalender anzeigen für folgendes Jahr</label></h3>
			    <select name="exportYear" id="exportYear" class="tl_select" onfocus="Backend.getScrollOffset();">';
			    $optionsArr = array(
				'' => '-- Standart --',
                    date('Y',strtotime('+2 years')) => date('Y',strtotime('+2 years')),
				    date('Y',strtotime('+1 years')) => date('Y',strtotime('+1 years')),
				    date('Y') => date('Y'),
				    date('Y',strtotime('-1 year')) => date('Y',strtotime('-1 year')),
				    date('Y',strtotime('-2 year')) => date('Y',strtotime('-2 year')),
				);
				foreach($optionsArr as $kOption => $vOption)
				{
				    $selected =  (Input::post('exportYear') == $kOption) ? ' selected' : '';
				    $returnHTML .= '<option value="'.$kOption.'"'.$selected.'>'.$vOption.'</option>';
				}
                            $returnHTML .= '
			    </select>'.(($GLOBALS['TL_LANG']['MSC']['separator'][1] != '') ? '<p class="tl_help tl_tip">Den Kalender mit allen Buchungen für diesen Zeitraum anzeigen.</p>' : '').'
			    </div>
			    <div style="margin-top: 30px;">
			     <input type="submit" name="save" id="save" class="tl_submit" accesskey="s" value="aktualisiere den Zeitraum" />
			     </div>
			</fieldset>
		    </div>

		    </form>
    <div class="tl_formbody_edit">
    <fieldset class="tl_tbox nolegend">';
        $BBKCal = new BbkCal();
		$BBKCal->setFirstMon($firstMon);
		$BBKCal->setLastMon($lastMon);

		$returnHTML .= $BBKCal->createCal();
		$returnHTML .= '</fieldset></div>';
		$returnHTML .= "<script type=\"text/javascript\">
				    <!--//--><![CDATA[//><!--
				    window.addEvent('domready', function()
				    {
					    var myTips = new Tips($$('.tooltip'),
					    {
						    showDelay: 0,    //Verzögerung bei MouseOver
						    hideDelay: 100,   //Verzögerung bei MouseOut
						    className: 'tool', //CSS-Klassennamen --> CSS-Definitionen
						    offsets: {'x': 20, 'y': -100 }, // Versatz des Tooltips
						    fixed: false, // false = Tooltip bewegt sich mit dem Mauszeiger, true=Tooltip bewegt sich nicht
					    });
				    // zeigt den Tooltip bei Fokus an
					    $$('.tooltip').each(function(el) {
						    el.addEvent('focus', function(event){
							    myTips.elementEnter(event, el);
						    }).addEvent('blur', function(event){
							    myTips.elementLeave(event, el);
						    });
					    });
				    });
				    //--><!]]>
				    </script>";

//                 $this->setSession();
            $returnHTML .= "</div>";
		return $returnHTML;
	   }
	}

	protected function setSession()
	{
		$this->import('Session');
		$this->import('BackendUser');
		$session = $this->Session->getData();

		// Main script
		if ($this->Environment->script == 'contao/main.php')
		{
			$session['referer']['last'] = $session['referer']['current'];
			$session['referer']['current'] = $this->Environment->requestUri;
		}
		// File manager
		elseif ($this->Environment->script == 'contao/files.php')
		{
			$session['fileReferer']['last'] = $session['referer']['current'];
			$session['fileReferer']['current'] = $this->Environment->requestUri;
		}

		// Store the session data
		if ($this->BackendUser->id != '')
		{

			$this->Database->prepare("UPDATE `tl_user` SET session=? WHERE id=?")
						   ->execute(serialize($session), $this->BackendUser->id);
		}


	}

}
