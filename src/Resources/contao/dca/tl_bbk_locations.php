<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Table tl_bbk_locations
 */
$GLOBALS['TL_DCA']['tl_bbk_locations'] = array
(

	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
//		'ctable'                      => ['tl_bbk', 'tl_bbk_booking'],
		'enableVersioning'            => false,
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary'
			)
		)
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 2,
			'fields'                  => array('name'),
			'flag'                    => 1,
			'panelLayout'             => 'limit'
		),
		'label' => array
		(
			'fields'                  => array('name'),
			'format'                  => '%s',
// 			'label_callback'          => array('tl_bbk_locations', 'addPreviewImage')
		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),
		'operations' => array
		(
			'showcal' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_locations']['bbk_booking'],
				'href'                => 'table=tl_bbk_booking',
				'icon'                => $GLOBALS['BE_BBK']['PROPERTIES']['PUBLICSRC'].'/icons/app_16.png',
				'button_callback'     => array('srhinow.bzbbk.listeners.dca.bbk_locations', 'createButton')
			),
			'reminder' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_locations']['reminder'],
				'href'                => 'table=tl_bbk_reminder',
				'icon'                => $GLOBALS['BE_BBK']['PROPERTIES']['PUBLICSRC'].'/icons/warning.png',
				'button_callback'     => array('srhinow.bzbbk.listeners.dca.bbk_locations', 'createButton')
			),
			'bbk' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_locations']['bbk'],
				'href'                => 'table=tl_bbk',
				'icon'                => $GLOBALS['BE_BBK']['PROPERTIES']['PUBLICSRC'].'/icons/media_book.png',
				'button_callback'     => array('srhinow.bzbbk.listeners.dca.bbk_locations', 'createButton')
			),
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_locations']['edit'],
				'href'                => 'act=edit',
				'icon'                => $GLOBALS['BE_BBK']['PROPERTIES']['PUBLICSRC'].'/icons/process.png'
			),
// 			'delete' => array
// 			(
// 				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_locations']['delete'],
// 				'href'                => 'act=delete',
// 				'icon'                => 'delete.gif',
// 				'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
// 			),



		)
	),

	// Palettes
	'palettes' => array
	(
		'default'                     => '{title_legend},name,alias;{email_legend},sender,senderName,booking_email;{confirmed_legend:hide},confirmed_subject,confirmed_html_email,confirmed_text_email;{rejected_legend:hide},rejected_subject,rejected_html_email,rejected_text_email'
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'name' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_locations']['name'],
			'exclude'                 => true,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'decodeEntities'=>true),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'alias' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_locations']['alias'],
			'exclude'                 => true,
			'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'alnum', 'doNotCopy'=>true, 'spaceToUnderscore'=>true, 'maxlength'=>128, 'tl_class'=>'w50'),
			'save_callback' => array
			(
				array('srhinow.bzbbk.listeners.dca.bbk_locations', 'generateAlias'),
				array('srhinow.bzbbk.listeners.dca.bbk_locations', 'updateBbkIfAliasChanges')
			),
			'sql'                     => "varchar(128) NOT NULL default ''"
		),
	    'sender' => array
	    (
		    'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_locations']['sender'],
		    'exclude'                 => true,
		    'search'                  => true,
		    'filter'                  => true,
		    'inputType'               => 'text',
		    'eval'                    => array('rgxp'=>'email', 'maxlength'=>128, 'decodeEntities'=>true, 'tl_class'=>'clr w50'),
		    'sql'                     => "varchar(255) NOT NULL default ''"
	    ),
	    'senderName' => array
	    (
		    'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_locations']['senderName'],
		    'exclude'                 => true,
		    'search'                  => true,
		    'filter'                  => true,
		    'inputType'               => 'text',
		    'eval'                    => array('rgxp'=>'alpha', 'maxlength'=>255, 'decodeEntities'=>true, 'tl_class'=>'w50'),
		    'sql'                     => "varchar(255) NOT NULL default ''"
	    ),
	    'booking_email' => array
	    (
		    'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_locations']['booking_email'],
		    'exclude'                 => true,
		    'search'                  => true,
		    'sorting'                 => true,
		    'inputType'               => 'text',
		    'eval'                    => array('rgxp'=>'email', 'maxlength'=>128, 'decodeEntities'=>true, 'tl_class'=>'w50'),
		    'sql'                     => "varchar(255) NOT NULL default ''"
	    ),
	    'confirmed_subject' => array
	    (
		    'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_locations']['confirmed_subject'],
		    'exclude'                 => true,
		    'search'                  => true,
		    'sorting'                 => true,
//		    'flag'                    => 11,
		    'inputType'               => 'text',
		    'default'		  => &$GLOBALS['TL_LANG']['tl_bbk_locations']['confirmed_subject_default'],
		    'eval'                    => array( 'maxlength'=>255, 'tl_class'=>'clr long', 'decodeEntities'=>true),
		    'sql'                     => "varchar(128) NOT NULL default ''"
	    ),
	    'confirmed_html_email' => array
	    (
		    'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_locations']['confirmed_html_email'],
		    'exclude'                 => true,
		    'flag'                    => 11,
		    'inputType'               => 'textarea',
            'default'		  => &$GLOBALS['TL_LANG']['tl_bbk_locations']['confirmed_html_default'],
		    'eval'                    => array('rte'=>'tinyMCE', 'helpwizard'=>true, 'style'=>'height:60px;', 'tl_class'=>'clr', 'decodeEntities'=>true),
		    'sql'                     => "text NULL"
	    ),
	    'confirmed_text_email' => array
	    (
		    'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_locations']['confirmed_text_email'],
		    'exclude'                 => true,
		    'search'                  => true,
		    'inputType'               => 'textarea',
            'default'		  => &$GLOBALS['TL_LANG']['tl_bbk_locations']['confirmed_text_default'],
		    'eval'                    => array('decodeEntities'=>true),
		    'sql'                     => "text NULL"
	    ),
	    'rejected_subject' => array
	    (
		    'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_locations']['rejected_subject'],
		    'exclude'                 => true,
		    'search'                  => true,
		    'sorting'                 => true,
		    'flag'                    => 11,
		    'inputType'               => 'text',
		    'default'		  => &$GLOBALS['TL_LANG']['tl_bbk_locations']['rejected_subject_default'],
		    'eval'                    => array( 'maxlength'=>255, 'tl_class'=>'clr long', 'decodeEntities'=>true),
		    'sql'                     => "varchar(128) NOT NULL default ''"
	    ),
	    'rejected_html_email' => array
	    (
		    'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_locations']['rejected_html_email'],
		    'exclude'                 => true,
		    'flag'                    => 11,
		    'inputType'               => 'textarea',
            'default'		  => &$GLOBALS['TL_LANG']['tl_bbk_locations']['rejected_html_default'],
		    'eval'                    => array('rte'=>'tinyMCE', 'helpwizard'=>true,'style'=>'height:60px;', 'tl_class'=>'clr'),
		    'sql'                     => "text NULL"
	    ),
	    'rejected_text_email' => array
	    (
		    'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_locations']['rejected_text_email'],
		    'exclude'                 => true,
		    'search'                  => true,
		    'inputType'               => 'textarea',
            'default'		  => &$GLOBALS['TL_LANG']['tl_bbk_locations']['rejected_text_default'],
		    'eval'                    => array('decodeEntities'=>true),
		    'sql'                     => "text NULL"
	    ),

	)
);
