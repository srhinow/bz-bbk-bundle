<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Table tl_bbk_booking
 */

$GLOBALS['TL_DCA']['tl_bbk_booking'] = array
(

	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
		'ptable'                      => 'tl_bbk_locations',
		'enableVersioning'            => true,
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
				'pid,startDate,endDate,accepted' => 'index'
			)
		),
		'oncreate_callback' => array
		(
			array('srhinow.bzbbk.listeners.dca.bbk_booking', 'setBbk4NewBooking')
		),
		'onsubmit_callback' => array
		(
		    array('srhinow.bzbbk.listeners.dca.bbk_booking','fillShippingStart'),
		    array('srhinow.bzbbk.listeners.dca.bbk_booking','fillShippingEnd'),
		    array('srhinow.bzbbk.listeners.dca.bbk_booking','fillLoan'),
		    array('srhinow.bzbbk.listeners.dca.bbk_booking', 'sendReservationEmail'),
		    array('srhinow.bzbbk.listeners.dca.bbk_booking', 'inHouseToDos'),
		)

	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 2,
			'fields'                  => array('accepted','startDate','bbk'),
			'panelLayout'             => 'filter;sort,limit;search',
			'headerFields'            => array('startDate'),
			'child_record_class'      => 'no_padding'
		),
		'label' => array
		(
			'fields'                  => array('library', 'startDate', 'endDate', 'bbk'),
			'format'                  => 'Standort: %s, Zeitraum: %s - %s, BBK: %s',
			'label_callback'          => array('srhinow.bzbbk.listeners.dca.bbk_booking', 'listEntries'),
		),
		'global_operations' => array
		(
			'runList' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_booking']['runList'],
				'href'                => 'key=runList',
				'class'               => 'export_pdf',
				'attributes'          => 'onclick="Backend.getScrollOffset();"'
			),
			'csvExportBookings' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_booking']['csvExportBookings'],
				'href'                => 'key=csvExportBookings',
				'class'               => 'export_csv',
				'attributes'          => 'onclick="Backend.getScrollOffset();"'
			),
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),
		'operations' => array
		(
			'booking' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_booking']['booking'],
				'href'                => 'table=tl_bbk_booking&key=showcal',
				'button_callback'     => array('srhinow.bzbbk.listeners.dca.bbk_booking', 'editBooking'),
				'icon'                => $GLOBALS['BE_BBK']['PROPERTIES']['PUBLICSRC'].'/icons/calendar.png'
			),
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_booking']['edit'],
				'href'                => 'act=edit',
				'icon'                => 'edit.gif'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_booking']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.gif'
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_booking']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_booking']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.gif'
			)
		)
	),

	// Palettes
	'palettes' => array
	(
		'default'                     => '
		{location_legend:hide},pid;
		{title_legend},bbk;
		{datefields_legend},startDate,endDate,startShippingDate,endShippingDate,loan;library_template;
		{address_legend},privaddress,firstname,lastname,library,street,postal,city;
		{contact_legend},phone,email;accepted;
		{email_legend::hide},subject,html_email,text_email,sendEmail;
		{more_legend},inHouse,returnDate,memo',
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'pid' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['location'],
			'foreignKey'              => 'tl_bbk_locations.name',
			'inputType'               => 'select',
			'options_callback'        => array('srhinow.bzbbk.listeners.dca.bbk_booking','getLocationOptions'),
			'sql'                     => "int(10) unsigned NOT NULL default '0'",
			'relation'                => array('type'=>'belongsTo', 'load'=>'eager')
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'modify' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'member_id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
        'library_id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
		'library_template' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['library_template'],
			'exclude'                 => true,
			'search'                  => false,
			'sorting'                 => false,
			'inputType'               => 'select',
			'options_callback'        => array('srhinow.bzbbk.listeners.dca.bbk_booking', 'getLibraryOptions'),
			'eval'                    => array('includeBlankOption'=>true,'submitOnChange'=>true, 'chosen'=>true,'tl_class'=>'w50'),
			'sql'                     => "int(10) unsigned NOT NULL default '0'",
			'save_callback' => array
			(
				array('srhinow.bzbbk.listeners.dca.bbk_booking', 'fillLibraryFields')
			)
		),
		'bbk' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['bbk'],
			'exclude'                 => false,
			'search'                  => false,
			'sorting'                 => true,
			'filter'                  => true,
			'flag'                    => 3,
			'inputType'               => 'select',
			'foreignKey'              => 'tl_bbk.title',
			'options_callback'        => array('srhinow.bzbbk.listeners.dca.bbk_booking', 'getBbkOptions'),
			'eval'                    => array('mandatory'=>true,'includeBlankOption'=>true,'submitOnChange'=>false, 'chosen'=>true,'tl_class'=>'','isAssociative'=>true),
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'location' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['location'],
			'filter'                  => false,
			'inputType'               => 'select',
			'options_callback'        => array('srhinow.bzbbk.listeners.dca.bbk_booking','getLocationOptions'),
			'eval'                    => array('mandatory'=>false,'includeBlankOption'=>true,'submitOnChange'=>true, 'tl_class'=>'w50'),
			'sql'                     => "varchar(155) NOT NULL default ''",
			'save_callback' => array
			(
				array('srhinow.bzbbk.listeners.dca.bbk_booking', 'correctLocation')
			)
		),
		'firstname' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['firstname'],
			'exclude'                 => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array(
                'mandatory'=>false,
                'maxlength'=>255,
                'feEditable'=>true,
                'feViewable'=>true,
                'feGroup'=>'personal',
                'tl_class'=>'w50',
                'decodeEntities'=>true
            ),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'lastname' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['lastname'],
			'exclude'                 => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array(
                'mandatory'=>false,
                'maxlength'=>255,
                'feEditable'=>true,
                'feViewable'=>true,
                'feGroup'=>'personal',
                'tl_class'=>'w50',
                'decodeEntities'=>true
            ),
			'sql'                     => "varchar(255) NOT NULL default ''",
		),
		'library' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['library'],
			'exclude'                 => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array(
                'mandatory'=>false,
                'maxlength'=>255,
                'feEditable'=>true,
                'feViewable'=>true,
                'feGroup'=>'personal',
                'tl_class'=>'w50',
                'decodeEntities'=>true
            ),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
        'privaddress' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['privaddress'],
            'sorting'                 => true,
            'filter'                  => true,
            'inputType'               => 'checkbox',
            'flag'                    => 11,
            'eval'                    => array('readonly'=>true),
            'sql'					  => "char(1) NOT NULL default ''"
        ),
		'street' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['street'],
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array(
                'maxlength'=>255,
                'feEditable'=>true,
                'feViewable'=>true,
                'feGroup'=>'address',
                'tl_class'=>'w50',
                'decodeEntities'=>true
            ),
			'sql'                     => "varchar(255) NOT NULL default ''",
		),
		'postal' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['postal'],
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('maxlength'=>32, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'address', 'tl_class'=>'w50'),
			'sql'                     => "varchar(32) NOT NULL default ''"
		),
		'city' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['city'],
			'exclude'                 => true,
			'filter'                  => true,
			'search'                  => true,
			'sorting'                 => true,
			'inputType'               => 'text',
			'eval'                    => array(
                'mandatory'=>true,
                'maxlength'=>255,
                'feEditable'=>true,
                'feViewable'=>true,
                'feGroup'=>'address',
                'tl_class'=>'w50',
                'decodeEntities'=>true
            ),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'phone' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['phone'],
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('maxlength'=>64, 'rgxp'=>'phone', 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'contact', 'tl_class'=>'w50'),
			'sql'                     => "varchar(64) NOT NULL default ''"
		),
		'email' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['email'],
			'exclude'                 => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>false, 'rgxp'=>'email', 'maxlength'=>128, 'decodeEntities'=>true, 'tl_class'=>'w50'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'accepted' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['accepted'],
			'exclude'                 => true,
			'filter'                  => true,
			'sorting'                 => true,
			'inputType'               => 'select',
			'options'                 => &$GLOBALS['TL_LANG']['tl_bbk_booking']['acceptedOptions'],
			'flag'                    => 1,
			'eval'                    => array('doNotCopy'=>true,'submitOnChange'=>true),
			'sql'                     => "char(1) NOT NULL default ''",
			'save_callback'	          => array
			(
			    array('srhinow.bzbbk.listeners.dca.bbk_booking','UpdateEntry')
			)
		),
		'addedOn' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['addedOn'],
			'filter'                  => true,
			'sorting'                 => true,
			'flag'                    => 8,
			'eval'                    => array('rgxp'=>'datim'),
			'sql'                     => "varchar(10) NOT NULL default ''",
		),
		'subject' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['subject'],
			'exclude'                 => true,
			'flag'                    => 11,
			'inputType'               => 'text',
			'eval'                    => array( 'maxlength'=>255, 'tl_class'=>'clr long','decodeEntities'=>true),
			'sql'                     => "varchar(128) NOT NULL default ''"
		),
		'html_email' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['html_email'],
			'exclude'                 => true,
			'flag'                    => 11,
			'inputType'               => 'textarea',
			'eval'                    => array('rte'=>'tinyMCE', 'helpwizard'=>true,'style'=>'height:60px;', 'tl_class'=>'clr'),
			'sql'                     => "text NULL"
		),
		'text_email' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['text_email'],
			'exclude'                 => true,
			'inputType'               => 'textarea',
			'eval'                    => array('decodeEntities'=>true),
			'sql'                     => "text NULL"
		),
		'sendEmail' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['sendEmail'],
// 			'search'                  => true,
// 			'sorting'                 => true,
			'inputType'               => 'checkbox',
			'flag'                    => 11,
			'sql'                     => "char(1) NOT NULL default ''",
		),
		'startDate' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['startDate'],
			'filter'                  => true,
			'sorting'                 => true,
			'flag'                    => 6,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true,'rgxp'=>'date', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
			'sql'                     => "varchar(10) NOT NULL default ''"
		),
		'endDate' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['endDate'],
			'filter'                  => true,
			'sorting'                 => true,
			'flag'                    => 6,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true,'rgxp'=>'date', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
			'sql'                     => "varchar(10) NOT NULL default ''"
		),
		'startShippingDate' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['startShippingDate'],
			'filter'                  => true,
			'sorting'                 => true,
			'flag'                    => 6,
			'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'date', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
			'sql'                     => "varchar(10) NOT NULL default ''"

		),
		'endShippingDate' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['endShippingDate'],
			'filter'                  => true,
			'sorting'                 => true,
			'flag'                    => 6,
			'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'date', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
			'sql'                     => "varchar(10) NOT NULL default ''"
		),
		'loan' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['loan'],
			'filter'                  => false,
			'sorting'                 => true,
			'flag'                    => 6,
			'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'digit','tl_class'=>'w50'),
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'memo' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['memo'],
			'search'                  => true,
			'inputType'               => 'textarea',
			'eval'                    => array('style'=>'height:60px;', 'tl_class'=>'clr','decodeEntities'=>true),
			'sql'                     => "text NULL"
		),
		'inHouse' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['inHouse'],
			'filter'                  => true,
			'sorting'                 => true,
			'inputType'               => 'checkbox',
			'flag'                    => 11,
			'eval'                    => array('tl_class'=>'w50'),
			'sql'                     => "char(1) NOT NULL default ''"
		),
		'returnDate' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['returnDate'],
			'filter'                  => true,
			'sorting'                 => true,
			'flag'                    => 6,
			'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'date', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
			'sql'                     => "varchar(10) NOT NULL default ''"
		),
	)
);
