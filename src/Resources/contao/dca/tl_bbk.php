<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Table bbk
 */
$GLOBALS['TL_DCA']['tl_bbk'] = array
(
	// Config
	'config' => array
	(
		'dataContainer'   => 'Table',
		'ptable'          => 'tl_bbk_locations',
		'switchToEdit'    => true,
		'enableVersioning'=> false,
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
				'pid' => 'index'
			)
		),
		'onsubmit_callback' => array
		(
			array('srhinow.bzbbk.listeners.dca.bbk', 'generateCreateTstamp'),
			array('srhinow.bzbbk.listeners.dca.bbk', 'fillEmptyLocations')
		)
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 1,
			'fields'                  => array('title ASC,location'),
			'flag'                    => 12,
			'panelLayout'             => 'filter;search,limit'
		),
		'label' => array
		(
			'fields'                  => array('title', 'author','bbkNr', 'location','published'),
			'format'                  => '"%s" (%s) BBK-Nr: %s, Standort: %s',
			'showColumns'             => true,
// 			'label_callback'          => array('tl_bbk', 'listEntries'),
		),
		'global_operations' => array
		(

			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset();" accesskey="e"'
			)
		),
		'operations' => array
		(
			'booking' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk']['booking'],
				'href'                => 'table=tl_bbk_booking&key=showcal',
				'button_callback'     => array('srhinow.bzbbk.listeners.dca.bbk', 'editBooking'),
				'icon'                => $GLOBALS['BE_BBK']['PROPERTIES']['PUBLICSRC'].'/icons/calendar.png'
			),
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk']['edit'],
				'href'                => 'table=tl_bbk&act=edit',
				'icon'                => 'edit.gif',
				'attributes'          => 'class="contextmenu"'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.gif'
			),

			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if (!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\')) return false; Backend.getScrollOffset();"',
			),


		)
	),

	// Palettes
	'palettes' => array
	(
		'__selector__'                => array(),
		'default'                     => 'title,bbkNr,barcode,imageUrl;mediatyp,pid,ageRecommendation;author,otherPerson1,otherPerson2,publisher,releaseDate,created_tstamp,collation,tags,annotation;published'
	),

	// Subpalettes
	'subpalettes' => array
	(

	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'pid' => array
		(
            'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['location'],
            'filter'                  => false,
            'inputType'               => 'select',
            'foreignKey'              => 'tl_bbk_locations.name',
            'eval'                    => array('mandatory'=>true, 'tl_class'=>'w50'),
			'sql'                     => "int(10) unsigned NOT NULL default '0'",
            'save_callback' => array
            (
                array('srhinow.bzbbk.listeners.dca.bbk','changeLocation')
            )
		),
		'tstamp' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_rms']['tstamp'],
			'filter'                  => true,
			'sorting'                 => true,
			'flag'                    => 6,
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'created_tstamp' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['created_tstamp'],
			'exclude'                 => true,
			'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'date', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'bbkNr' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['bbkNr'],
			'search'                  => true,
			'flag'					=> 3,
			'lenght'				=> 4,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true, 'maxlength'=>155, 'tl_class'=>'w50','decodeEntities'=>true),
			'sql'                     => "varchar(155) NOT NULL default ''"
		),
		'barcode' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['barcode'],
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true, 'maxlength'=>155, 'tl_class'=>'w50','decodeEntities'=>true),
			'sql'                     => "varchar(155) NOT NULL default ''"
		),
		'title' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['title'],
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true, 'maxlength'=>255,  'tl_class'=>'long clr','decodeEntities'=>true),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'imageUrl' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['imageUrl'],
            'inputType'               => 'fileTree',
            'eval'                    => array('filesOnly'=>true, 'extensions'=>Config::get('validImageTypes'), 'fieldType'=>'radio', 'mandatory'=>true,'tl_class'=>'long clr'),
            'sql'                     => "binary(16) NULL"
		),
        'imageUrlStr' => array
        (
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
		'mediatyp' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['mediatyp'],
			'filter'                  => true,
			'inputType'               => 'select',
			'reference'				  => &$GLOBALS['TL_LANG']['tl_bbk']['mediatyp_options'],
			'options'				  => &$GLOBALS['TL_LANG']['tl_bbk']['mediatyp_options'],
			'eval'                    => array('mandatory'=>true, 'tl_class'=>'w50'),
			'sql'                     => "varchar(155) NOT NULL default ''"
		),
		'location' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['location'],
			'sql'                     => "varchar(155) NOT NULL default ''",
		),
		'ageRecommendation' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['ageRecommendation'],
			'filter'                  => true,
			'inputType'               => 'select',
			'options' 		  => array('1','2','3','4','5','6','7','9','10','11','12','13','14','15','16','17','18'),
			'reference' 		  => &$GLOBALS['TL_LANG']['tl_bbk'],
			'eval'                    => array('mandatory'=>true, 'tl_class'=>'w50'),
			'sql'                     => "varchar(55) NOT NULL default ''"
		),
		'author' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['author'],
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>false, 'maxlength'=>255,  'tl_class'=>'long clr','decodeEntities'=>true),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'otherPerson1' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['otherPerson1'],
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>false, 'maxlength'=>255,  'tl_class'=>'long clr','decodeEntities'=>true),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'otherPerson2' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['otherPerson2'],
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>false, 'maxlength'=>255,  'tl_class'=>'long clr','decodeEntities'=>true),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'publisher' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['publisher'],
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>false, 'maxlength'=>255,  'tl_class'=>'long clr','decodeEntities'=>true),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'releaseDate' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['releaseDate'],
			'search'                  => true,
			'inputType'               => 'text',
			'default'		  => date('Y'),
			'eval'                    => array('mandatory'=>false, 'maxlength'=>4, 'tl_class'=>'w50', 'rgxp' => 'digit'),
			'sql'                     => "varchar(10) NOT NULL default ''"
		),
		'collation' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['collation'],
			'search'                  => true,
			'inputType'               => 'textarea',
			'eval'                    => array('style'=>'height:60px;', 'tl_class'=>'clr','decodeEntities'=>true),
			'explanation'             => 'insertTags',
			'sql'                     => "text NULL"
		),
		'tags' => array
		(
			'inputType'               => 'tag',
 			'exclude'                 => false,
			'eval'                    => array('tl_class'=>'clr long','isTag'=>true,'table'=>'tl_bbk'),
			'sql'                     => "text NULL"
		),
		'annotation' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['annotation'],
			'search'                  => true,
			'inputType'               => 'textarea',
			'eval'                    => array('style'=>'height:60px;', 'tl_class'=>'clr','decodeEntities'=>true),
			'explanation'             => 'insertTags',
			'sql'                     => "text NULL"
		),
		'memo' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['memo'],
			'search'                  => true,
			'inputType'               => 'textarea',
			'eval'                    => array('style'=>'height:60px;', 'tl_class'=>'clr','decodeEntities'=>true),
			'sql'                     => "text NULL"
		),
		'published' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['published'],
			'exclude'                 => true,
			'filter'                  => true,
			'flag'                    => 1,
			'inputType'               => 'checkbox',
			'eval'                    => array('doNotCopy'=>true, 'tl_class'=>'w50'),
			'sql'                     => "char(1) NOT NULL default ''"
		),
		'loaned' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk']['loaned'],
			'exclude'                 => true,
			'filter'                  => true,
			'flag'                    => 1,
			'inputType'               => 'checkbox',
			'eval'                    => array('doNotCopy'=>true, 'tl_class'=>'w50'),
			'sql'                     => "char(1) NOT NULL default ''"
		),
	)
);

