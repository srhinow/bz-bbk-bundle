<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Table tl_bbk_libraries
 */
$GLOBALS['TL_DCA']['tl_bbk_libraries'] = array
(

	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
//		'ctable'                      => ['tl_bbk'],
		'enableVersioning'            => true,
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary'
			)
		)
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 2,
			'fields'                  => array('name'),
			'panelLayout'             => 'filter;sort,limit;search',
			'headerFields'            => array('startDate'),
			'child_record_class'      => 'no_padding'
		),
		'label' => array
		(
			'fields'                  => array('name', 'plz', 'city'),
			'format'                  => '%s %s %s',
// 			'label_callback'          => array('tl_bbk', 'listEntries'),
		),
		'global_operations' => array
		(
            'librariesCsvExport' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['csvExport'],
                'href'                => 'key=librariesCsvExport',
                'class'               => 'export_csv',
                'attributes'          => 'onclick="Backend.getScrollOffset();"'
            ),
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['edit'],
				'href'                => 'act=edit',
				'icon'                => 'edit.gif'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.gif'
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.gif'
			)
		)
	),

	// Palettes
	'palettes' => array
	(
		'default'                     => 'member_id;name;{contact_legend},privaddress,firstname,lastname,street,postal,city,state,country,phone,mobile,fax,email,website',
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'modify' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'member_id' => array
		(
		    'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['member_id'],
		    'exclude'                 => false,
		    'filter'                  => true,
		    'sorting'                 => true,
		    'inputType'               => 'select',
		    'options_callback'        => array('srhinow.bzbbk.listeners.dca.bbk_libraries', 'getMemberOptions'),
		    'eval'                    => array(
				'includeBlankOption'=>true,
				'chosen'=>true,
				'feEditable'=>true,
				'feViewable'=>true,
				'feGroup'=>'address',
				'tl_class'=>'long'
			),
		    'sql'					  => "int(10) unsigned NOT NULL default '0'"
		),
		'privaddress' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['privaddress'],			
			'sorting'                 => true,
			'filter'                  => true,
			'inputType'               => 'checkbox',
			'flag'                    => 11,
			'sql'					  => "char(1) NOT NULL default ''"
		),
		'name' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['name'],
			'exclude'                 => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array(
				'mandatory'=>false,
				'maxlength'=>255,
				'feEditable'=>true,
				'feViewable'=>true,
				'feGroup'=>'personal',
				'tl_class'=>'w50',
				'decodeEntities'=>true
			),
			'sql'                     => "varchar(128) NOT NULL default ''"
		),
		'firstname' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['firstname'],
			'exclude'                 => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array(
				'mandatory'=>false,
				'maxlength'=>255,
				'feEditable'=>true,
				'feViewable'=>true,
				'feGroup'=>'personal',
				'tl_class'=>'w50',
				'decodeEntities'=>true
			),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'lastname' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['lastname'],
			'exclude'                 => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array(
				'mandatory'=>false,
				'maxlength'=>255,
				'feEditable'=>true,
				'feViewable'=>true,
				'feGroup'=>'personal',
				'tl_class'=>'w50',
				'decodeEntities'=>true
			),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'street' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['street'],
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array(
				'maxlength'=>255,
				'feEditable'=>true,
				'feViewable'=>true,
				'feGroup'=>'address',
				'tl_class'=>'w50',
				'decodeEntities'=>true
			),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'postal' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['postal'],
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array(
				'maxlength'=>32,
				'feEditable'=>true,
				'feViewable'=>true,
				'feGroup'=>'address',
				'tl_class'=>'w50'
			),
			'sql'                     => "varchar(32) NOT NULL default ''"
		),
		'city' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['city'],
			'exclude'                 => true,
			'filter'                  => true,
			'search'                  => true,
			'sorting'                 => true,
			'inputType'               => 'text',
			'eval'                    => array(
				'mandatory'=>true,
				'maxlength'=>255,
				'feEditable'=>true,
				'feViewable'=>true,
				'feGroup'=>'address',
				'tl_class'=>'w50',
				'decodeEntities'=>true
			),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'state' => array
		(
		    'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['state'],
		    'exclude'                 => true,
		    'filter'                  => true,
		    'sorting'                 => true,
		    'inputType'               => 'select',
		    'default'		      => 'niedersachsen',
		    'options'                 => &$GLOBALS['TL_LANG']['DE_STATES'],
		    'eval'                    => array(
				'includeBlankOption'=>true,
				'chosen'=>true,
				'feEditable'=>true,
				'feViewable'=>true,
				'feGroup'=>'address',
				'tl_class'=>'w50',
				'decodeEntities'=>true
			),
		    'sql'                     => "varchar(64) NOT NULL default ''"
		),
		'country' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['country'],
			'exclude'                 => true,
			'filter'                  => true,
			'sorting'                 => true,
			'inputType'               => 'select',
			'default' 		  		  => 'de',
			'options'                 => $this->getCountries(),
			'eval'                    => array('includeBlankOption'=>true, 'chosen'=>true, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'address', 'tl_class'=>'w50'),
			'sql'                     => "varchar(2) NOT NULL default ''"
		),
		'phone' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['phone'],
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array(
				'maxlength'=>64,
				'rgxp'=>'phone',
				'feEditable'=>true,
				'feViewable'=>true,
				'feGroup'=>'contact',
				'tl_class'=>'w50'
			),
			'sql'                     => "varchar(64) NOT NULL default ''"
		),
		'mobile' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['mobile'],
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array(
				'maxlength'=>64,
				'rgxp'=>'phone',
				'decodeEntities'=>true,
				'feEditable'=>true,
				'feViewable'=>true,
				'feGroup'=>'contact',
				'tl_class'=>'w50'
			),
			'sql'                     => "varchar(64) NOT NULL default ''"
		),
		'fax' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['fax'],
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array(
				'maxlength'=>64,
				'rgxp'=>'phone',
				'decodeEntities'=>true,
				'feEditable'=>true,
				'feViewable'=>true,
				'feGroup'=>'contact',
				'tl_class'=>'w50'
			),
			'sql'                     => "varchar(64) NOT NULL default ''"
		),
		'email' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['email'],
			'exclude'                 => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array(
				'mandatory'=>false,
				'rgxp'=>'email',
				'maxlength'=>128,
				'decodeEntities'=>true,
				'tl_class'=>'clr'
			),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'website' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_libraries']['website'],
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array(
				'rgxp'=>'url',
				'maxlength'=>255,
				'feEditable'=>true,
				'feViewable'=>true,
				'feGroup'=>'contact',
				'tl_class'=>'w50'
			),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		// for old datasets
		'memo' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['memo'],
			'search'                  => true,
			'inputType'               => 'textarea',
			'eval'                    => array(
				'style'=>'height:60px;',
				'tl_class'=>'clr',
				'decodeEntities'=>true
			),
			'sql'                     => "text NULL"
		)
	)
);


