<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Table tl_bbk_reminder
 */

$GLOBALS['TL_DCA']['tl_bbk_reminder'] = array
(

	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
		'ptable'                      => 'tl_bbk_locations',
		'enableVersioning'            => false,
		'closed'                      => true,
		'notEditable'                 => true,
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
				'pid,endDate' => 'index'
			)
		),
		'oncreate_callback' => array
		(
			array('tl_bbk_reminder', 'setBooking4NewReminder')
		),
		'onsubmit_callback' => array
		(
		    array('tl_bbk_reminder','fillShippingStart'),
		    array('tl_bbk_reminder','fillShippingEnd'),
		    array('tl_bbk_reminder','fillLoan'),
		    array('tl_bbk_reminder', 'sendReservationEmail')
		)

	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 1,
			'flag'					=> 8,
			'fields'                  => array('endDate'),
			'panelLayout'             => 'limit',
			'child_record_class'      => 'no_padding'
		),
		'label' => array
		(
			'fields'                  => array('booking_id'),
			'format'                  => ' %s',
			'label_callback'          => array('tl_bbk_reminder', 'listEntries'),
		),
		'global_operations' => array
		(
			'checkReminder' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_reminder']['checkReminder'],
				'href'                => 'key=checkReminder',
				'class'               => 'check_reminder',
				'attributes'          => 'onclick="Backend.getScrollOffset();"'
			),
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),
		'operations' => array
		(
			'bookingcal' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_reminder']['bookingcal'],
				'href'                => 'table=tl_bbk_reminder&key=showcal',
				'button_callback'     => array('tl_bbk_reminder', 'calButton'),
				'icon'                => 'system/modules/BBK/html/calendar.png'
			),
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_reminder']['edit'],
				'href'                => 'act=edit',
				'button_callback'     => array('tl_bbk_reminder', 'editButton'),
				'icon'                => 'edit.gif'
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_bbk_reminder']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
			),
		)
	),

	// Palettes
	'palettes' => array
	(
		'default'                     => '{location_legend:hide},location;{title_legend},booking_id;{email_legend::hide},subject,html_email,text_email,sendEmail;{more_legend},memo',
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'pid' => array
		(
			'foreignKey'              => 'tl_bbk.title',
			'sql'                     => "int(10) unsigned NOT NULL default '0'",
			'relation'                => array('type'=>'belongsTo', 'load'=>'eager')
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'member_id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'endDate' => array
		(
			'sql'                     => "varchar(10) NOT NULL default ''"
		),
		'subject' => array
		(
			'sql'                     => "varchar(128) NOT NULL default ''"
		),
		'library_template' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_reminder']['library_template'],
			'exclude'                 => true,
			'search'                  => false,
			'sorting'                 => false,
			'inputType'               => 'select',
			'options_callback'        => array('tl_bbk_reminder', 'getLibraryOptions'),
			'eval'                    => array('includeBlankOption'=>true,'submitOnChange'=>true, 'chosen'=>true,'tl_class'=>'w50'),
			'save_callback' => array
			(
				array('tl_bbk_reminder', 'fillLibraryFields')
			),
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'bbk_id' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_booking']['bbk_id'],
			'exclude'                 => false,
			'search'                  => false,
			'sorting'                 => true,
			'filter'                  => true,
			'flag'                    => 3,
			'inputType'               => 'select',
			'foreignKey'              => 'tl_bbk.title',
			'options_callback'        => array('tl_bbk_reminder', 'getBbkOptions'),
			'eval'                    => array('mandatory'=>true,'includeBlankOption'=>true,'submitOnChange'=>false, 'chosen'=>true,'tl_class'=>'','isAssociative'=>true),
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'booking_id' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_reminder']['booking'],
			'exclude'                 => false,
			'search'                  => false,
			'sorting'                 => true,
			'filter'                  => true,
			'flag'                    => 3,
			'inputType'               => 'select',
// 			'foreignKey'              => 'tl_bbk_booking.title',
			'options_callback'        => array('tl_bbk_reminder', 'getBookingOptions'),
			'eval'                    => array('mandatory'=>true,'includeBlankOption'=>true,'submitOnChange'=>false, 'chosen'=>true,'tl_class'=>'','isAssociative'=>true),
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'html_email' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_reminder']['html_email'],
			'exclude'                 => true,
			'flag'                    => 11,
			'inputType'               => 'textarea',
			'eval'                    => array('rte'=>'tinyMCE', 'helpwizard'=>true,'style'=>'height:60px;', 'tl_class'=>'clr'),
			'sql'                     => "text NULL"
		),
		'text_email' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_reminder']['text_email'],
			'exclude'                 => true,
			'inputType'               => 'textarea',
			'eval'                    => array('decodeEntities'=>true),
			'sql'                     => "text NULL"
		),
		'sendEmail' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_reminder']['sendEmail'],
			'inputType'               => 'checkbox',
			'flag'                    => 11,
			'sql'                     => "char(1) NOT NULL default ''"
		),
		'memo' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_reminder']['memo'],
			'search'                  => false,
			'inputType'               => 'textarea',
			'eval'                    => array('style'=>'height:60px;', 'tl_class'=>'clr', 'decodeEntities'=>true),
			'sql'                     => "text NULL"
		),

	)
);


/**
 * Class tl_bbk_reminder
 *
 * Provide miscellaneous methods that are used by the data configuration array.
 * @copyright  Leo Feyer 2005-2012
 * @author     Leo Feyer <http://www.contao.org>
 * @package    Controller
 */
class tl_bbk_reminder extends Backend
{

	/**
	 * Import the back end user object
	 */
	public function __construct()
	{
		parent::__construct();
		$this->import('BackendUser', 'User');
	}


	/**
	 * Check permissions to edit table tl_bbk_reminder
	 */
	public function checkPermission()
	{
		if ($this->User->isAdmin)
		{
			return;
		}

		// Set root IDs
		if (!is_array($this->User->calendars) || empty($this->User->calendars))
		{
			$root = array(0);
		}
		else
		{
			$root = $this->User->calendars;
		}

		$id = strlen($this->Input->get('id')) ? $this->Input->get('id') : CURRENT_ID;

		// Check current action
		switch ($this->Input->get('act'))
		{
			case 'create':
				if (!strlen($this->Input->get('pid')) || !in_array($this->Input->get('pid'), $root))
				{
					$this->log('Not enough permissions to create Event Reservation in channel ID "'.$this->Input->get('pid').'"', 'tl_bbk_reminder checkPermission', TL_ERROR);
					$this->redirect('contao/main.php?act=error');
				}
				break;

			case 'edit':
			case 'show':
			case 'copy':
			case 'delete':
			case 'toggle':
				$objRecipient = $this->Database->prepare("SELECT pid FROM tl_bbk_reminder WHERE id=?")
											   ->limit(1)
											   ->execute($id);

				if ($objRecipient->numRows < 1)
				{
					$this->log('Invalid Event Reservation ID "'.$id.'"', 'tl_bbk_reminder checkPermission', TL_ERROR);
					$this->redirect('contao/main.php?act=error');
				}

				if (!in_array($objRecipient->pid, $root))
				{
					$this->log('Not enough permissions to '.$this->Input->get('act').' recipient ID "'.$id.'" of calendar event ID "'.$objRecipient->pid.'"', 'tl_bbk_reminder checkPermission', TL_ERROR);
					$this->redirect('contao/main.php?act=error');
				}
				break;

			case 'select':
			case 'editAll':
			case 'deleteAll':
			case 'overrideAll':
				if (!in_array($id, $root))
				{
					$this->log('Not enough permissions to access calendar event ID "'.$id.'"', 'tl_bbk_reminder checkPermission', TL_ERROR);
					$this->redirect('contao/main.php?act=error');
				}

				$objRecipient = $this->Database->prepare("SELECT id FROM tl_bbk_reminder WHERE pid=?")
											 ->execute($id);

				if ($objRecipient->numRows < 1)
				{
					$this->log('Invalid Event Reservation ID "'.$id.'"', 'tl_bbk_reminder checkPermission', TL_ERROR);
					$this->redirect('contao/main.php?act=error');
				}

				$session = $this->Session->getData();
				$session['CURRENT']['IDS'] = array_intersect($session['CURRENT']['IDS'], $objRecipient->fetchEach('id'));
				$this->Session->setData($session);
				break;

			default:
				if (strlen($this->Input->get('act')))
				{
					$this->log('Invalid command "'.$this->Input->get('act').'"', 'tl_bbk_reminder checkPermission', TL_ERROR);
					$this->redirect('contao/main.php?act=error');
				}
				elseif (!in_array($id, $root))
				{
					$this->log('Not enough permissions to access Event Reservation ID "'.$id.'"', 'tl_bbk_reminder checkPermission', TL_ERROR);
					$this->redirect('contao/main.php?act=error');
				}
				break;
		}
	}

	/**
	 * Return the calndar-button
	 * @param array
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @return string
	 */
	public function calButton($row, $href, $label, $title, $icon, $attributes)
	{
        $href = str_replace('table=tl_bbk_reminder','table=tl_bbk_booking',$href);
	    return '<a href="'.$this->addToUrl($href.'&amp;bbk='.$row['bbk_id']).'" title="'.specialchars($title).'"'.$attributes.'>'.$this->generateImage($icon, $label).'</a> ';
	}
	/**
	 * Return the calndar-button
	 * @param array
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @return string
	 */
	public function editButton($row, $href, $label, $title, $icon, $attributes)
	{

	    return '<a href="'.$this->addToUrl('table=tl_bbk_booking&act=edit&id='.$row['booking_id']).'" title="'.specialchars($title).'"'.$attributes.'>'.$this->generateImage($icon, $label).'</a> ';
	}

	/**
	* onload_callback function
	*/
	public function setBooking4NewReminder($table,$id,$set,$obj)
	{
	    if($this->Input->get('bbk_booking') && $this->Input->get('mode')==2)
	    {
		$set['booking'] = $this->Input->get('bbk_booking');

		$this->Database->prepare('UPDATE `tl_bbk_reminder` %s WHERE `id`=?')
		->set($set)
		->execute($id);
	    }
	}
	
     /**
	 * get custom view from library-item-options
	 * @param object
	 * @throws Exception
	 */
	public function getLibraryOptions(DataContainer $dc)
	{
            $varValue= array();

            $all = $this->Database->prepare('SELECT * FROM `tl_bbk_libraries` ORDER BY `name` ASC')
				  ->execute();
            while($all->next())
            {
		$varValue[$all->id] = $all->name.' ('.$all->postal.' '.$all->city.')';
            }

	    return $varValue;
	}

     /**
	 * get custom view from bbk-item-options
	 * @param object
	 * @throws Exception
	 */
	public function getBbkOptions(DataContainer $dc)
	{
            $varValue= array();

            //Standort-ID
            $locationID = ($dc->activeRecord->pid) ? $dc->activeRecord->pid : $this->Input->get('id');


            $all = $this->Database->prepare('SELECT `tl_bbk`.*,`tl_bbk_locations`.`name` `standort` FROM `tl_bbk`
            LEFT JOIN `tl_bbk_locations` ON `tl_bbk_locations`.`id` = `tl_bbk`.`pid`
            WHERE `published`=? AND `tl_bbk`.`pid`=? ORDER BY `title` ASC')
				  ->execute(1,$locationID);
            while($all->next())
            {
		$varValue[$all->id] = $all->title.' ('.$all->bbkNr.', '.$all->standort.')';
            }

	    return $varValue;
	}


	/**
	* update Entry-Dataset
	*
	*/
	public function UpdateEntry($varValue, DataContainer $dc)
	{
	    if($varValue != $dc->activeRecord->accepted)
	    {
			if($varValue == 1)
			{
				$this->Database->prepare("UPDATE `tl_bbk_reminder` SET `modify` = ". time() .", `addedOn` = ".time()." WHERE id=?")
				->execute($dc->id);

				$this->Database->prepare("UPDATE `tl_calendar_events` SET `booked` = `booked`+1 WHERE id=?")
				->execute($dc->activeRecord->pid);

			}
			elseif($varValue == 2)
			{
		    	$this->Database->prepare("UPDATE `tl_bbk_reminder` SET `modify` = ". time() ." WHERE id=?")
				->execute($dc->id);
			}
			else
			{
		    	$this->Database->prepare("UPDATE `tl_bbk_reminder` SET `modify` = ". time() .", `addedOn` = '' WHERE id=?")
				->execute($dc->id);
			}

			//Update Email-Fields
			$this->fillEmailFields($varValue, $dc);
	    }

		return  $varValue;
	}


   /**
	 * get custom view from bbk-item-options
	 * @param object
	 * @throws Exception
	 */
	public function getBookingOptions(DataContainer $dc)
	{
            $varValue= array();

            //Standort-ID
            $locationID = ($dc->activeRecord->pid) ? $dc->activeRecord->pid : $this->Input->get('id');


            $all = $this->Database->prepare('SELECT `tl_bbk_booking`.*,`tl_bbk_locations`.`name` `standort` FROM `tl_bbk_booking`
            LEFT JOIN `tl_bbk_locations` ON `tl_bbk_locations`.`id` = `tl_bbk_booking`.`pid`
            WHERE `accepted`=? AND `tl_bbk_booking`.`pid`=? AND `inHouse`=? ORDER BY `endDate` ASC')
				  ->execute(1,$locationID, '');
            while($all->next())
            {
				$varValue[$all->id] = $all->bbk.' ('.$all->bbk.', '.$all->library.')';
            }

	    return $varValue;
	}

	/**
	 * modify list-view
	 * @param array
	 * @param string
	 * @return string
	 */
	public function listEntries($row, $label)
	{
	    $bbkObj = $this->Database->prepare('SELECT * FROM `tl_bbk` WHERE `id`=?')->limit(1)->execute($row['pid']);
	    $bookingObj = $this->Database->prepare('SELECT `bkg`.*, `bbk`.`bbkNr`, `bbk`.`title` FROM `tl_bbk_booking` `bkg` LEFT JOIN `tl_bbk` `bbk` ON `bkg`.`bbk` = `bbk`.`id`  WHERE `bkg`.`id`=?')->limit(1)->execute($row['booking_id']);

	    $label  = '<strong>Status:</strong> <span class="status_'.(int)$bookingObj->accepted.'">'.$GLOBALS['TL_LANG']['tl_bbk_reminder']['acceptedOptions'][$bookingObj->accepted].'</span><br>';
	    $label .= '<strong>Ausleiher:</strong> '.$bookingObj->library.', '.$bookingObj->firstname.' '.$bookingObj->lastname.' '.$bookingObj->postal.' '.$bookingObj->city.',<br> Tel.: '.$bookingObj->phone.' ,Email: '.$row['email'].'<br>';
	    $label .= '<strong>Zeitraum: </strong>'.date($GLOBALS['TL_CONFIG']['dateFormat'],$bookingObj->startDate).' - '.date($GLOBALS['TL_CONFIG']['dateFormat'],$bookingObj->endDate).'<br>';
	    $label .= '<strong>BBK:</strong> '.$bookingObj->title.' ('.$bookingObj->bbkNr.')<br>';
	    if($row['memo'] !='') $label .= '<strong>Memo/Notiz:</strong> '.nl2br($bookingObj->memo);

	    return sprintf('<div style="float:left">%s</div>',$label) . "\n";
	}



}

?>
