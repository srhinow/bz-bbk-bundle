<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Table tl_bbk_properties
 */
$GLOBALS['TL_DCA']['tl_bbk_properties'] = array
(

	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
		'enableVersioning'            => false,
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary'
			)
		),
		'onload_callback' => array
		(
			array('tl_bbk_properties', 'create_property_entry')
		),


	),
	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 1,
			'flag'                    => 12,
			'panelLayout'             => 'filter;search,limit'
		),
		'label' => array
		(
			'fields'                  => array('title', 'author'),
			'format'                  => '%s (%s)',
		),
		'global_operations' => array
		(
			'properties' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_story_book_cinema']['properties'],
				'href'                => 'table=tl_bbk_properties&act=edit',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset();" accesskey="e"'
			),
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset();" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_story_book_cinema']['edit'],
				'href'                => 'table=tl_story_book_cinema&act=edit',
				'icon'                => 'edit.gif',
				'attributes'          => 'class="contextmenu"'
			),
			'editheader' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_story_book_cinema']['editheader'],
				'href'                => 'act=edit',
				'icon'                => 'header.gif',
				'attributes'          => 'class="edit-header"'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_story_book_cinema']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.gif'
			),

			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_story_book_cinema']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if (!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\')) return false; Backend.getScrollOffset();"',
			)

		)
	),

	// Palettes
	'palettes' => array
	(
		'default'                     => '{new_member_info_legend:hide},newuser_info_email,newuser_info_name,newuser_info_subject',

	),

	// Fields
	'fields' => array
	(
	    'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'modify' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
	    'newuser_info_email' => array
	    (
		    'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_properties']['newuser_info_email'],
		    'exclude'                 => true,
		    'search'                  => true,
		    'filter'                  => true,
		    'inputType'               => 'text',
		    'eval'                    => array('rgxp'=>'email', 'maxlength'=>128, 'decodeEntities'=>true, 'tl_class'=>'clr w50'),
		    'sql'                     => "varchar(128) NOT NULL default ''"
	    ),
	    'newuser_info_name' => array
	    (
		    'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_properties']['newuser_info_name'],
		    'exclude'                 => true,
		    'search'                  => true,
		    'sorting'                 => true,
		    'flag'                    => 11,
		    'inputType'               => 'text',
		    'eval'                    => array('decodeEntities'=>true, 'maxlength'=>128, 'tl_class'=>'w50'),
		    'sql'                     => "varchar(128) NOT NULL default ''"
	    ),
	    'newuser_info_subject' => array
	    (
		    'label'                   => &$GLOBALS['TL_LANG']['tl_bbk_properties']['newuser_info_subject'],
		    'exclude'                 => true,
		    'search'                  => true,
		    'sorting'                 => true,
		    'flag'                    => 11,
		    'inputType'               => 'text',
		    'default'		      => &$GLOBALS['TL_LANG']['tl_bbk_properties']['newmember_subject_default'],
		    'eval'                    => array( 'maxlength'=>255, 'tl_class'=>'clr long', 'decodeEntities'=>true),
		    'sql'                     => "varchar(128) NOT NULL default ''"
	    ),
	)
);


/**
 * Class tl_bbk_properties
 *
 * Provide miscellaneous methods that are used by the data configuration array.
 * @copyright  Leo Feyer 2005-2012
 * @author     Leo Feyer <http://www.contao.org>
 * @package    Controller
 */
class tl_bbk_properties extends Backend
{

	/**
	 * Import the back end user object
	 */
	public function __construct()
	{
		parent::__construct();
		$this->import('BackendUser', 'User');
	}


	/**
	* create an entry if id=1 not exists
	* @return none
	*/
	public function create_property_entry()
	{
		$testObj = $this->Database->execute('SELECT * FROM `tl_bbk_properties`');

		if($testObj->numRows == 0)
		{
			$this->Database->execute('INSERT INTO `tl_bbk_properties`(`id`) VALUES(1)');
		}
	}

	/**
	* update Entry-Dataset
	*
	*/
	public function UpdateEntry($varValue, DataContainer $dc)
	{
		if($varValue != $dc->activeRecord->accepted)
		{
			if($varValue == 1)
			{
				$this->Database->prepare("UPDATE `tl_bbk_properties` SET `modify` = ". time() .", `addedOn` = ".time()." WHERE id=?")
				->execute($dc->id);

				$this->Database->prepare("UPDATE `tl_calendar_events` SET `booked` = `booked`+1 WHERE id=?")
				->execute($dc->activeRecord->pid);

			}
			elseif($varValue == 2)
			{
				$this->Database->prepare("UPDATE `tl_bbk_properties` SET `modify` = ". time() ." WHERE id=?")
				->execute($dc->id);
			}
			else
			{
				$this->Database->prepare("UPDATE `tl_bbk_properties` SET `modify` = ". time() .", `addedOn` = '' WHERE id=?")
				->execute($dc->id);

				$this->Database->prepare("UPDATE `tl_calendar_events` SET `booked` = `booked`-1 WHERE id=?")
				->execute($dc->activeRecord->pid);
			}

			//Update Email-Fields
			$this->fillEmailFields($varValue, $dc);
		}
		return  $varValue;

	}


}

?>
