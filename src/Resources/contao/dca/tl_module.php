<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2016 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Add palettes to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['bbk_reserv_calendar']    = '{title_legend},name,headline,type;{config_legend},cal_startDay;{template_legend:hide},cal_ctemplate;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';
$GLOBALS['TL_DCA']['tl_module']['palettes']['bbk_reserv_form'] = '{title_legend},name,headline,type;{email_legend},ereserv_email,ereserv_subject;{template_legend:hide},bbk_template;{protected_legend:hide},protected;{expert_legend:hide},cssID,space;{registerform_legend},jumpTo';
$GLOBALS['TL_DCA']['tl_module']['palettes']['bbk_details']  = '{title_legend},name,headline,type;{redirect_legend},jumpTo;{template_legend:hide},bbk_template';
$GLOBALS['TL_DCA']['tl_module']['palettes']['bbk_filter_form']  = '{title_legend},name,headline,type;{redirect_legend},jumpTo;{template_legend:hide},bbk_template';
$GLOBALS['TL_DCA']['tl_module']['palettes']['bbk_list']  = '{title_legend},name,headline,type;{redirect_legend},jumpTo,numberOfItems,perPage;{template_legend:hide},bbk_template';
$GLOBALS['TL_DCA']['tl_module']['palettes']['bbk_pdf_create_button']  = '{title_legend},name,headline,type;{template_legend:hide},bbk_template,bbk_pdf_title,bbk_pdf_keywords';

$GLOBALS['TL_DCA']['tl_module']['fields']['bbk_template'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['bbk_template'],
	'default'                 => 'bbk_default',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'        => array('tl_module_bbk', 'getTemplates'),
	'eval'                    => array('tl_class'=>'w50'),
	'sql'					  => "varchar(32) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['bbk_pdf_title'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['bbk_pdf_title'],
	'exclude'                 => true,
	'inputType'               => 'text',
	'eval'                    => array( 'maxlength'=>255, 'tl_class'=>'clr long', 'decodeEntities'=>true),
	'sql'					  => "varchar(255) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['bbk_pdf_keywords'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['bbk_pdf_keywords'],
	'exclude'                 => true,
	'inputType'               => 'text',
	'eval'                    => array( 'maxlength'=>255, 'tl_class'=>'clr long', 'decodeEntities'=>true),
	'sql'					  => "varchar(255) NOT NULL default ''"
);

/**
 * Class tl_module_bbk
 *
 * Provide miscellaneous methods that are used by the data configuration array.
 * @package    Controller
 */
class tl_module_bbk extends \Backend
{
	/**
	 * Return all info templates as array
	 * @param DataContainer
	 * @return array
	 */
	public function getTemplates(DataContainer $dc)
	{
		$intPid = $dc->activeRecord->pid;

		if ($this->Input->get('act') == 'overrideAll')
		{
			$intPid = $this->Input->get('id');
		}

		return $this->getTemplateGroup('bbk_');
	}
}
