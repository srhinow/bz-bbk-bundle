<?php

/**
 * for Contao Open Source CMS
 *
 * Copyright (c) 2016 Sven Rhinow
 *
 * @package BBK
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */
namespace Srhinow\BzBbkBundle\Models;

use Contao\Model;

class BbkModel extends Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_bbk';


	/**
	 * Find bbk-items for pagination-list
	 *
	 * @param array   $filter     where-options
	 * @param array   $arrOptions An optional options array
	 *
	 * @return \Model\Collection|null A collection of models or null if there are no news
	 */
	public static function findBbks($intLimit=0, $intOffset=0, array $filter=array(), $distance=1, array $arrIds=array(), array $arrOptions=array())
	{
		$t = static::$strTable;
		$arrColumns = (count($filter) > 0)? $filter : null;

		if (!isset($arrOptions['order']))
		{
			$arrOptions['order'] = "$t.title, $t.mediatyp, $t.location ASC";
		}

		$arrOptions['limit']  = $intLimit;
		$arrOptions['offset'] = $intOffset;

		return static::findBy($arrColumns, null, $arrOptions);
	}
	
	/**
	 * Count all libraries items
	 *
	 * @param array   $filter     where-options
	 * @param array   $arrOptions An optional options array
	 *
	 * @return \Model\Collection|null A collection of models or null if there are no news
	 */
	public static function countBbkEntries(array $filter=array(), array $arrOptions=array())
	{
		$t = static::$strTable;
		$arrColumns = (count($filter) > 0)? $filter : null;

		return static::countBy($arrColumns, null, $arrOptions);
	}
}
