<?php

/**
 * for Contao Open Source CMS
 *
 * Copyright (c) 2016 Sven Rhinow
 *
 * @package BBK
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */
namespace Srhinow\BzBbkBundle\Models;

use Contao\Model;

class BbkLocationsModel extends Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_bbk_locations';
	
}
