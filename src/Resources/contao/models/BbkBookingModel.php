<?php
/**
 * Created by bzn-cms_contao4.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 25.01.20
 */

namespace Srhinow\BzBbkBundle\Models;


use Contao\Model;

class BbkBookingModel extends Model
{
    /**
     * Table name
     * @var string
     */
    protected static $strTable = 'tl_bbk_booking';

}