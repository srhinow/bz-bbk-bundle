<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension lawyer-client-portal.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license commercial
 */

namespace Srhinow\BzBbkBundle\EventListener\Hooks;

use Contao\CoreBundle\Event\MenuEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class BackendMenuListener
{
    protected $router;
    protected $requestStack;

    public function __construct(RouterInterface $router, RequestStack $requestStack)
    {
        $this->router = $router;
        $this->requestStack = $requestStack;
    }

    public function onBuild(MenuEvent $event): void
    {
        $factory = $event->getFactory();
        $tree = $event->getTree();
        if ('mainMenu' !== $tree->getName()) {
            return;
        }

        $contentNode = $tree->getChild('bbk');
        if(null === $contentNode) return;

        //damit es kein Exception wirft wenn ein Feld  dabei ist was für den BackendUser nicht verfügbar ist
        $arrToOrder = [];
        $arrBbkChilds = ['bbk_locations', 'bbk_libraries', 'bbk_properties'];

        $arrChilds = $contentNode->getChildren();
        if(!is_array($arrChilds)) return;

        $arrChildKeys = array_keys($arrChilds);

        foreach ($arrBbkChilds as $name) {
            if (\in_array($name, $arrChildKeys, true)) {
                $arrToOrder[] = $name;
                $key = array_search($name, $arrChildKeys, true);
                unset($arrChildKeys[$key]);
            }
        }

        $contentNode->reorderChildren(array_merge($arrToOrder, $arrChildKeys));
    }
}
