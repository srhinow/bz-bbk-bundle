<?php
/**
 * Created by bzn-cms_contao4.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 25.01.20
 */

namespace Srhinow\BzBbkBundle\EventListener\Hooks;


use Contao\CoreBundle\Framework\ContaoFrameworkInterface;
use Srhinow\BzBbkBundle\Models\BbkModel;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class InsertTagsListener
{
    /**
     * Constructor.
     */
    public function __construct(ContaoFrameworkInterface $framework, UrlGeneratorInterface $router)
    {
        $this->framework = $framework;
        $this->router = $router;
    }

    /**
     * replace bn-specific inserttag if get-paramter isset
     * bn::colname::alternative from objPage
     * @param string
     * @return string
     */
    public function bbkReplaceInsertTags($strTag)
    {

        if (substr($strTag,0,5) == 'bbk::')
        {
            global $objPage;

            $bbkId = \Input::get('bbk');
            $split = explode('::',$strTag);

            if(strlen($bbkId) < 1 || (int) $bbkId < 1) return $objPage->$split[2];

            $objBbk = BbkModel::findByIdOrAlias($bbkId);

            switch($split[1]){
                case 'printbutton':
                    return (!$bbkId) ? '' : '<a href="javascript:window.print()" class="printbutton"><i class="fa fa-print"></i></a>';

                    break;
                default:
                    return $objBbk->$split[1];
            }

        }

        return false;
    }


    /**
     * replace bn-specific inserttag if get-paramter isset
     * bn::colname::alternative from objPage
     * @param string
     * @return string
     */
    public function bzNewsReplaceInsertTags($strTag)
    {
        exit();
        return $strTag;

        if (substr($strTag,0,8) == 'bznews::')
        {
            global $objPage;

            // Set the item from the auto_item parameter
            if (!isset($_GET['items']) && \Config::get('useAutoItem') && isset($_GET['auto_item']))
            {
                \Input::setGet('items', \Input::get('auto_item'));
            }

            $split = explode('::',$strTag);

            // Do not index or cache the page if there are no archives
            if (!\Input::get('items'))  return $objPage->$split[2];

            // Get the news item
            $objNews = \NewsModel::findByIdOrAlias(\Input::get('items'));

            if($objNews !== null) return $objNews->$split[1];

        }

        return false;
    }
}