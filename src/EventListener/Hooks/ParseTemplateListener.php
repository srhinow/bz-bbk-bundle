<?php
/**
 * Created by bzn-cms_contao4.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 25.01.20
 */

namespace Srhinow\BzBbkBundle\EventListener\Hooks;


use Contao\Controller;
use Contao\CoreBundle\Framework\ContaoFrameworkInterface;
use Contao\Template;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ParseTemplateListener
{
    /**
     * @var ContaoFrameworkInterface
     */
    private $framework;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * Constructor.
     */
    public function __construct(ContaoFrameworkInterface $framework, UrlGeneratorInterface $router)
    {
        $this->framework = $framework;
        $this->router = $router;
    }

    public function redirectBbkDeeplink(Template $objTemplate)
    {
        if ($objTemplate->getName() == 'be_confirm' && $objTemplate->info['table'] == 'tl_bbk_booking' && !$objTemplate->info['rt'])
        {
            Controller::redirect($objTemplate->href);
        }
    }
}