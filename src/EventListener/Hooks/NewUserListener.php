<?php
/**
 * Created by bzn-cms_contao4.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 25.01.20
 */

namespace Srhinow\BzBbkBundle\EventListener\Hooks;


use Contao\CoreBundle\Framework\ContaoFrameworkInterface;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\Email;
use Contao\Environment;
use Contao\StringUtil;
use Contao\System;
use Psr\Log\LogLevel;
use Srhinow\BzBbkBundle\Models\BbkPropertiesModel;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class NewUserListener
{
    /**
     * Constructor.
     */
    public function __construct(ContaoFrameworkInterface $framework, UrlGeneratorInterface $router)
    {
        $this->framework = $framework;
        $this->router = $router;
    }

    /**
     * Send an admin notification e-mail
     * @param integer
     * @param array
     */
    public function sendNewRegisterNotification($intId, $arrData)
    {
        //get settings from bbk_properties
        $objSettings = BbkPropertiesModel::findByPk(1);

        if(null === $objSettings)
        {
            $objEmail = new Email();

            $objEmail->from = $GLOBALS['TL_ADMIN_EMAIL'];
            $objEmail->fromName = $GLOBALS['TL_ADMIN_NAME'];
            $objEmail->subject = sprintf($objSettings->newuser_info_subject, Environment::get('host'));

            $strData = "\n\n";

            // Add user details
            foreach ($arrData as $k=>$v)
            {
                if ($k == 'password' || $k == 'tstamp' || $k == 'activation')
                {
                    continue;
                }

                $v = StringUtil::deserialize($v);

                if ($k == 'dateOfBirth' && strlen($v))
                {
                    $v = \Date::parse($GLOBALS['TL_CONFIG']['dateFormat'], $v);
                }

                $strData .= $GLOBALS['TL_LANG']['tl_member'][$k][0] . ': ' . (is_array($v) ? implode(', ', $v) : $v) . "\n";
            }

            $objEmail->text = sprintf($GLOBALS['TL_LANG']['MSC']['adminText'], $intId, $strData . "\n") . "\n";
            $objEmail->sendTo($objSettings->newuser_info_email);
        }

        $strText = 'A new user (ID ' . $intId . ') has registered on the website';
        $logger = System::getContainer()->get('monolog.logger.contao');
        $logger->log(LogLevel::INFO, $strText, array('contao' => new ContaoContext(__METHOD__, TL_ACCESS)));
    }
}