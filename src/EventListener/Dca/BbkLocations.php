<?php
/**
 * Created by bzn-cms_contao4.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 26.01.20
 */

namespace Srhinow\BzBbkBundle\EventListener\Dca;


use Contao\Backend;
use Contao\DataContainer;
use Contao\Image;
use Contao\StringUtil;
use Srhinow\BzBbkBundle\Models\BbkLocationsModel;
use Srhinow\BzBbkBundle\Models\BbkModel;

class BbkLocations extends Backend
{

    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * Return the edit BBK button
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     * @return string
     */
    public function editBBK($row, $href, $label, $title, $icon, $attributes)
    {
        return '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * Return the edit BBK button
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     * @return string
     */
    public function createButton($row, $href, $label, $title, $icon, $attributes)
    {
        return '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * Auto-generate an article alias if it has not been set yet
     * @param mixed
     * @param DataContainer
     * @return string
     * @throws \Exception
     */
    public function generateAlias($varValue, DataContainer $dc)
    {
        $autoAlias = false;

        // Generate an alias if there is none
        if ($varValue == '')
        {
            $autoAlias = true;
            $varValue = standardize(StringUtil::restoreBasicEntities($dc->activeRecord->name));
        }

        $countAlias = BbkLocationsModel::countBy(['alias=?'],[$varValue]);

        // Check whether the page alias exists
        if ($countAlias > 1)
        {
            if (!$autoAlias)
            {
                throw new \Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }

            $varValue .= '-' . $dc->id;
        }

        return $varValue;
    }

    /**
     * Wenn der Alias der Location geändert wurde,
     * werden auch alle location-Felder der zum Standort gehoerenden bbks angepasst
     * @param $value
     * @param DataContainer $dc
     * @return mixed
     */
    public function updateBbkIfAliasChanges($value, DataContainer $dc) {

        if($value === '' || $value === $dc->activeRecord->alias) {
            return $value;
        }

        $objBbk = BbkModel::findBy(['pid=?'],[$dc->id]);
        if(null === $objBbk) {
            return $value;
        }

        while($objBbk->next()) {
            $objBbk->location = $value;
            $objBbk->save();
        }

        return $value;
    }
}