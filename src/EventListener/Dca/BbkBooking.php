<?php
/**
 * Created by bzn-cms_contao4.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 25.01.20
 */

namespace Srhinow\BzBbkBundle\EventListener\Dca;


use Contao\Backend;
use Contao\Controller;
use Contao\DataContainer;
use Contao\Date;
use Contao\Email;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use Contao\Versions;
use Srhinow\BzBbkBundle\Models\BbkBookingModel;
use Srhinow\BzBbkBundle\Models\BbkLibrariesModel;

class BbkBooking extends Backend
{
    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }


    /**
     * Return the edit BBK button
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     * @return string
     */
    public function editBooking($row, $href, $label, $title, $icon, $attributes)
    {
        return '<a href="'.$this->addToUrl($href.'&amp;bbk='.$row['bbk']).'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * onload_callback function
     */
    public function setBbk4NewBooking($table, $id, $set, $obj)
    {
        if(Input::get('bbk') && Input::get('mode')==2) {
            $objBbkBooking = BbkBookingModel::findByPk($id);
            $objBbkBooking->bbk = Input::get('bbk');
            $objBbkBooking->save();
        }
    }

    /**
     * update Entry-Dataset
     * @param mixed $varValue
     * @param DataContainer $dc
     * @return mixed
     * @throws \Exception
     */
    public function UpdateEntry($varValue, DataContainer $dc)
    {
        if($varValue != $dc->activeRecord->accepted)
        {
            $Database = \Contao\Database::getInstance();

            if($varValue == 1)
            {
                $Database->prepare("UPDATE `tl_bbk_booking` SET `modify` = ". time() .", `addedOn` = ".time()." WHERE id=?")
                    ->execute($dc->id);

                $Database->prepare("UPDATE `tl_calendar_events` SET `booked` = `booked`+1 WHERE id=?")
                    ->execute($dc->activeRecord->pid);

            }
            elseif($varValue == 2)
            {
                $Database->prepare("UPDATE `tl_bbk_booking` SET `modify` = ". time() ." WHERE id=?")
                    ->execute($dc->id);
            }
            else
            {
                $Database->prepare("UPDATE `tl_bbk_booking` SET `modify` = ". time() .", `addedOn` = '' WHERE id=?")
                    ->execute($dc->id);
            }

            //Update Email-Fields
            $this->fillEmailFields($varValue, $dc);
        }

        return  $varValue;
    }

    /**
     *    send a confirmation email to the event participants
     *
     */
    public function sendReservationEmail(DataContainer $dc)
    {

        if($dc->activeRecord->sendEmail > 0)
        {
            $resultObj = $this->Database->prepare('SELECT `booking`.*
			,`bbk`.`title`
			,`location`.`sender` as email_from
			,`location`.`senderName` as emailname_from
			FROM `tl_bbk_booking` as `booking`
			LEFT JOIN `tl_bbk` as `bbk` ON `bbk`.`id` = `booking`.`bbk`
			LEFT JOIN `tl_bbk_locations` as `location` ON `location`.`id` = `booking`.`pid`
			WHERE `booking`.`id` = ?')
                ->limit(1)
                ->execute($dc->id);

            if($resultObj->numRows)
            {
                if($resultObj->email)
                {
                    //send Email
                    $email = new Email();
                    $email->from = $resultObj->email_from;
                    $email->fromName = $resultObj->emailname_from;
                    $email->charset = 'utf-8';
                    $email->subject = $this->replacePlaceHolder($resultObj,$resultObj->subject);
                    $email->text = StringUtil::restoreBasicEntities($this->replacePlaceHolder($resultObj, $resultObj->text_email));
                    $email->html = StringUtil::restoreBasicEntities($this->replacePlaceHolder($resultObj, $resultObj->html_email));
                    $email->sendTo($resultObj->email);
                }
            }

            //disable everytime sendEmail
            $this->Database->prepare('UPDATE `tl_bbk_booking` SET `sendEmail`="" WHERE `id`=?')->execute($dc->id);
        }
    }

    public function inHouseToDos(DataContainer $dc)
    {
        if($dc->activeRecord->inHouse == 1)
        {
            $this->Database->prepare('DELETE FROM `tl_bbk_reminder` WHERE `pid`=? AND `booking_id`=? AND `bbk_id`')
                ->execute($dc->activeRecord->pid, $dc->id,$dc->activeRecord->bbk);
        }
    }

    public function replacePlaceHolder($dbObj,$text)
    {
        preg_match_all('/\#\#([^\#]+)\#\#/', $text, $tags);

        for($c=0;$c<count($tags[0]);$c++)
        {
            switch($tags[1][$c])
            {
                case 'date':
                case 'startDate':
                    $text = str_replace($tags[0][$c],\Date::parse($GLOBALS['TL_CONFIG']['dateFormat'],$dbObj->startDate),$text);
                    break;
                case 'endDate':
                    $text = str_replace($tags[0][$c],\Date::parse($GLOBALS['TL_CONFIG']['dateFormat'],$dbObj->endDate),$text);
                    break;
                default:
                    $text = str_replace($tags[0][$c],$dbObj->{$tags[1][$c]}, $text);
            }
        }

        return $text;
    }

    /**
     * fill Text before
     * @param object
     * @throws \Exception
     */
    public function fillEmailFields($varValue, DataContainer $dc)
    {
        $result = $this->Database->prepare('SELECT * FROM `tl_bbk_locations` WHERE `id`=?')
            ->limit(1)
            ->execute($dc->activeRecord->pid);
        switch($varValue)
        {
            case '1':
                $subject = $result->confirmed_subject;
                $html = $result->confirmed_html_email;
                $text = $result->confirmed_text_email;
                break;
            case '2':
                $subject = $result->rejected_subject;
                $html = $result->rejected_html_email;
                $text = $result->rejected_text_email;
                break;
            default:
                $subject = '';
                $html = '';
                $text = '';
        }

        //Insert Invoice-Entry
        $postenset = array
        (
            'subject' => $subject,
            'html_email' => $html,
            'text_email' => $text,
            'accepted' => $varValue
        );

        $this->Database->prepare('UPDATE `tl_bbk_booking` %s WHERE `id`=?')
            ->set($postenset)
            ->execute($dc->id);

        $this->reload();

        return $varValue;
    }

    /**
     * fill librarie fields
     * @param object
     * @throws \Exception
     */
    public function fillLibraryFields($varValue, DataContainer $dc)
    {
        if(strlen($varValue)<=0) return $varValue;

        $objLibraries = BbkLibrariesModel::findByPk($varValue);
        if(null === $objLibraries) return $varValue;

        $objBbkBooking = BbkBookingModel::findByPk($dc->id);
        $objBbkBooking->tstamp = time();
        $objBbkBooking->library = $objLibraries->name;
        $objBbkBooking->library_id = (int) $objLibraries->id;
        $objBbkBooking->privaddress = (int) $objLibraries->privaddress;

        if($dc->activeRecord->firstname == '') $objBbkBooking->firstname = $objLibraries->firstname;
        if($dc->activeRecord->lastname == '') $objBbkBooking->lastname = $objLibraries->lastname;
        if($dc->activeRecord->street == '') $objBbkBooking->street = $objLibraries->street;
        if($dc->activeRecord->postal == '') $objBbkBooking->postal = $objLibraries->postal;
        if($dc->activeRecord->city == '') $objBbkBooking->city = $objLibraries->city;
        if($dc->activeRecord->phone == '') $objBbkBooking->phone = $objLibraries->phone;
        if($dc->activeRecord->email == '') $objBbkBooking->email = $objLibraries->email;
        $objBbkBooking->save();

        $this->reload();

        return $varValue;
    }

    /**
     * fill ShippngStart field
     * @param object
     * @throws \Exception
     */
    public function fillShippingStart(DataContainer $dc)
    {
        if($this->Input->post('startDate')!='' && $dc->activeRecord->startShippingDate == 0)
        {
            //Insert Invoice-Entry
            $set = array(
                'startShippingDate' => strtotime($this->Input->post('startDate')),
            );

            $this->Database->prepare('UPDATE `tl_bbk_booking` %s WHERE `id`=?')
                ->set($set)
                ->execute($dc->id);
        }
    }

    /**
     * fill ShippngEnd field
     * @param object
     * @throws \Exception
     */
    public function fillShippingEnd(DataContainer $dc)
    {
        if($this->Input->post('endDate') && $dc->activeRecord->endShippingDate == 0)
        {
            $endTime = strtotime($this->Input->post('endDate'));

            switch(date('N',$endTime)) // Wochenend-Tag überspringen
            {
                case 3: // Mittwoch = 3 Tage vor Sonnabend
                    $addDaysStr = '+5 days';
                    break;
                case 4: // Donnerstag = 3 Tage vor Sonntag
                    $addDaysStr = '+4 days';
                    break;
                default:
                    $addDaysStr = '+3 days';
            }

            //Insert Invoice-Entry
            $set = array(
                'endShippingDate' => strtotime($addDaysStr,strtotime($this->Input->post('endDate'))),
            );

            $this->Database->prepare('UPDATE `tl_bbk_booking` %s WHERE `id`=?')
                ->set($set)
                ->execute($dc->id);
        }
    }

    /**
     * fill Loan field
     * @param object
     * @throws \Exception
     */
    public function fillLoan(DataContainer $dc)
    {

        if((int) $dc->activeRecord->loan == 0)
        {

            // den topaktuellsten Stand mit den evtl gerade abgespeicherten startShippingDate und endShippingDate holen
            $bookingObj = $this->Database->prepare('SELECT * FROM `tl_bbk_booking` WHERE `id`=?')
                ->limit(1)
                ->execute($dc->id);

            if($bookingObj->startDate > 0 && $bookingObj->endDate > 0)
            {
                $loan = 0;

                if($bookingObj->endDate == $bookingObj->startDate)   // wenn selber Tag
                {
                    $loan = 1;
                }
                elseif($bookingObj->endDate > $bookingObj->startDate) // wenn über mehrere Tage
                {
                    $diff = ($bookingObj->endDate - $bookingObj->startDate) / 86400;// Differenz in Tagen
                    $loan = ceil($diff /14);
                }

                $set = array(
                    'loan' => $loan,
                );

                $this->Database->prepare('UPDATE `tl_bbk_booking` %s WHERE `id`=?')
                    ->set($set)
                    ->execute($dc->id);
            }
        }
    }
    /**
     * correct the bbk from other location and allocate this reservation
     * @param object
     * @throws \Exception
     */
    public function correctLocation($value, DataContainer $dc)
    {
        if($dc->activeRecord->bbk=='' || $value=='') return $value;

        //get current bbk-dataset
        $curBbkObj = $this->Database->prepare('SELECT * FROM `tl_bbk` WHERE `id`=?')
            ->limit(1)
            ->execute($dc->activeRecord->bbk);

        if($curBbkObj->numRows < 1)
        {
            $_SESSION['TL_ERROR'][] = 'kein Passender  BBK-Datensatz gefunden';
            $this->reload();
        }

        //get associated bbk-dataset to new location
        $newBbkObj = $this->Database->prepare('SELECT * FROM `tl_bbk` WHERE `pid`=? AND `title`=? AND `mediatyp`=?')
            ->limit(1)
            ->execute($value,$curBbkObj->title,$curBbkObj->mediatyp);

        if($newBbkObj->numRows < 1)
        {
            $_SESSION['TL_ERROR'][] = 'kein aktueller aktueller BBK-Datensatz gefunden';
            $this->reload();
        }

        //update current booking-dataset
        $set = array('bbk'=>$newBbkObj->id, 'pid'=>$newBbkObj->pid);
        $this->Database->prepare('UPDATE `tl_bbk_booking` %s WHERE `id`=?')->set($set)->execute($dc->id);


        //redirect to new location  in edit-modus
        $this->reload();

    }

    /**
     * get custom view from bbk-item-options
     * @param object
     * @throws \Exception
     */
    public function getLocationOptions(DataContainer $dc)
    {
        $varValue= array();

        $all = $this->Database->prepare('SELECT * FROM `tl_bbk_locations`')
            ->execute();
        while($all->next())
        {
            $varValue[$all->id] = $all->name;
        }

        return $varValue;
    }

    /**
     * get custom view from library-item-options
     * @param object
     * @throws \Exception
     */
    public function getLibraryOptions(DataContainer $dc)
    {
        $varValue= array();

        $all = $this->Database->prepare('SELECT * FROM `tl_bbk_libraries` ORDER BY `name` ASC')
            ->execute();
        while($all->next())
        {
            $varValue[$all->id] = $all->name.' ('.$all->postal.' '.$all->city.')';
        }

        return $varValue;
    }

    /**
     * get custom view from bbk-item-options
     * @param object
     * @throws \Exception
     */
    public function getBbkOptions(DataContainer $dc)
    {
        $varValue= array();

        //Standort-ID
        $locationID = isset($dc->activeRecord->pid) ? $dc->activeRecord->pid : Input::get('id');


        $all = $this->Database->prepare('SELECT `tl_bbk`.*,`tl_bbk_locations`.`name` `standort` FROM `tl_bbk`
            LEFT JOIN `tl_bbk_locations` ON `tl_bbk_locations`.`id` = `tl_bbk`.`pid`
            WHERE `published`=? AND `tl_bbk`.`pid`=? ORDER BY `title` ASC')
            ->execute(1,$locationID);
        while($all->next())
        {
            $varValue[$all->id] = $all->title.' ('.$all->bbkNr.', '.$all->standort.')';
        }

        return $varValue;
    }

    /**
     * modify list-view
     * @param array
     * @param string
     * @return string
     */
    public function listEntries($row, $label)
    {
        /*			'fields'                  => array('library', 'startDate', 'endDate', 'bbk'),
                    'format'                  => 'Standort: %s, Zeitraum: %s - %s, BBK: %s',	    */
        $bbkObj = $this->Database->prepare('SELECT * FROM `tl_bbk` WHERE `id`=?')->limit(1)->execute($row['bbk']);

        $label  = '<strong>Status:</strong> <span class="status_'.(int)$row['accepted'].($row['inHouse']?' finish':'').'">'.$GLOBALS['TL_LANG']['tl_bbk_booking']['acceptedOptions'][$row['accepted']].'</span>'.($row['inHouse']?'&nbsp;<span class="loan_finish">zurück gegeben</span>':'').'<br>';
        $label .= '<strong>Ausleiher:</strong> '.$row['library'].', '.$row['firstname'].' '.$row['lastname'].' '.$row['postal'].' '.$row['city'].',<br> Tel.: '.$row['phone'].' ,Email: '.$row['email'].'<br>';
        if(!empty($row['startDate']) || !empty($row['endDate']))
        {
            $label .= '<strong>Zeitraum: </strong>'.date($GLOBALS['TL_CONFIG']['dateFormat'],(int)$row['startDate']).' - '.date($GLOBALS['TL_CONFIG']['dateFormat'],(int) $row['endDate']).'<br>';
        }
        $label .= '<strong>BBK:</strong> '.$bbkObj->title.' ('.$bbkObj->bbkNr.')<br>';
        if($row['memo'] !='') $label .= '<strong>Memo/Notiz:</strong> '.nl2br($row['memo']);

        return sprintf('<div style="float:left">%s</div>',$label) . "\n";
    }
}