<?php
/**
 * Created by bzn-cms_contao4.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 06.03.22
 */

namespace Srhinow\BzBbkBundle\EventListener\Dca;

use Contao\Backend;

class BbkLibraries extends Backend
{
    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * get custom view from library-item-options
     * @param object
     * @throws \Exception
     */
    public function getMemberOptions(): array
    {
        $varValue = array();
        $groupID = 1;

        $all = $this->Database->prepare('SELECT * FROM `tl_member` WHERE `groups` IS NOT NULL ORDER BY `postal` ASC')
            ->execute();

        while ($all->next()) {
            $groupArr = unserialize($all->groups);

            if (!is_array($groupArr) || !in_array($groupID, $groupArr)) continue;

            $varValue[$all->id] = $all->firstname . ' ' . $all->lastname . ' (' . $all->username . '), ' . $all->postal . ' ' . $all->city;
        }

        return $varValue;
    }
}