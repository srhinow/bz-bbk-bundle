<?php
/**
 * Created by bzn-cms_contao4.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 25.01.20
 */

namespace Srhinow\BzBbkBundle\EventListener\Dca;


use Contao\Backend;
use Contao\Config;
use Contao\DataContainer;
use Contao\Input;
use Contao\StringUtil;
use Srhinow\BzBbkBundle\Models\BbkBookingModel;
use Srhinow\BzBbkBundle\Models\BbkLocationsModel;
use Srhinow\BzBbkBundle\Models\BbkModel;

class Bbk extends Backend
{
    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * Return the edit BBK button
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     * @return string
     */
    public function editBooking($row, $href, $label, $title, $icon, $attributes)
    {
        return '<a href="'.$this->addToUrl($href.'&amp;bbk='.$row['id']).'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'.\Image::getHtml($icon, $label).'</a> ';
    }


    /**
     * fill date-Field if this empty
     * @param mixed
     * @param object
     */
    public function  generateCreateTstamp(DataContainer $dc)
    {
        $created_tstamp = ($dc->activeRecord->created_tstamp == 0) ? time() : $dc->activeRecord->created_tstamp;

        $objBbk = BbkModel::findByPk($dc->id);
        $objBbk->created_tstamp = $created_tstamp;
        $objBbk->save();
    }

    /**
     * verknuepft das BBK mit einem Standort wenn location ausgewählt/geändert wurde
     * @param mixed
     * @param object
     */
    public function  changeLocation($value, DataContainer $dc)
    {
        if($value !== $dc->activeRecord->pid || $dc->activeRecord->loacation === '')
        {
            $objLocation = BbkLocationsModel::findByPk($value);
            if(null === $objLocation) return $value;

            //den Alias in 'location' zum aktuellen Datensatz speichern
            $objBbk = BbkModel::findByPk($dc->id);
            $objBbk->location = $objLocation->alias;
            $objBbk->save();

            // alle Buchungen zum BBk mit der neuen Location aktualisieren
            $objBookings = BbkBookingModel::findBy(['bbk=?'],[$dc->id]);
            if(null === $objBookings) return $value;

            while($objBookings->next()) {
                $objBookings->pid = $value;
                $objBookings->save();
            }
        }

        return $value;
    }

    public function fillEmptyLocations(): void
    {

        $objBbk = BbkModel::findBy(['location=?'],['']);
        if(null === $objBbk) return;

        $arrLocation = $this->getLocationAliasAsArray();

        while($objBbk->next()) {
            $objBbk->location = $arrLocation[$objBbk->pid];
            $objBbk->save();
        }
    }

    protected function getLocationAliasAsArray(): array
    {
        $arrReturn = [];

        $objLocation = BbkLocationsModel::findAll();
        if(null === $objLocation) return $arrReturn;

        while($objLocation->next()){
            $arrReturn[$objLocation->id] = $objLocation->alias;
        }

        return $arrReturn;
    }
}